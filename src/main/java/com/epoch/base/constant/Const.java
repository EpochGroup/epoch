package com.epoch.base.constant;

public class Const {
	
	public static final int ZERO = 0;
	
	
	public static final String INIT_PASSWORD = "123456";
	
	//closed说明有子项，会以文件夹的可展开的形式显示
	public static final String CLOSED = "closed";
	
	//open说明没有子项，不可展开。
	public static final String OPEN = "open";
	
	//已授权未授权
	public static final String GRANTED = "anuthorized";
	public static final String UNGRANTED = "unanuthorized";
	
	public static final String DEFAULT_MENU_URL = "#";
	public static final String DEFAULT_MENU_ICON = "fa fa-file-text";
	
	
	
}

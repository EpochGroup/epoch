package com.epoch.base.constant;

public interface DBConst {
	
    /**
     * 数据库连接参数：ORACLE驱动
     */
    String DRIVERCLASS = ".driverClass";

    /**
     * 数据库连接参数：ORACLE连接URL
     */
    String JDBCURL = ".jdbcUrl";

    /**
     * 数据库连接参数：ORACLE用户名
     */
    String USERNAME = ".userName";

    /**
     * 数据库连接参数：ORACLE密码
     */
    String PASSWORD = ".passWord";
    /**
     * 数据库连接参数：初始连接池大小
     */
    String DBINITIALSIZE = "db.initialSize";
    /**
     * 数据库连接参数：最小空闲连接数
     */
    String DBMINIDLE = "db.minIdle";
    /**
     * 数据库连接参数：最大活跃连接数
     */
    String DBMAXACTIVE = "db.maxActive";
    /**
     * 当前数据库类型
     */
    String DBTYPE = "db.type";

    /**
     * 当前数据库类型
     */
    String DBTYPE_MYSQL = "mysql";

    /**
     * 当前数据库类型
     */
    String DBTYPE_ORACLE = "oracle";

    /**
     * 配置获取连接等待超时的时间
     */
    String DB_SETMAXWAIT = "db.setMaxWait";

    /**
     * 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
     */
    String DB_SETTIMEBETWEENEVICTIONRUNSMILLIS = "db.setTimeBetweenEvictionRunsMillis";

    /**
     * 配置一个连接在池中最小生存的时间，单位是毫秒
     */
    String DB_SETMINEVICTABLEIDLETIMEMILLIS = "db.setMinEvictableIdleTimeMillis";

    /**
     * 主数据源名称：系统主数据源
     */
    String DBDATASOURCEMAIN = "db.dataSource.main";

    /**
     * 是否开发模式
     */
    String CONFIGDEVMODE = "config.devMode";


    /**
     * 缓存类型配置，可以是ehcache、redis
     */
    String CONFIG_CACHE_TYPE = "config.cache.type";

    /**
     * 缓存类型ehcache
     */
    String CACHE_TYPE_EHCACHE = "ehcache";

    /**
     * 缓存类型redis
     */
    String CACHE_TYPE_REDIS = "redis";

    /**
     * URL缓存Key
     */
    String CACHE_NAME_EHCACHE_PAGE = "SimplePageCachingFilter";

    /**
     * 系统缓存，主要是权限和数据字典等
     */
    String CACHE_NAME_EHCACHE_SYSTEM = "system";

    /**
     * 系统缓存，主要是权限和数据字典等
     */
    String CACHE_NAME_REDIS_SYSTEM = "system";

    /**
     * 扫包
     */
    String CONFIG_SCAN_JAR = "config.scan.jar";
    /**
     * 是否开启架包查找
     */
    String CONFIG_SCAN_JAR_START = "config.scan.jar.start";

    /**
     * sqlMap格式名称
     */
    String SQLMAP_FILE_NAME = "sqlMap.xml";

    /**
     * 未加密密码
     * 密码存入cookie名称
     */
    String PASSWORD_SHIRO = "shiro_password";
    /**
     * 公共图片基础url
     */
    String BASE_IMAGE_URL = "config.image.baseUrl";

    String SYS_STATIC_URL = "config.sys.static.url";

    String BASE_UPLOAD_PATH = "config.fileupold.url";
}

package com.epoch.base.constant;

import com.jfinal.kit.PropKit;

/**
 * 数据字典常量
 */
public interface DictCodeConstant {
    /**
     * 环境变量
     */
    String ENVIRONMENT_CONFIGURATION = PropKit.use("application.properties").get("sys.environment.configuration.name");
    // 数据字典常量

    // ==Start== ================ [公共模块] ================
    // 邮件控制
    String EMAIL_CONTROL = "EMAIL_CONTROL"; // 控制邮件使用uat库还是使用sit库和dev库
    String EMAIL_CONTROL_FALSE = "false"; // sit
    // 邮件发送人地址
    String EMAIL_FROM = "EMAIL_FROM"; // 邮件发送人地址
    String EMAIL_FROM_VAL = " "; // 邮件发送人地址
    // 国际化语言
    String I18N_LANG = "I18N_LANG"; // 国际化语言
    String I18N_LANG_EN_US = "en_US"; // 英文语言
    String I18N_LANG_ZH_CN = "zh_CN"; // 简体中文语言
    String I18N_LANG_ZH_HK = "zh_HK"; // 繁体中文语言
    // 预算类型
    String PUB_BUDGET_TYPE = "PUB_BUDGET_TYPE"; // 预算类型
    String PUB_BUDGET_TYPE_CAPEX = "CAPEX"; // 资本类支出
    String PUB_BUDGET_TYPE_OPEX = "OPEX"; // 费用类支出
    String PUB_BUDGET_TYPE_MPEX = "MPEX"; // 销售类支出
    // 启用禁用
    String PUB_ENABLE_DISABLE = "PUB_ENABLE_DISABLE"; // 启用禁用
    String PUB_ENABLE_DISABLE_ENABLE = "ENABLE"; // 启用
    String PUB_ENABLE_DISABLE_DISABLE = "DISABLE"; // 禁用
    // 性别
    String PUB_GENDER = "PUB_GENDER"; // 性别
    String PUB_GENDER_MALE = "MALE"; // 男
    String PUB_GENDER_FEMALE = "FEMALE"; // 女
    // 月份
    String PUB_MONTH = "PUB_MONTH"; // 月份
    String PUB_MONTH_1 = "1"; // 1月
    String PUB_MONTH_2 = "2"; // 2月
    String PUB_MONTH_3 = "3"; // 3月
    String PUB_MONTH_4 = "4"; // 4月
    String PUB_MONTH_5 = "5"; // 5月
    String PUB_MONTH_6 = "6"; // 6月
    String PUB_MONTH_7 = "7"; // 7月
    String PUB_MONTH_8 = "8"; // 8月
    String PUB_MONTH_9 = "9"; // 9月
    String PUB_MONTH_10 = "10"; // 10月
    String PUB_MONTH_11 = "11"; // 11月
    String PUB_MONTH_12 = "12"; // 12月
    // 开启关闭
    String PUB_ON_OFF = "PUB_ON_OFF"; // 开启关闭
    String PUB_ON_OFF_ON = "ON"; // 开启
    String PUB_ON_OFF_OFF = "OFF"; // 关闭
    // 季度
    String PUB_QUARTER = "PUB_QUARTER"; // 季度
    String PUB_QUARTER_1 = "1"; // 1季度
    String PUB_QUARTER_2 = "2"; // 2季度
    String PUB_QUARTER_3 = "3"; // 3季度
    String PUB_QUARTER_4 = "4"; // 4季度
    // 启动暂停
    String PUB_START_STOP = "PUB_START_STOP"; // 启动暂停
    String PUB_START_STOP_START = "START"; // 任务调度启动
    String PUB_START_STOP_STOP = "STOP"; // 任务调度停止
    // 成功失败
    String PUB_SUCESS_FAIL = "PUB_SUCESS_FAIL"; // 成功失败
    String PUB_SUCESS_FAIL_SUCCESS = "SUCCESS"; // 成功
    String PUB_SUCESS_FAIL_FAIL = "FAIL"; // 失败
    // 系统模块类别
    String PUB_SYS_MODULE = "PUB_SYS_MODULE"; // 系统模块类别
    String PUB_SYS_MODULE_BUD = "BUD"; // 预算管理模块
    String PUB_SYS_MODULE_PROJ = "PROJ"; // 项目管理模块
    String PUB_SYS_MODULE_PUR = "PUR"; // 采购管理模块
    String PUB_SYS_MODULE_INV = "INV"; // 库存管理模块
    String PUB_SYS_MODULE_CONT = "CONT"; // 合同管理模块
    String PUB_SYS_MODULE_EA = "EA"; // 报账管理模块
    String PUB_SYS_MODULE_SYS = "SYS"; // 系统管理模块
    String PUB_SYS_MODULE_VMS = "VMS"; // 供应商管理
    // 生效失效
    String PUB_VALID_INVALID = "PUB_VALID_INVALID"; // 生效失效
    String PUB_VALID_INVALID_VALID = "VALID"; // 生效
    String PUB_VALID_INVALID_INVALID = "INVALID"; // 失效
    // 仓库属性
    String PUB_WAREHOUSE_ATTRIBUTE = "PUB_WAREHOUSE_ATTRIBUTE"; // 仓库属性
    String PUB_WAREHOUSE_ATTRIBUTE_INVENTORY = "INVENTORY"; // 库存
    String PUB_WAREHOUSE_ATTRIBUTE_EXPENSE = "EXPENSE"; // 费用
    // 审批流程状态
    String PUB_WF_STATUS = "PUB_WF_STATUS"; // 审批流程状态
    String PUB_WF_STATUS_PREPARING = "PREPARING"; // 草稿
    String PUB_WF_STATUS_INPROCESS = "INPROCESS"; // 审批中
    String PUB_WF_STATUS_APROVED = "APROVED"; // 审批完成
    String PUB_WF_STATUS_CANCELED = "CANCELED"; // 撤销
    // 是否
    String PUB_Y_N = "PUB_Y_N"; // 是否
    String PUB_Y_N_Y = "Y"; // 是
    String PUB_Y_N_N = "N"; // 否
    String SYS_ORDER_TYPE_MEP = "MEP"; // 空卡写卡单
    // ===End=== ================ [公共模块] ================
    // ==Start== ================ [预算模块] ================
    // 预算不含审批状态
    String BUD_ANNUAL_STATUS = "BUD_ANNUAL_STATUS"; // 预算不含审批状态
    String BUD_ANNUAL_STATUS_DRAFT = "DRAFT"; // 草稿
    String BUD_ANNUAL_STATUS_CONFIRM = "CONFIRM"; // 已确认
    // 日常项目状态
    String BUD_DAILY_PROJECT_STATUS = "BUD_DAILY_PROJECT_STATUS"; // 日常项目状态
    String BUD_DAILY_PROJECT_STATUS_DRAFT = "DRAFT"; // 草稿
    String BUD_DAILY_PROJECT_STATUS_INPROCESS = "INPROCESS"; // 审批中
    String BUD_DAILY_PROJECT_STATUS_APPROVED = "APPROVED"; // 已审批
    String BUD_DAILY_PROJECT_STATUS_CANCELED = "CANCELED"; // 撤销审批
    String BUD_DAILY_PROJECT_STATUS_EXECUTION = "EXECUTION"; // 执行中
    String BUD_DAILY_PROJECT_STATUS_CLOSED = "CLOSED"; // 关闭
    // 部门调整审批状态
    String BUD_DEPT_STATUS = "BUD_DEPT_STATUS"; // 部门调整审批状态
    String BUD_DEPT_STATUS_DRAFT = "DRAFT"; // 草稿
    String BUD_DEPT_STATUS_INPROCESS = "INPROCESS"; // 审批中
    String BUD_DEPT_STATUS_APPROVED = "APPROVED"; // 已审批
    String BUD_DEPT_STATUS_CANCELED = "CANCELED"; // 撤销审批
    // 维度类型
    String BUD_DIMENSION_TYPE = "BUD_DIMENSION_TYPE"; // 维度类型
    String BUD_DIMENSION_TYPE_BUDGET_ACCOUNT = "BUDGET_ACCOUNT"; // 预算科目
    String BUD_DIMENSION_TYPE_OPERATING_ACTIVITY = "OPERATING_ACTIVITY"; // 业务活动
    String BUD_DIMENSION_TYPE_ACCOUNTING_SUBJECT = "ACCOUNTING_SUBJECT"; // 会计科目
    String BUD_DIMENSION_TYPE_TRANSACTION = "TRANSACTION"; // 交易类型
    String BUD_DIMENSION_TYPE_NETWORK = "NETWORK"; // 网络
    String BUD_DIMENSION_TYPE_BUDGET_GROUP = "BUDGET_GROUP"; // 预算组别
    String BUD_DIMENSION_TYPE_SUB_ACCOUNTING = "SUB_ACCOUNTING"; // 会计子目
    // 预算模板列类型
    String BUD_LABEL_COLUM_TYPE = "BUD_LABEL_COLUM_TYPE"; // 预算模板列类型
    String BUD_LABEL_COLUM_TYPE_DIMENSION = "DIMENSION"; // 维度
    String BUD_LABEL_COLUM_TYPE_SCENE = "SCENE"; // 情景
    String BUD_LABEL_COLUM_TYPE_TITLE = "TITLE"; // 标题
    String BUD_LABEL_COLUM_TYPE_NUM = "NUM"; // 数值
    // 预算模板情景
    String BUD_LABEL_SCENE = "BUD_LABEL_SCENE"; // 预算模板情景
    String BUD_LABEL_SCENE_MONTH1_AMOUNT = "MONTH1_AMOUNT"; // 一月
    String BUD_LABEL_SCENE_MONTH2_AMOUNT = "MONTH2_AMOUNT"; // 二月
    String BUD_LABEL_SCENE_MONTH3_AMOUNT = "MONTH3_AMOUNT"; // 三月
    String BUD_LABEL_SCENE_MONTH4_AMOUNT = "MONTH4_AMOUNT"; // 四月
    String BUD_LABEL_SCENE_MONTH5_AMOUNT = "MONTH5_AMOUNT"; // 五月
    String BUD_LABEL_SCENE_MONTH6_AMOUNT = "MONTH6_AMOUNT"; // 六月
    String BUD_LABEL_SCENE_MONTH7_AMOUNT = "MONTH7_AMOUNT"; // 七月
    String BUD_LABEL_SCENE_MONTH8_AMOUNT = "MONTH8_AMOUNT"; // 八月
    String BUD_LABEL_SCENE_MONTH9_AMOUNT = "MONTH9_AMOUNT"; // 九月
    String BUD_LABEL_SCENE_MONTH10_AMOUNT = "MONTH10_AMOUNT"; // 十月
    String BUD_LABEL_SCENE_MONTH11_AMOUNT = "MONTH11_AMOUNT"; // 十一月
    String BUD_LABEL_SCENE_MONTH12_AMOUNT = "MONTH12_AMOUNT"; // 十二月
    // 预算模板状态
    String BUD_TEMPLATE_STATUS = "BUD_TEMPLATE_STATUS"; // 预算模板状态
    String BUD_TEMPLATE_STATUS_DRAFT = "DRAFT"; // 草稿
    String BUD_TEMPLATE_STATUS_GENERATED = "GENERATED"; // 已生成
    String BUD_TEMPLATE_STATUS_USED = "USED"; // 已使用
    // 预算模板类型
    String BUD_TEMPLATE_TYPE = "BUD_TEMPLATE_TYPE"; // 预算模板类型
    String BUD_TEMPLATE_TYPE_REPORT = "REPORT"; // 上报
    String BUD_TEMPLATE_TYPE_ISSUED = "ISSUED"; // 下达
    String BUD_TEMPLATE_TYPE_ADJUSTMENT = "ADJUSTMENT"; // 跨年申请
    String BUD_TEMPLATE_TYPE_CUMULATIVE = "CUMULATIVE"; // 集团上报
    // ===End=== ================ [预算模块] ================
    // ==Start== ================ [项目模块] ================
    // 计划类型
    String PLAN_TYPE = "PLAN_TYPE"; // 计划类型
    String PLAN_TYPE_Y = "Y"; // 计划内
    String PLAN_TYPE_N = "N"; // 计划外
    // 验收类型
    String PMS_ACCEPTANCE_TYPE = "PMS_ACCEPTANCE_TYPE"; // 验收类型
    String PMS_ACCEPTANCE_TYPE_TESTRUN = "TESTRUN"; // 试运行
    String PMS_ACCEPTANCE_TYPE_INITIALRUN = "INITIALRUN"; // 初验
    String PMS_ACCEPTANCE_TYPE_FINALRUN = "FINALRUN"; // 终验
    // 年度投资计划版本
    String PMS_ANNUAL_INVEST_HEADER_VERSION = "PMS_ANNUAL_INVEST_HEADER_VERSION"; // 年度投资计划版本
    String PMS_ANNUAL_INVEST_HEADER_VERSION_REPORT = "REPORT"; // 年初上报
    String PMS_ANNUAL_INVEST_HEADER_VERSION_CONFIRM = "CONFIRM"; // 集团下达
    String PMS_ANNUAL_INVEST_HEADER_VERSION_ADJUSTMENT_REPORT = "ADJUSTMENT REPORT"; // 年中调整上报
    String PMS_ANNUAL_INVEST_HEADER_VERSION_ADJUSTMENT_CONFIRM = "ADJUSTMENT CONFIRM"; // 集团调整下达
    // 投资状态
    String PMS_ANNUAL_STATUS = "PMS_ANNUAL_STATUS"; // 投资状态
    String PMS_ANNUAL_STATUS_COMPILATION = "COMPILATION"; // 编制中
    String PMS_ANNUAL_STATUS_CONFIRMED = "CONFIRMED"; // 已确认
    // 单据审批状态
    String PMS_APPROVE_STATUS = "PMS_APPROVE_STATUS"; // 单据审批状态
    String PMS_APPROVE_STATUS_DRAFT = "DRAFT"; // 编制中
    String PMS_APPROVE_STATUS_INPROCESS = "INPROCESS"; // 审批中
    String PMS_APPROVE_STATUS_COMPLETED = "COMPLETED"; // 已审批
    String PMS_APPROVE_STATUS_CLOSED = "CLOSED"; // 已关闭
    // 项目分类级别
    String PMS_CATEGORY_LEVEL = "PMS_CATEGORY_LEVEL"; // 项目分类级别
    String PMS_CATEGORY_LEVEL_1 = "1"; // 一级
    String PMS_CATEGORY_LEVEL_2 = "2"; // 二级
    String PMS_CATEGORY_LEVEL_3 = "3"; // 三级
    String PMS_CATEGORY_LEVEL_4 = "4"; // 四级
    // 地点类型
    String PMS_LOCATION_TYPE = "PMS_LOCATION_TYPE"; // 地点类型
    String PMS_LOCATION_TYPE_BASE_STATION = "BASE_STATION"; // 基站
    String PMS_LOCATION_TYPE_REPERTORY = "REPERTORY"; // 仓库
    String PMS_LOCATION_TYPE_SHOP = "SHOP"; // 营业厅
    String PMS_LOCATION_TYPE_OFFICE = "OFFICE"; // 办公室
    String PMS_LOCATION_TYPE_COUNTRIES = "COUNTRIES"; // 国家
    // 项目计划上报版本
    String PMS_MONTH_VERSION = "PMS_MONTH_VERSION"; // 项目计划上报版本
    String PMS_MONTH_VERSION_8 = "8"; // 8月版
    String PMS_MONTH_VERSION_11 = "11"; // 11月版
    String PMS_MONTH_VERSION_5 = "5"; // 5月调整版
    // 项目级别
    String PMS_PROJECT_LEVEL = "PMS_PROJECT_LEVEL"; // 项目级别
    String PMS_PROJECT_LEVEL_1 = "1"; // 一类项目
    String PMS_PROJECT_LEVEL_2 = "2"; // 二类项目
    String PMS_PROJECT_LEVEL_3 = "3"; // 三类项目
    // 项目计划上报类型
    String PMS_PROJECT_PLAN_REPORT_TYPE = "PMS_PROJECT_PLAN_REPORT_TYPE"; // 项目计划上报类型
    String PMS_PROJECT_PLAN_REPORT_TYPE_NEW = "NEW"; // 新增
    String PMS_PROJECT_PLAN_REPORT_TYPE_ADJUST = "ADJUST"; // 调整
    // 年度投资计划项目集新增类型
    String PMS_PROJECT_SET = "PMS_PROJECT_SET"; // 年度投资计划项目集新增类型
    String PMS_PROJECT_SET_NEW = "NEW"; // 新建
    String PMS_PROJECT_SET_CONTINUE = "CONTINUE"; // 续建
    String PMS_PROJECT_SET_ARRANGEMENT = "ARRANGEMENT"; // 自主安排
    String PMS_PROJECT_SET_PURCHASE = "PURCHASE"; // 零星购置
    // 项目上报状态
    String PMS_REPORT_STATUS = "PMS_REPORT_STATUS"; // 项目上报状态
    String PMS_REPORT_STATUS_COMPILATION = "COMPILATION"; // 项目上报状态
    String PMS_REPORT_STATUS_REPORTED = "REPORTED"; // 项目上报状态
    // 项目任务分解状态
    String PMS_TASK_STATUS = "PMS_TASK_STATUS"; // 项目任务分解状态
    String PMS_TASK_STATUS_UNCONFIRMED = "UNCONFIRMED"; // 未确认
    String PMS_TASK_STATUS_CONFIRMED = "CONFIRMED"; // 确认
    // 项目计划新增类型
    String PROJECT_DATA_TYPE = "PROJECT_DATA_TYPE"; // 项目计划新增类型
    String PROJECT_DATA_TYPE_NS = "NS"; // 新建
    String PROJECT_DATA_TYPE_RC = "RC"; // 续建
    // ===End=== ================ [项目模块] ================
    // ==Start== ================ [采购模块] ================
    // 是否费用物料
    String BS_FEE_FLAG = "BS_FEE_FLAG"; // 申请物料用
    String BS_FEE_FLAG_N = "N"; // 申请物料用
    String BS_FEE_FLAG_Y = "Y"; // 申请物料用
    // 采购项目状态
    String PP_STATUS = "PP_STATUS"; // 采购项目状态
    String PP_STATUS_PREPARING = "PREPARING"; // 草稿
    String PP_STATUS_CGFNSHZ = "CGFNSHZ"; // 采购方案审核中
    String PP_STATUS_CGFNTG = "CGFNTG"; // 采购方案通过
    String PP_STATUS_COMPLETED = "COMPLETED"; // 已完成
    String PP_STATUS_CGFNWTG = "CGFNWTG"; // 采购方案未通过
    String PP_STATUS_CGJGWTG = "CGJGWTG"; // 采购结果未通过
    String PP_STATUS_CGJGSHZ = "CGJGSHZ"; // 采购结果审核中
    // 供应商评估-评估状态
    String PUR_ASSESS_STATUS = "PUR_ASSESS_STATUS"; // 供应商评估-评估状态
    String PUR_ASSESS_STATUS_NOT_EVALUATED = "NOT_EVALUATED"; // 未评估
    String PUR_ASSESS_STATUS_ASSESSMENT = "ASSESSMENT"; // 评估中
    String PUR_ASSESS_STATUS_EVALUATION_COMPLETION = "EVALUATION_COMPLETION"; // 评估完成
    // 供应商注册-公司类别
    String PUR_COMPANY_CATEGORY = "PUR_COMPANY_CATEGORY"; // 供应商注册-公司类别
    String PUR_COMPANY_CATEGORY_LIMITED_COMPANY = "LIMITED_COMPANY"; // 有限公司Limited Company
    String PUR_COMPANY_CATEGORY_UNLIMITED_COMPANY = "UNLIMITED_COMPANY"; // 无限公司Unlimited Company
    String PUR_COMPANY_CATEGORY_OTHERS = "OTHERS"; // 其他Others
    // 供应商注册-公司类型
    String PUR_COMPANY_TYPE = "PUR_COMPANY_TYPE"; // 供应商注册-公司类型
    String PUR_COMPANY_TYPE_PUBLIC_COMPANY = "PUBLIC_COMPANY"; // 公开公司Public Company
    String PUR_COMPANY_TYPE_LISTED_COMPANY = "LISTED_COMPANY"; // 上市公司Listed Company
    String PUR_COMPANY_TYPE_PRIVATE_COMPANY = "PRIVATE_COMPANY"; // 私人公司Private Company
    String PUR_COMPANY_TYPE_PARTNERSHIP = "PARTNERSHIP"; // 合伙公司Partnership
    String PUR_COMPANY_TYPE_SOLE_PROPRIETORSHIP = "SOLE_PROPRIETORSHIP"; // 独资公司Sole Proprietorship
    String PUR_COMPANY_TYPE_OTHERS = "OTHERS"; // 其他Others
    // 供应商评估-保密类型
    String PUR_CONFIDENTIAL_TYPE = "PUR_CONFIDENTIAL_TYPE"; // 供应商评估-保密类型
    String PUR_CONFIDENTIAL_TYPE_INTERNAL_USE = "INTERNAL_USE"; // 内部使用
    String PUR_CONFIDENTIAL_TYPE_EXTERNAL_USE = "EXTERNAL_USE"; // 外部使用
    // 物料新增默认生效
    String PUR_DEFAULT_VALID = "PUR_DEFAULT_VALID"; // 物料新增默认生效
    String PUR_DEFAULT_VALID_VALID = "VALID"; // 生效
    // 物料申请-ERP物料属性模板
    String PUR_ERP_ITEM_TEMPLATE = "PUR_ERP_ITEM_TEMPLATE"; // 物料申请-ERP物料属性模板
    String PUR_ERP_ITEM_TEMPLATE_CAPEX_MATERIEL = "CAPEX_MATERIEL"; // 工程建设相关物资（包括：主设备、配置设备、辅材）
    String PUR_ERP_ITEM_TEMPLATE_CAPEX_EXPENSE = "CAPEX_EXPENSE"; // 工程建设相关费用（如：设计费、征地费、施工费、监理费等）
    String PUR_ERP_ITEM_TEMPLATE_SPARE_PARTS = "SPARE_PARTS"; // 网络运维相关物资（包括：主设备、配置设备、辅材）
    String PUR_ERP_ITEM_TEMPLATE_BUSINESS_MATERIEL = "BUSINESS_MATERIEL"; // 市场营销相关物资（包括：手机、储值卡等）
    String PUR_ERP_ITEM_TEMPLATE_MISC_MATERIEL = "MISC_MATERIEL"; // 低值易耗类物资（包括：日常办公文具用品、茶水间用品等低价值消耗品）
    String PUR_ERP_ITEM_TEMPLATE_MINOR_PURCHASE = "MINOR_PURCHASE"; // 零星购置固定资产（包括：办公桌椅、电脑、空调等归入固定资产管理的物品）
    // 费用采购退货状态
    String PUR_EXP_RTN_DOCUMENT_STATUS = "PUR_EXP_RTN_DOCUMENT_STATUS"; // 费用采购退货状态
    String PUR_EXP_RTN_DOCUMENT_STATUS_PREPARING = "PREPARING"; // 编制中
    String PUR_EXP_RTN_DOCUMENT_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String PUR_EXP_RTN_DOCUMENT_STATUS_APPROVED = "APPROVED"; // 已审批
    String PUR_EXP_RTN_DOCUMENT_STATUS_COMPLETED = "COMPLETED"; // 已完成
    // 框架协议状态
    String PUR_FRAME_STATUS = "PUR_FRAME_STATUS"; // 框架协议状态
    String PUR_FRAME_STATUS_VALID = "VALID"; // 框架协议状态
    String PUR_FRAME_STATUS_STOP = "STOP"; // 框架协议状态
    String PUR_FRAME_STATUS_OFF = "OFF"; // 框架协议状态
    String PUR_FRAME_STATUS_CANCEL = "CANCEL"; // 框架协议状态
    // 是否库存管理
    String PUR_ITEM_FEEFLAG = "PUR_ITEM_FEEFLAG"; // 是否库存管理
    String PUR_ITEM_FEEFLAG_N = "N"; // 否
    // 物料编码申请来源
    String PUR_ITEM_FROM = "PUR_ITEM_FROM"; // 物料编码申请来源
    String PUR_ITEM_FROM_EXT = "EXT"; // EXT
    String PUR_ITEM_FROM_IM = "IM"; // IM
    // 物料申请单状态
    String PUR_ITEM_REQUISITION_STATUS = "PUR_ITEM_REQUISITION_STATUS"; // 物料申请单状态
    String PUR_ITEM_REQUISITION_STATUS_INPUT = "INPUT"; // 录入
    String PUR_ITEM_REQUISITION_STATUS_APPROVAL = "APPROVAL"; // 审批中
    String PUR_ITEM_REQUISITION_STATUS_IMPORT_ERP = "IMPORT_ERP"; // 导入ERP
    String PUR_ITEM_REQUISITION_STATUS_IMPORT_ERP_FAILED = "IMPORT_ERP_FAILED"; // 导入ERP失败
    // 付款条件
    String PUR_PAYMENT_TERM = "PUR_PAYMENT_TERM"; // 付款条件
    String PUR_PAYMENT_TERM_30 = "30"; // 30 days credit
    String PUR_PAYMENT_TERM_45 = "45"; // 45 days credit
    String PUR_PAYMENT_TERM_60 = "60"; // 60 days credit
    String PUR_PAYMENT_TERM_OTHER = "OTHER"; // Others
    // 付款方式
    String PUR_PAYMENT_TYPE = "PUR_PAYMENT_TYPE"; // 付款方式
    String PUR_PAYMENT_TYPE_P_CASH = "P_CASH"; // 现金
    String PUR_PAYMENT_TYPE_P_AUTOPAY = "P_AUTOPAY"; // 自动转账
    String PUR_PAYMENT_TYPE_P_BANK = "P_BANK"; // 银行本票
    String PUR_PAYMENT_TYPE_P_ONETIME_PAYMENT = "P_ONETIME_PAYMENT"; // 支票（一次性付款）
    String PUR_PAYMENT_TYPE_P_PAYMENT_REQUEST = "P_PAYMENT_REQUEST"; // 支票（支付请求）
    String PUR_PAYMENT_TYPE_P_MONTHLY_PAYMENT = "P_MONTHLY_PAYMENT"; // 支票（每月付款）
    String PUR_PAYMENT_TYPE_P_TELEGRAPHIC = "P_TELEGRAPHIC"; // 电汇
    String PUR_PAYMENT_TYPE_P_NA = "P_NA"; // 不适用
    // 采购计划下发周期
    String PUR_PL_ISSUE_TYPE = "PUR_PL_ISSUE_TYPE"; // 采购计划下发周期
    String PUR_PL_ISSUE_TYPE_QUARTER = "QUARTER"; // 季度
    String PUR_PL_ISSUE_TYPE_YEAR = "YEAR"; // 年度
    // 采购计划填报状态
    String PUR_PLAN_FILL_STATUS = "PUR_PLAN_FILL_STATUS"; // 采购计划填报状态
    String PUR_PLAN_FILL_STATUS_FILL_ALL = "FILL_ALL"; // 采购计划全部完成填报
    String PUR_PLAN_FILL_STATUS_FILL_PART = "FILL_PART"; // 采购计划部分填报完成
    String PUR_PLAN_FILL_STATUS_FILL_NULL = "FILL_NULL"; // 采购计划全部未填报
    // 采购计划预算追加单状态
    String PUR_PLAN_FILL_STATUS_ADD = "PUR_PLAN_FILL_STATUS_ADD"; // 采购计划预算追加单状态
    String PUR_PLAN_FILL_STATUS_ADD_NOTFILLIN = "NOTFILLIN"; // 未填报
    String PUR_PLAN_FILL_STATUS_ADD_APPROVALING = "APPROVALING"; // 审批中
    String PUR_PLAN_FILL_STATUS_ADD_FILLCOMPLETED = "FILLCOMPLETED"; // 填报完成
    // 采购计划填报审批状态
    String PUR_PLAN_FILL_STATUS_WF = "PUR_PLAN_FILL_STATUS_WF"; // 采购计划填报审批状态
    String PUR_PLAN_FILL_STATUS_WF_PREPARING = "PREPARING"; // 未填报
    String PUR_PLAN_FILL_STATUS_WF_INPROCESS = "INPROCESS"; // 审批中
    String PUR_PLAN_FILL_STATUS_WF_COMPLETED = "COMPLETED"; // 审批完成
    String PUR_PLAN_FILL_STATUS_WF_FILLCOMPLETED = "FILLCOMPLETED"; // 填报完成
    // 寄售订单状态
    String PUR_PO_CONSIGN_STATUS = "PUR_PO_CONSIGN_STATUS"; // 寄售订单状态
    String PUR_PO_CONSIGN_STATUS_PREPARING = "PREPARING"; // 草稿
    String PUR_PO_CONSIGN_STATUS_INPROCESS = "INPROCESS"; // 审批中
    String PUR_PO_CONSIGN_STATUS_APPROVED = "APPROVED"; // 审批完成
    String PUR_PO_CONSIGN_STATUS_MODIFYING = "MODIFYING"; // 修改中
    // 手机订单状态
    String PUR_PO_HANDSET_STATUS = "PUR_PO_HANDSET_STATUS"; // 手机订单状态
    String PUR_PO_HANDSET_STATUS_PREPARING = "PREPARING"; // 草稿
    String PUR_PO_HANDSET_STATUS_INPROCESS = "INPROCESS"; // 审批中
    String PUR_PO_HANDSET_STATUS_APPROVED = "APPROVED"; // 审批完成
    String PUR_PO_HANDSET_STATUS_CANCELED = "CANCELED"; // 已取消
    String PUR_PO_HANDSET_STATUS_CLOSED = "CLOSED"; // 已关闭
    String PUR_PO_HANDSET_STATUS_MODIFYING = "MODIFYING"; // 手机订单状态
    // 订单退单类型
    String PUR_PO_RTN_TYPE = "PUR_PO_RTN_TYPE"; // 订单退单类型
    String PUR_PO_RTN_TYPE_PO_CANCEL = "PO_CANCEL"; // 订单取消
    String PUR_PO_RTN_TYPE_PO_UPDATE = "PO_UPDATE"; // 订单修改
    // 采购订单来源类型
    String PUR_PO_SOURCE_TYPE = "PUR_PO_SOURCE_TYPE"; // 采购订单来源类型
    String PUR_PO_SOURCE_TYPE_PR = "PR"; // 采购申请
    String PUR_PO_SOURCE_TYPE_PP = "PP"; // 采购项目
    String PUR_PO_SOURCE_TYPE_AE = "AE"; // 框架执行
    // 采购订单状态
    String PUR_PO_STATUS = "PUR_PO_STATUS"; // 采购订单状态
    String PUR_PO_STATUS_PREPARING = "PREPARING"; // 草稿
    String PUR_PO_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String PUR_PO_STATUS_APPROVED = "APPROVED"; // 审批完成
    String PUR_PO_STATUS_COMPLETED = "COMPLETED"; // 已完成
    String PUR_PO_STATUS_MODIFYING = "MODIFYING"; // 修改中
    String PUR_PO_STATUS_CANCELED = "CANCELED"; // 已取消
    String PUR_PO_STATUS_CLOSED = "CLOSED"; // 已关闭
    // 采购订单修改申请状态
    String PUR_PO_UPDATE_APPLY_STATUS = "PUR_PO_UPDATE_APPLY_STATUS"; // 采购订单修改申请状态
    String PUR_PO_UPDATE_APPLY_STATUS_PREPARING = "PREPARING"; // 草稿
    String PUR_PO_UPDATE_APPLY_STATUS_INPROCESS = "INPROCESS"; // 審批中
    String PUR_PO_UPDATE_APPLY_STATUS_APPROVED = "APPROVED"; // 審批完畢
    String PUR_PO_UPDATE_APPLY_STATUS_MODIFYING = "MODIFYING"; // 修改中
    String PUR_PO_UPDATE_APPLY_STATUS_MODIFYED = "MODIFYED"; // 修改完成
    // 采购决策层级
    String PUR_PP_DECISION_LEVEL = "PUR_PP_DECISION_LEVEL"; // 采购决策层级
    String PUR_PP_DECISION_LEVEL_COLLECTIVE = "COLLECTIVE"; // 集体决策
    String PUR_PP_DECISION_LEVEL_MANAGEMENT = "MANAGEMENT"; // 管理层决策
    String PUR_PP_DECISION_LEVEL_DEPARTMENT = "DEPARTMENT"; // 部门决策
    // 采购方式
    String PUR_PP_PURCHASE_METHOD = "PUR_PP_PURCHASE_METHOD"; // 采购方式
    String PUR_PP_PURCHASE_METHOD_OPEN_TENDER = "OPEN_TENDER"; // 公开招标
    String PUR_PP_PURCHASE_METHOD_INVITE_TENDER = "INVITE_TENDER"; // 邀请招标
    String PUR_PP_PURCHASE_METHOD_OMPETITIVE_NEGOTIATION = "OMPETITIVE_NEGOTIATION"; // 直接谈判
    String PUR_PP_PURCHASE_METHOD_INQUIRY = "INQUIRY"; // 询价
    String PUR_PP_PURCHASE_METHOD_SINGLE_SOURCE = "SINGLE_SOURCE"; // 单一来源
    // 供应商评估-售前服务
    String PUR_PRE_SALES_SERVICE = "PUR_PRE_SALES_SERVICE"; // 供应商评估-售前服务
    String PUR_PRE_SALES_SERVICE_UNACCEPTABLE = "UNACCEPTABLE"; // 0 Unacceptable 不能接受
    String PUR_PRE_SALES_SERVICE_BAD = "BAD"; // 1 Bad 差
    String PUR_PRE_SALES_SERVICE_FAIR = "FAIR"; // 2 Fair  稍差
    String PUR_PRE_SALES_SERVICE_ACCEPTABLE = "ACCEPTABLE"; // 3 Acceptable 尚可
    String PUR_PRE_SALES_SERVICE_GOOD = "GOOD"; // 4 Good 良好
    String PUR_PRE_SALES_SERVICE_EXCELLENT = "EXCELLENT"; // 5 Excellent 优秀
    // 供应商评估-售前服务及支持
    String PUR_PRE_SALES_SUPPORT = "PUR_PRE_SALES_SUPPORT"; // 供应商评估-售前服务及支持
    String PUR_PRE_SALES_SUPPORT_UNACCEPTABLE = "UNACCEPTABLE"; // 0 Unacceptable 不能接受
    String PUR_PRE_SALES_SUPPORT_BAD = "BAD"; // 1 Bad 未达到期望
    String PUR_PRE_SALES_SUPPORT_ACCEPTABLE = "ACCEPTABLE"; // 3 Acceptable 达到期望
    String PUR_PRE_SALES_SUPPORT_GOOD = "GOOD"; // 4 Good 优于期望
    String PUR_PRE_SALES_SUPPORT_FAIR = "FAIR"; // 2 Fair 稍低于期望
    String PUR_PRE_SALES_SUPPORT_EXCELLENT = "EXCELLENT"; // 5 Excellent 超出期望
    // 供应商评估-采购类型
    String PUR_PROCUREMENT_TYPE = "PUR_PROCUREMENT_TYPE"; // 供应商评估-采购类型
    String PUR_PROCUREMENT_TYPE_NETWORK = "NETWORK"; // 网络采购Network Procurement
    String PUR_PROCUREMENT_TYPE_NON_NETWORD = "NON_NETWORD"; // 非网络采购Non-Network Procurement
    // 采购类别
    String PUR_PURCHASE_CATEGORY = "PUR_PURCHASE_CATEGORY"; // 采购类别
    String PUR_PURCHASE_CATEGORY_HANDSET = "HANDSET"; // 手机
    String PUR_PURCHASE_CATEGORY_INVENTORY_ITEM = "INVENTORY_ITEM"; // 销售类
    String PUR_PURCHASE_CATEGORY_MARKETING = "MARKETING"; // 市场推广
    String PUR_PURCHASE_CATEGORY_NETWORK_PROCUREMENT = "NETWORK_PROCUREMENT"; // 网络采购
    String PUR_PURCHASE_CATEGORY_CELL_SITE = "CELL_SITE"; // 基站建设
    String PUR_PURCHASE_CATEGORY_NON_NETWORK = "NON-NETWORK"; // 非网络采购
    String PUR_PURCHASE_CATEGORY_IT_RELATED = "IT_RELATED"; // IT相关采购
    String PUR_PURCHASE_CATEGORY_ORTHERS = "ORTHERS"; // 其它
    String PUR_PURCHASE_CATEGORY_FEE = "FEE"; // 费用
    String PUR_PURCHASE_CATEGORY_SIM_HANDSET_ACCESSORIES = "SIM_HANDSET_ACCESSORIES"; // SIM卡及手机配件
    // 报价单状态
    String PUR_QUOTATION_STATUS = "PUR_QUOTATION_STATUS"; // 报价单状态
    String PUR_QUOTATION_STATUS_PREPARE = "PREPARE"; // 未报价
    String PUR_QUOTATION_STATUS_COMPLETED = "COMPLETED"; // 已报价
    String PUR_QUOTATION_STATUS_EXPIRED = "EXPIRED"; // 已过期
    // 采购退货状态
    String PUR_RTN_DOC_STATUS = "PUR_RTN_DOC_STATUS"; // 采购退货状态
    String PUR_RTN_DOC_STATUS_PREPARING = "PREPARING"; // 草稿
    String PUR_RTN_DOC_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String PUR_RTN_DOC_STATUS_COMPLETED = "COMPLETED"; // 已完成
    String PUR_RTN_DOC_STATUS_APPROVED = "APPROVED"; // 已审批
    // 供应商评估--供应商等级
    String PUR_SUPPLIER_ASSESSEMENT_LEVEL = "PUR_SUPPLIER_ASSESSEMENT_LEVEL"; // 供应商评估--供应商等级
    String PUR_SUPPLIER_ASSESSEMENT_LEVEL_A = "A"; // A >=85%
    String PUR_SUPPLIER_ASSESSEMENT_LEVEL_B = "B"; // B >=75%
    String PUR_SUPPLIER_ASSESSEMENT_LEVEL_C = "C"; // C >=60%
    String PUR_SUPPLIER_ASSESSEMENT_LEVEL_D = "D"; // D >=50%
    String PUR_SUPPLIER_ASSESSEMENT_LEVEL_E = "E"; // E UNDER 50%
    // 供应商评估-评估结果
    String PUR_SUPPLIER_ASSESSMENT_RESULT = "PUR_SUPPLIER_ASSESSMENT_RESULT"; // 供应商评估-评估结果
    String PUR_SUPPLIER_ASSESSMENT_RESULT_EXCELLENT = "EXCELLENT"; // 优秀Excellent (分数score 90-100)
    String PUR_SUPPLIER_ASSESSMENT_RESULT_COOD = "COOD"; // 好Good (分数score 70-89)
    String PUR_SUPPLIER_ASSESSMENT_RESULT_ACCEPTABLE = "ACCEPTABLE"; // 可接受Acceptable (分数score 50-69)
    String PUR_SUPPLIER_ASSESSMENT_RESULT_UNACCEPTABLE = "UNACCEPTABLE"; // 不能接受Unacceptable (分数score 0-49)
    // 供应商状态
    String PUR_SUPPLIER_STATUS = "PUR_SUPPLIER_STATUS"; // 供应商状态
    String PUR_SUPPLIER_STATUS_PROBATION = "PROBATION"; // 试用期
    String PUR_SUPPLIER_STATUS_QUALIFIED = "QUALIFIED"; // 合格
    String PUR_SUPPLIER_STATUS_LNACTIVE = "LNACTIVE"; // 不活跃
    String PUR_SUPPLIER_STATUS_EXPULSION = "EXPULSION"; // 除名
    String PUR_SUPPLIER_STATUS_BLACKLIST = "BLACKLIST"; // 黑名单
    String PUR_SUPPLIER_STATUS_POTENTIAL = "POTENTIAL"; // 潜在期
    // 供应商类型
    String PUR_SUPPLIER_TYPE = "PUR_SUPPLIER_TYPE"; // 供应商类型
    String PUR_SUPPLIER_TYPE_ENTERPRISE_SUPPLIER = "ENTERPRISE_SUPPLIER"; // 企业供应商
    String PUR_SUPPLIER_TYPE_GOVERNMENT_PUBLIC_UTILITIES = "GOVERNMENT_PUBLIC_UTILITIES"; // 政府机构或公共事业
    String PUR_SUPPLIER_TYPE_HOTEL_CATERING_SERVICES = "HOTEL_CATERING_SERVICES"; // 酒店及餐饮服务
    String PUR_SUPPLIER_TYPE_RETAILER = "RETAILER"; // 零售商
    String PUR_SUPPLIER_TYPE_CMCC_SUBSIDIARIES = "CMCC_SUBSIDIARIES"; // 中国移动集团及其子公司
    // 用户物料类型
    String PUR_USER_ITEM_TYPE = "PUR_USER_ITEM_TYPE"; // 用户物料类型
    String PUR_USER_ITEM_TYPE_MX = "MX"; // 可以单独形成1条固定资产的物资，如：移动交换服务器
    String PUR_USER_ITEM_TYPE_NA = "NA"; // 可以按批形成1条固定资产的物资，如：管道光缆
    String PUR_USER_ITEM_TYPE_QA = "QA"; // 可与FA明细物料组合形成1条固定资产，如：综合配置机柜
    String PUR_USER_ITEM_TYPE_FA = "FA"; // 不能单独形成固定资产，需与FA汇总物料组合形成1条固定资产，如：机柜安装组件
    // ===End=== ================ [采购模块] ================
    // ==Start== ================ [库存模块] ================
    // 出库单状态
    String INV_ISSUE_DOCUMENT_STATUS = "INV_ISSUE_DOCUMENT_STATUS"; // 出库单状态
    String INV_ISSUE_DOCUMENT_STATUS_PREPARING = "PREPARING"; // 草稿
    String INV_ISSUE_DOCUMENT_STATUS_PARTIAL_RECEIVED = "PARTIAL_RECEIVED"; // 部分确认
    String INV_ISSUE_DOCUMENT_STATUS_COMPLETED = "COMPLETED"; // 已完成
    String INV_ISSUE_DOCUMENT_STATUS_SUBMITTED = "SUBMITTED"; // 已提交
    String INV_ISSUE_DOCUMENT_STATUS_ISSUED = "ISSUED"; // 已出库
    String INV_ISSUE_DOCUMENT_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String INV_ISSUE_DOCUMENT_STATUS_APPROVED = "APPROVED"; // 已审批
    String INV_ISSUE_DOCUMENT_STATUS_PARTIAL_ISSUED = "PARTIAL_ISSUED"; // 部分出库
    // 领料申请单状态
    String INV_ISSUE_ORDER_STATUS = "INV_ISSUE_ORDER_STATUS"; // 领料申请单状态
    String INV_ISSUE_ORDER_STATUS_PREPARING = "PREPARING"; // 草稿
    String INV_ISSUE_ORDER_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String INV_ISSUE_ORDER_STATUS_COMPLETED = "COMPLETED"; // 已完成
    // 库存组织分类
    String INV_ORG_TYPES = "INV_ORG_TYPES"; // 库存组织分类
    String INV_ORG_TYPES_10E = "10E"; // 工程物资
    String INV_ORG_TYPES_10C = "10C"; // 业务用品
    String INV_ORG_TYPES_10O = "10O"; // 杂品
    String INV_ORG_TYPES_10P = "10P"; // 备品备件
    String INV_ORG_TYPES_10S = "10S"; // 零购资产
    // 收货单-状态
    String INV_RCV_DOC_STATUS = "INV_RCV_DOC_STATUS"; // 收货单-状态
    String INV_RCV_DOC_STATUS_PREPARING = "PREPARING"; // 草稿
    String INV_RCV_DOC_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String INV_RCV_DOC_STATUS_COMPLETED = "COMPLETED"; // 已完成
    // 接收入库状态
    String INV_RCV_TRANSACTON_STATUS = "INV_RCV_TRANSACTON_STATUS"; // 接收入库状态
    String INV_RCV_TRANSACTON_STATUS_COMPLETED = "COMPLETED"; // 已完成
    String INV_RCV_TRANSACTON_STATUS_PREPARING = "PREPARING"; // 草稿
    String INV_RCV_TRANSACTON_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String INV_RCV_TRANSACTON_STATUS_REJECTED = "REJECTED"; // 拒绝
    String INV_RCV_TRANSACTON_STATUS_SUBJECT = "SUBJECT"; // 审批
    String INV_RCV_TRANSACTON_STATUS_RETURN = "RETURN"; // 退回
    // 退库单状态
    String INV_RTN_DOC_STATUS = "INV_RTN_DOC_STATUS"; // 退库单状态
    String INV_RTN_DOC_STATUS_PREPARING = "PREPARING"; // 编制中
    String INV_RTN_DOC_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String INV_RTN_DOC_STATUS_APPROVED = "APPROVED"; // 批准
    String INV_RTN_DOC_STATUS_COMPLETED = "COMPLETED"; // 已完成
    // 退库申请单状态
    String INV_RTN_REQ_DOC_STATUS = "INV_RTN_REQ_DOC_STATUS"; // 退库申请单状态
    String INV_RTN_REQ_DOC_STATUS_PREPARING = "PREPARING"; // 草稿
    String INV_RTN_REQ_DOC_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String INV_RTN_REQ_DOC_STATUS_APPROVED = "APPROVED"; // 批准
    String INV_RTN_REQ_DOC_STATUS_REJECTED = "REJECTED"; // 已拒绝
    String INV_RTN_REQ_DOC_STATUS_RETURNED = "RETURNED"; // 已退回
    String INV_RTN_REQ_DOC_STATUS_ABORTED = "ABORTED"; // 已取消
    // 退库类型
    String INV_RTN_TYPE = "INV_RTN_TYPE"; // 退库类型
    String INV_RTN_TYPE_PA_RTN = "PA_RTN"; // 项目退库
    String INV_RTN_TYPE_UN_PA_RTN = "UN_PA_RTN"; // 非项目退库
    // 转移单状态
    String INV_TR_REQUESTION_STATUS = "INV_TR_REQUESTION_STATUS"; // 转移单状态
    String INV_TR_REQUESTION_STATUS_PREPARING = "PREPARING"; // 草稿
    String INV_TR_REQUESTION_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String INV_TR_REQUESTION_STATUS_APPROVED = "APPROVED"; // 批准
    // 转移单转移类型
    String INV_TR_TRANSFER_TYPE = "INV_TR_TRANSFER_TYPE"; // 转移单转移类型
    String INV_TR_TRANSFER_TYPE_INNER_WAREHOUSE = "INNER_WAREHOUSE"; // 仓库内转移
    String INV_TR_TRANSFER_TYPE_ACROSS_WAREHOUSE = "ACROSS_WAREHOUSE"; // 仓库间转移
    String INV_TR_TRANSFER_TYPE_INNER_PROJECT = "INNER_PROJECT"; // 项目内转移
    String INV_TR_TRANSFER_TYPE_ACROSS_PROJECT = "ACROSS_PROJECT"; // 项目间转移
    String INV_TR_TRANSFER_TYPE_ACROSS_INV_ORG = "ACROSS_INV_ORG"; // 组织间转移
    String INV_TR_TRANSFER_TYPE_ACROSS_INV_ORG_EC = "ACROSS_INV_ORG_EC"; // 工程转维护
    // 采购仓储事务类型
    String INV_TRANSACTION_TYPE = "INV_TRANSACTION_TYPE"; // 采购仓储事务类型
    String INV_TRANSACTION_TYPE_10 = "10"; // 费用接收入库
    String INV_TRANSACTION_TYPE_11 = "11"; // 订单接收入库
    String INV_TRANSACTION_TYPE_13 = "13"; // 订单现场到货
    String INV_TRANSACTION_TYPE_15 = "15"; // 项目退库
    String INV_TRANSACTION_TYPE_16 = "16"; // 账户/账户别名退库
    String INV_TRANSACTION_TYPE_17 = "17"; // 账户别名入库
    String INV_TRANSACTION_TYPE_21 = "21"; // 订单退回供应商（采购退货）
    String INV_TRANSACTION_TYPE_22 = "22"; // 费用退回供应商
    String INV_TRANSACTION_TYPE_23 = "23"; // 订单现场发货
    String INV_TRANSACTION_TYPE_25 = "25"; // 项目领用
    String INV_TRANSACTION_TYPE_26 = "26"; // 账户别名领用
    String INV_TRANSACTION_TYPE_27 = "27"; // 报废出库
    String INV_TRANSACTION_TYPE_32 = "32"; // 仓库间转移
    String INV_TRANSACTION_TYPE_33 = "33"; // 项目间调拨事务
    String INV_TRANSACTION_TYPE_37 = "37"; // 组织调拨事务
    String INV_TRANSACTION_TYPE_38 = "38"; // 中转仓库转移
    // 仓库分类
    String INV_WAREHOUSE_TYPES = "INV_WAREHOUSE_TYPES"; // 仓库分类
    String INV_WAREHOUSE_TYPES_CONSIGNMENT = "CONSIGNMENT"; // 寄售仓
    String INV_WAREHOUSE_TYPES_NORMAL = "NORMAL"; // 标准仓
    // 接收直发-状态
    String PUR_RCV_WIP_STATUS = "PUR_RCV_WIP_STATUS"; // 接收直发-状态
    String PUR_RCV_WIP_STATUS_PREPARING = "PREPARING"; // 草稿
    String PUR_RCV_WIP_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String PUR_RCV_WIP_STATUS_COMPLETED = "COMPLETED"; // 已完成
    // ===End=== ================ [库存模块] ================
    // ==Start== ================ [合同模块] ================
    // 合同纠纷类型
    String CONT_BREACH_TYPE = "CONT_BREACH_TYPE"; // 合同纠纷类型
    String CONT_BREACH_TYPE_DELAY = "DELAY"; // 设备延迟收货
    String CONT_BREACH_TYPE_UNQUALIFIED = "UNQUALIFIED"; // 设备不符合规格
    // 合同对方选择方式
    String CONT_CHOICE_TYPE = "CONT_CHOICE_TYPE"; // 合同对方选择方式
    String CONT_CHOICE_TYPE_OPEN_TENDER = "OPEN_TENDER"; // 公开招标
    String CONT_CHOICE_TYPE_INVITE_TENDER = "INVITE_TENDER"; // 邀请招标
    String CONT_CHOICE_TYPE_OMPETITIVE_NEGOTIATION = "OMPETITIVE_NEGOTIATION"; // 直接谈判
    String CONT_CHOICE_TYPE_INQUIRY = "INQUIRY"; // 询价
    String CONT_CHOICE_TYPE_SINGLE_SOURCE = "SINGLE_SOURCE"; // 单一来源
    String CONT_CHOICE_TYPE_OTHER = "OTHER"; // 其他
    // 合同范本级别
    String CONT_MODEL_LEVEL = "CONT_MODEL_LEVEL"; // 合同范本级别
    String CONT_MODEL_LEVEL_GROUP = "GROUP"; // 集团范本
    String CONT_MODEL_LEVEL_PROVINCE = "PROVINCE"; // 省/自治区/直辖市公司范本
    String CONT_MODEL_LEVEL_CITY = "CITY"; // 地市公司范本
    // 文本种类
    String CONT_TEXT_TYPE = "CONT_TEXT_TYPE"; // 文本种类
    String CONT_TEXT_TYPE_NEW_CONTRACT = "NEW_CONTRACT"; // New Contract
    String CONT_TEXT_TYPE_RENEWAL = "RENEWAL"; // Renewal
    String CONT_TEXT_TYPE_OFFER_LETTER_NEW_CONTRACT = "OFFER_LETTER-NEW_CONTRACT"; // Offer Letter – New Contract
    String CONT_TEXT_TYPE_OFFER_LETTER_RENEWAL = "OFFER_LETTER-RENEWAL"; // Offer Letter – Renewal
    String CONT_TEXT_TYPE_INTERIM = "INTERIM"; // Interim
    String CONT_TEXT_TYPE_OTHERS = "OTHERS"; // Others
    // 合同金额类型
    String CONTRACT_AMT_TYPE = "CONTRACT_AMT_TYPE"; // 合同金额类型
    String CONTRACT_AMT_TYPE_COST = "COST"; // 费用类
    String CONTRACT_AMT_TYPE_INVESTMENT = "INVESTMENT"; // 投资类
    String CONTRACT_AMT_TYPE_OTHER = "OTHER"; // 其他类
    // 合同属性
    String CONTRACT_ATTRIBUTE = "CONTRACT_ATTRIBUTE"; // 合同属性
    String CONTRACT_ATTRIBUTE_INTENTIONAL_FRAMEWORK_CONTRACT = "INTENTIONAL_FRAMEWORK_CONTRACT"; // 意向框架合同
    String CONTRACT_ATTRIBUTE_GENERAL_FRAMEWORK_CONTRACT = "GENERAL_FRAMEWORK_CONTRACT"; // 一般框架合同
    String CONTRACT_ATTRIBUTE_COMMON_SINGLE_CONTRACT = "COMMON_SINGLE_CONTRACT"; // 普通单项合同
    String CONTRACT_ATTRIBUTE_COMMON_FRAMEWORK_CONTRACT = "COMMON_FRAMEWORK_CONTRACT"; // 普通框架合同
    String CONTRACT_ATTRIBUTE_GENERAL_CONTRACT = "GENERAL_CONTRACT"; // 一般标的合同
    // 合同授权状态
    String CONTRACT_AUTHOR_STATUS = "CONTRACT_AUTHOR_STATUS"; // 合同授权状态
    String CONTRACT_AUTHOR_STATUS_DRAFT = "DRAFT"; // 起草
    String CONTRACT_AUTHOR_STATUS_APPROVING = "APPROVING"; // 审批中
    String CONTRACT_AUTHOR_STATUS_EFFECT = "EFFECT"; // 生效
    String CONTRACT_AUTHOR_STATUS_OUT = "OUT"; // 作废
    // 合同资金性质
    String CONTRACT_CAPITAL_PROPERTY = "CONTRACT_CAPITAL_PROPERTY"; // 合同资金性质
    String CONTRACT_CAPITAL_PROPERTY_P_CAPEX = "P_CAPEX"; // Capex
    String CONTRACT_CAPITAL_PROPERTY_P_OPEX = "P_OPEX"; // Opex
    // 合同分级（大类，小类）
    String CONTRACT_CAT_LEVEL = "CONTRACT_CAT_LEVEL"; // 合同分级（大类，小类）
    String CONTRACT_CAT_LEVEL_B_LEVEL = "B_LEVEL"; // 大类
    String CONTRACT_CAT_LEVEL_S_LEVEL = "S_LEVEL"; // 小类
    // 所属类别
    String CONTRACT_CAT_TYPE = "CONTRACT_CAT_TYPE"; // 所属类别
    String CONTRACT_CAT_TYPE_CMHK = "CMHK"; // CMHK
    String CONTRACT_CAT_TYPE_CMCC = "CMCC"; // CMCC
    // 纠纷责任方类型
    String CONTRACT_DISPUTE_LIABILITY = "CONTRACT_DISPUTE_LIABILITY"; // 纠纷责任方类型
    String CONTRACT_DISPUTE_LIABILITY_OTHER = "OTHER"; // 对方
    String CONTRACT_DISPUTE_LIABILITY_OUR = "OUR"; // 我方
    String CONTRACT_DISPUTE_LIABILITY_BOTH = "BOTH"; // 双方
    // 合同纠纷处理方式
    String CONTRACT_DISPUTE_WAY = "CONTRACT_DISPUTE_WAY"; // 合同纠纷处理方式
    String CONTRACT_DISPUTE_WAY_LAWSUIT = "LAWSUIT"; // 诉讼
    // 合同级别
    String CONTRACT_LEVEL = "CONTRACT_LEVEL"; // 合同级别
    String CONTRACT_LEVEL_IMPORTANT = "IMPORTANT"; // 重大
    String CONTRACT_LEVEL_UNIMPORTANT = "UNIMPORTANT"; // 非重大
    // 合同性质
    String CONTRACT_NATURE = "CONTRACT_NATURE"; // 合同性质
    String CONTRACT_NATURE_REVENUE = "REVENUE"; // 收入类
    String CONTRACT_NATURE_PAYMENT = "PAYMENT"; // 支付类
    String CONTRACT_NATURE_REVENUEANDPAYMENT = "REVENUEANDPAYMENT"; // 混合类
    String CONTRACT_NATURE_OTHER = "OTHER"; // 其他類
    // 网络类型
    String CONTRACT_NETWORK = "CONTRACT_NETWORK"; // 网络类型
    String CONTRACT_NETWORK_3G = "3G"; // 3G
    String CONTRACT_NETWORK_4G = "4G"; // 4G
    // 合同付款周期
    String CONTRACT_PAYMENT_PERIOD = "CONTRACT_PAYMENT_PERIOD"; // 合同付款周期
    String CONTRACT_PAYMENT_PERIOD_QUARTERLY = "QUARTERLY"; // 每季度
    String CONTRACT_PAYMENT_PERIOD_MONTHLY = "MONTHLY"; // 每月
    String CONTRACT_PAYMENT_PERIOD_YEARLY = "YEARLY"; // 每年
    String CONTRACT_PAYMENT_PERIOD_HALF_YEARLY = "HALF-YEARLY"; // 每半年
    String CONTRACT_PAYMENT_PERIOD_PREMONTHLY = "PREMONTHLY"; // 每月_预付
    String CONTRACT_PAYMENT_PERIOD_ONCE = "ONCE"; // 合同付款周期
    // 合同收付款状态
    String CONTRACT_PAYMENT_STATUS = "CONTRACT_PAYMENT_STATUS"; // 合同收付款状态
    String CONTRACT_PAYMENT_STATUS_UNPAID = "UNPAID"; // 未付款
    String CONTRACT_PAYMENT_STATUS_PAID = "PAID"; // 已发送至财务
    String CONTRACT_PAYMENT_STATUS_STOP = "STOP"; // 停止付款
    String CONTRACT_PAYMENT_STATUS_PAIING = "PAIING"; // 合同收付款状态
    // 合同对方来源
    String CONTRACT_SOURCE = "CONTRACT_SOURCE"; // 合同对方来源
    String CONTRACT_SOURCE_SELF = "SELF"; // 自行选择
    String CONTRACT_SOURCE_UNIFIED = "UNIFIED"; // 统谈分签
    String CONTRACT_SOURCE_FOCUS = "FOCUS"; // 集中采购
    String CONTRACT_SOURCE_APPOINT = "APPOINT"; // 集团公司指定
    // 合同用印类型
    String CONTRACT_STAMP_TYPE = "CONTRACT_STAMP_TYPE"; // 合同用印类型
    String CONTRACT_STAMP_TYPE_BOTH = "BOTH"; // 双方用印
    String CONTRACT_STAMP_TYPE_OTHER = "OTHER"; // 对方用印
    String CONTRACT_STAMP_TYPE_OUR = "OUR"; // 我方用印
    // 合同状态
    String CONTRACT_STATUS = "CONTRACT_STATUS"; // 合同状态
    String CONTRACT_STATUS_CONCLUDE = "CONCLUDE"; // 已办结
    String CONTRACT_STATUS_DRAFT = "DRAFT"; // 起草
    String CONTRACT_STATUS_APPROVING = "APPROVING"; // 审批中
    String CONTRACT_STATUS_APPROVEND = "APPROVEND"; // 审批完毕
    String CONTRACT_STATUS_EXECUTE = "EXECUTE"; // 履行中
    String CONTRACT_STATUS_OUT = "OUT"; // 作废
    String CONTRACT_STATUS_SUSPEND = "SUSPEND"; // 暂停
    String CONTRACT_STATUS_RESCISSION = "RESCISSION"; // 解除
    String CONTRACT_STATUS_CONFIRM = "CONFIRM"; // 合同確認狀態
    // 合同范本属性
    String CONTRACT_TEMPLATE_TYPEXQ = "CONTRACT_TEMPLATE_TYPEXQ"; // 合同范本属性
    String CONTRACT_TEMPLATE_TYPEXQ_ORDINARY = "ORDINARY"; // 普通合同
    String CONTRACT_TEMPLATE_TYPEXQ_REFERENCE_RESOURCES = "REFERENCE RESOURCES"; // 参考范本的合同
    String CONTRACT_TEMPLATE_TYPEXQ_APPLY = "APPLY"; // 套用范本的合同
    // ===End=== ================ [合同模块] ================
    // ==Start== ================ [报账模块] ================
    // 分摊类型
    String EA_ALLOCATION_TYPE = "EA_ALLOCATION_TYPE"; // 分摊类型
    String EA_ALLOCATION_TYPE_COST_CENTER = "COST_CENTER"; // 成本中心分摊
    String EA_ALLOCATION_TYPE_LOCATION = "LOCATION"; // 地点分摊
    // 保密级别
    String EA_CONFIDENTIAL_LEVEL = "EA_CONFIDENTIAL_LEVEL"; // 保密级别
    String EA_CONFIDENTIAL_LEVEL_PRIVATE = "PRIVATE"; // 内部
    String EA_CONFIDENTIAL_LEVEL_PUBLIC = "PUBLIC"; // 公用
    // 日常费用类型
    String EA_DAILY_EXP_CATAGORY = "EA_DAILY_EXP_CATAGORY"; // 日常费用类型
    String EA_DAILY_EXP_CATAGORY_EXP_CAT001 = "EXP_CAT001"; // CHINA MOBILE LTD
    String EA_DAILY_EXP_CATAGORY_EXP_CAT002 = "EXP_CAT002"; // System Support
    String EA_DAILY_EXP_CATAGORY_EXP_CAT003 = "EXP_CAT003"; // System Training
    String EA_DAILY_EXP_CATAGORY_EXP_CAT004 = "EXP_CAT004"; // Network Maintenance
    String EA_DAILY_EXP_CATAGORY_EXP_CAT005 = "EXP_CAT005"; // Roadshow Cost
    String EA_DAILY_EXP_CATAGORY_EXP_CAT006 = "EXP_CAT006"; // New Subscriber Material
    String EA_DAILY_EXP_CATAGORY_EXP_CAT007 = "EXP_CAT007"; // Promotional Material
    String EA_DAILY_EXP_CATAGORY_EXP_CAT008 = "EXP_CAT008"; // Other Promotional Event-Mkting
    String EA_DAILY_EXP_CATAGORY_EXP_CAT009 = "EXP_CAT009"; // Market & Customer Research
    String EA_DAILY_EXP_CATAGORY_EXP_CAT010 = "EXP_CAT010"; // Staff Messing
    // 招待活动性质
    String EA_ENT_EXP_ACTIVITY_NATURE = "EA_ENT_EXP_ACTIVITY_NATURE"; // 招待活动性质
    String EA_ENT_EXP_ACTIVITY_NATURE_BUSINESS_FOREIGN = "BUSINESS_FOREIGN"; // 商务和外事招待
    String EA_ENT_EXP_ACTIVITY_NATURE_OTHER_PUBLIC = "OTHER_PUBLIC"; // 其他公务招待
    // 招待费用类型
    String EA_ENT_EXP_CATAGORY = "EA_ENT_EXP_CATAGORY"; // 招待费用类型
    String EA_ENT_EXP_CATAGORY_ENTERTAINMENT = "ENTERTAINMENT"; // 娱乐费
    String EA_ENT_EXP_CATAGORY_ACCOMMODATION = "ACCOMMODATION"; // 食宿费
    String EA_ENT_EXP_CATAGORY_OTHER = "OTHER"; // 其他
    // 业务招待类型
    String EA_ENT_EXP_NATURE_EVENT = "EA_ENT_EXP_NATURE_EVENT"; // 业务招待类型
    String EA_ENT_EXP_NATURE_EVENT_MEALS = "MEALS"; // 招待餐费
    String EA_ENT_EXP_NATURE_EVENT_FESTIVE = "FESTIVE"; // 赠送节日性礼物
    String EA_ENT_EXP_NATURE_EVENT_NFESTIVE = "NFESTIVE"; // 赠送非节日性礼物
    // 出差费率标准旺季月份
    String EA_EXP_RATE_MONTHS = "EA_EXP_RATE_MONTHS"; // 出差费率标准旺季月份
    String EA_EXP_RATE_MONTHS_JANUARY = "JANUARY"; // 1月
    String EA_EXP_RATE_MONTHS_FEBRUARY = "FEBRUARY"; // 2月
    String EA_EXP_RATE_MONTHS_MARCH = "MARCH"; // 3月
    String EA_EXP_RATE_MONTHS_APRIL = "APRIL"; // 4月
    String EA_EXP_RATE_MONTHS_MAY = "MAY"; // 5月
    String EA_EXP_RATE_MONTHS_JUNE = "JUNE"; // 6月
    String EA_EXP_RATE_MONTHS_JULY = "JULY"; // 7月
    String EA_EXP_RATE_MONTHS_AUGUST = "AUGUST"; // 8月
    String EA_EXP_RATE_MONTHS_SEPTEMBER = "SEPTEMBER"; // 9月
    String EA_EXP_RATE_MONTHS_OCTOBER = "OCTOBER"; // 10月
    String EA_EXP_RATE_MONTHS_NOVEMBER = "NOVEMBER"; // 11月
    String EA_EXP_RATE_MONTHS_DECEMBER = "DECEMBER"; // 12月
    // 报账单导入EBS状态
    String EA_IMPORT_FLAG = "EA_IMPORT_FLAG"; // 报账单导入EBS状态
    String EA_IMPORT_FLAG_Y = "Y"; // 导入成功
    String EA_IMPORT_FLAG_N = "N"; // 导入失败
    String EA_IMPORT_FLAG_S = "S"; // 未导入
    // 付款限额类型
    String EA_LIMITED_FLAG = "EA_LIMITED_FLAG"; // 付款限额类型
    String EA_LIMITED_FLAG_PERSONERAL = "PERSONERAL"; // 员工支付金额
    String EA_LIMITED_FLAG_ALLOWED = "ALLOWED"; // 允许付款金额
    // 收款活动
    String EA_RECEIVE_ACTIVITY = "EA_RECEIVE_ACTIVITY"; // 收款活动
    String EA_RECEIVE_ACTIVITY_INTEREST = "INTEREST"; // 利息收款
    String EA_RECEIVE_ACTIVITY_PREPAYMENT = "PREPAYMENT"; // 预收款
    // 收款方式
    String EA_RECEIVE_MODE = "EA_RECEIVE_MODE"; // 收款方式
    String EA_RECEIVE_MODE_BANK = "BANK"; // 银行收款
    String EA_RECEIVE_MODE_CASH = "CASH"; // 现金收款
    String EA_RECEIVE_MODE_OTHER = "OTHER"; // 收款方式
    // 收款类型
    String EA_RECEIVE_TYPE = "EA_RECEIVE_TYPE"; // 收款类型
    String EA_RECEIVE_TYPE_STANDARD = "STANDARD"; // 标准
    String EA_RECEIVE_TYPE_MISCELLANEOUS = "MISCELLANEOUS"; // 杂项
    String EA_RECEIVE_TYPE_ADCANCE = "ADCANCE"; // 预收
    // 事务处理类型
    String EA_TRANSACTION_TYPE = "EA_TRANSACTION_TYPE"; // 事务处理类型
    String EA_TRANSACTION_TYPE_AR = "AR"; // 应收发票
    String EA_TRANSACTION_TYPE_CREDIT = "CREDIT"; // 贷项通知单
    // 时间类型
    String EA_TRAVEL_DATE_TYPE = "EA_TRAVEL_DATE_TYPE"; // 时间类型
    String EA_TRAVEL_DATE_TYPE_AM = "AM"; // 上午
    String EA_TRAVEL_DATE_TYPE_PM = "PM"; // 下午
    // 出差费类型编号
    String EA_TRAVEL_EXP_CATAGORY = "EA_TRAVEL_EXP_CATAGORY"; // 出差费类型编号
    String EA_TRAVEL_EXP_CATAGORY_HOTEL = "HOTEL"; // 住宿费
    String EA_TRAVEL_EXP_CATAGORY_BOARD = "BOARD"; // 伙食费
    String EA_TRAVEL_EXP_CATAGORY_PUBLIC = "PUBLIC"; // 公杂费
    String EA_TRAVEL_EXP_CATAGORY_TRANSPORT = "TRANSPORT"; // 机票
    // 紧急程度
    String EA_URGENCY_DEGREE = "EA_URGENCY_DEGREE"; // 紧急程度
    String EA_URGENCY_DEGREE_GENERAL = "GENERAL"; // 一般
    String EA_URGENCY_DEGREE_URGENT = "URGENT"; // 紧急
    String EA_URGENCY_DEGREE_EXTRA = "EXTRA"; // 特急
    // 核销状态
    String EA_VERIFICATION_FLAG = "EA_VERIFICATION_FLAG"; // 核销状态
    String EA_VERIFICATION_FLAG_Y = "Y"; // 已核销
    String EA_VERIFICATION_FLAG_N = "N"; // 未核销
    // ===End=== ================ [报账模块] ================
    // ==Start== ================ [系统模块] ================
    // 部门流程角色
    String SYS_DEPT_WF_ROLE = "SYS_DEPT_WF_ROLE"; // 部门流程角色
    String SYS_DEPT_WF_ROLE_CHAIRMAN = "CHAIRMAN"; // Chairman
    String SYS_DEPT_WF_ROLE_DIRECTOR = "DIRECTOR"; // Director
    String SYS_DEPT_WF_ROLE_DIVISION_HEAD = "DIVISION_HEAD"; // Division Head
    String SYS_DEPT_WF_ROLE_DEPARTMENT_MANAGER = "DEPARTMENT_MANAGER"; // Department Manager
    String SYS_DEPT_WF_ROLE_SECTION_MANAGER = "SECTION_MANAGER"; // Section Manager
    String SYS_DEPT_WF_ROLE_GENERAL_STAFF = "GENERAL_STAFF"; // General Staff
    // 运维工单状态
    String SYS_FEEDBACK_STATUS = "SYS_FEEDBACK_STATUS"; // 运维工单状态
    String SYS_FEEDBACK_STATUS_INPROCESS = "INPROCESS"; // 处理中
    String SYS_FEEDBACK_STATUS_COMPLETED = "COMPLETED"; // 已完成
    // 运维工单类型
    String SYS_FEEDBACK_TYPE = "SYS_FEEDBACK_TYPE"; // 运维工单类型
    String SYS_FEEDBACK_TYPE_SYSTEM_MODULE = "SYSTEM_MODULE"; // 系统模块
    String SYS_FEEDBACK_TYPE_BUSINESS_MODULE = "BUSINESS_MODULE"; // 业务模块
    // 具体模块
    String SYS_FUNCTION_TYPE = "SYS_FUNCTION_TYPE"; // 具体模块
    String SYS_FUNCTION_TYPE_BUD_DAILY_BUDGET = "BUD_DAILY_BUDGET"; // 日常预算
    String SYS_FUNCTION_TYPE_BUD_GROUP_BUDGET = "BUD_GROUP_BUDGET"; // 集团预算
    String SYS_FUNCTION_TYPE_BUD_BUDGET_REPORT = "BUD_BUDGET_REPORT"; // 预算报表
    String SYS_FUNCTION_TYPE_PROJ_INVESTENT_PLAN = "PROJ_INVESTENT_PLAN"; // 投资计划
    String SYS_FUNCTION_TYPE_PROJ_PROGRESS = "PROJ_PROGRESS"; // 项目过程
    String SYS_FUNCTION_TYPE_PROJ_WORKSPACE = "PROJ_WORKSPACE"; // 项目工作台
    String SYS_FUNCTION_TYPE_PUR_PLAN = "PUR_PLAN"; // 采购计划
    String SYS_FUNCTION_TYPE_PUR_APPLICATION = "PUR_APPLICATION"; // 采购申请
    String SYS_FUNCTION_TYPE_PUR_PROJECT = "PUR_PROJECT"; // 采购项目
    String SYS_FUNCTION_TYPE_PUR_ARGREEMENT = "PUR_ARGREEMENT"; // 框架协议
    String SYS_FUNCTION_TYPE_PUR_PROCUREMENT = "PUR_PROCUREMENT"; // 采购订单
    String SYS_FUNCTION_TYPE_INV_STOCKIN = "INV_STOCKIN"; // 接收入库
    String SYS_FUNCTION_TYPE_INV_TRANSFER_ALLOCATION = "INV_TRANSFER_ALLOCATION"; // 转移调拨
    String SYS_FUNCTION_TYPE_INV_STOCKOUT = "INV_STOCKOUT"; // 领料出库
    String SYS_FUNCTION_TYPE_INV_INVENTORY_RETURN = "INV_INVENTORY_RETURN"; // 退货入库
    String SYS_FUNCTION_TYPE_INV_RETURN_SUPPILER = "INV_RETURN_SUPPILER"; // 退供应商
    String SYS_FUNCTION_TYPE_INV_INVENTORY_ENQUIRY = "INV_INVENTORY_ENQUIRY"; // 库存查询
    String SYS_FUNCTION_TYPE_CONT_CONTRACT = "CONT_CONTRACT"; // 合同管理
    // 消息类型
    String SYS_INFO_TYPE = "SYS_INFO_TYPE"; // 消息类型
    String SYS_INFO_TYPE_0 = "0"; // 公告
    String SYS_INFO_TYPE_1 = "1"; // 统一维护通知
    // 系统接口类型
    String SYS_INTFC_TYPE = "SYS_INTFC_TYPE"; // 系统接口类型
    String SYS_INTFC_TYPE_INTERNAL = "INTERNAL"; // 内部系统接口
    String SYS_INTFC_TYPE_EXTERNAL = "EXTERNAL"; // 外部系统接口
    // EBS接口默认值
    String SYS_ITF_EBS_DEF = "SYS_ITF_EBS_DEF"; // EBS接口默认值
    String SYS_ITF_EBS_DEF_OU_ID = "OU_ID"; // OUID
    String SYS_ITF_EBS_DEF_BUYER_ID = "BUYER_ID"; // 采购员ID
    // 定时任务状态
    String SYS_JOB_STATUS = "SYS_JOB_STATUS"; // 定时任务状态
    String SYS_JOB_STATUS_0 = "0"; // 启用
    String SYS_JOB_STATUS_1 = "1"; // 暂停
    String SYS_JOB_STATUS_2 = "2"; // 禁用
    // 系统操作日志类型
    String SYS_LOG_TYPE = "SYS_LOG_TYPE"; // 系统操作日志类型
    String SYS_LOG_TYPE_NEW = "NEW"; // 新增
    String SYS_LOG_TYPE_DELETE = "DELETE"; // 删除
    String SYS_LOG_TYPE_UPDATE = "UPDATE"; // 修改
    String SYS_LOG_TYPE_QUERY = "QUERY"; // 查询
    String SYS_LOG_TYPE_LOGIN = "LOGIN"; // 登陆
    String SYS_LOG_TYPE_LOGOUT = "LOGOUT"; // 登出
    // 菜单类型
    String SYS_MENU_TYPE = "SYS_MENU_TYPE"; // 菜单类型
    String SYS_MENU_TYPE_OUTSIDE = "OUTSIDE"; // 外部URL
    String SYS_MENU_TYPE_INSIDE = "INSIDE"; // 系统URL
    // 系统订单类型
    String SYS_ORDER_TYPE = "SYS_ORDER_TYPE"; // 系统订单类型
    String SYS_ORDER_TYPE_PN = "PN"; // 采购计划通知单
    String SYS_ORDER_TYPE_PF = "PF"; // 采购计划填报单
    String SYS_ORDER_TYPE_PP = "PP"; // 采购项目
    String SYS_ORDER_TYPE_ENQ = "ENQ"; // 询价单
    String SYS_ORDER_TYPE_QUO = "QUO"; // 报价单
    String SYS_ORDER_TYPE_PO = "PO"; // 订单
    String SYS_ORDER_TYPE_CO = "CO"; // 暂收单
    String SYS_ORDER_TYPE_RN = "RN"; // 入库通知单
    String SYS_ORDER_TYPE_RM = "RM"; // 入库单
    String SYS_ORDER_TYPE_IO = "IO"; // 领料单
    String SYS_ORDER_TYPE_IM = "IM"; // 出库单
    String SYS_ORDER_TYPE_RIR = "RIR"; // 退库申请单
    String SYS_ORDER_TYPE_RIM = "RIM"; // 退库单
    String SYS_ORDER_TYPE_CPR = "CPR"; // 暂收直发
    String SYS_ORDER_TYPE_TM = "TM"; // 配送单
    String SYS_ORDER_TYPE_CTR = "CTR"; // 批量调拨单
    String SYS_ORDER_TYPE_ERM = "ERM"; // 费用接收单
    String SYS_ORDER_TYPE_ETM = "ETM"; // 费用回退单
    String SYS_ORDER_TYPE_MRM = "MRM"; // 杂项入库单
    String SYS_ORDER_TYPE_TR = "TR"; // 转移调拨单
    String SYS_ORDER_TYPE_MIM = "MIM"; // 报废单
    String SYS_ORDER_TYPE_PRM = "PRM"; // 直发单
    String SYS_ORDER_TYPE_PTR = "PTR"; // 项目转移单
    String SYS_ORDER_TYPE_RVM = "RVM"; // 暂收退货单
    String SYS_ORDER_TYPE_COM = "COM"; // 核销单
    String SYS_ORDER_TYPE_CCP = "CCP"; // 盘点计划
    String SYS_ORDER_TYPE_PRT = "PRT"; // 采购需求模板
    String SYS_ORDER_TYPE_PRN = "PRN"; // 采购需求通知
    String SYS_ORDER_TYPE_PR = "PR"; // 采购需求
    String SYS_ORDER_TYPE_RP = "RP"; // 自动退单
    String SYS_ORDER_TYPE_PL = "PL"; // 装箱单
    String SYS_ORDER_TYPE_ADD = "ADD"; // 采购计划追加单
    String SYS_ORDER_TYPE_CONT_DRAFT = "CONT_DRAFT"; // 合同起草
    String SYS_ORDER_TYPE_CONT_CHANGE = "CONT_CHANGE"; // 合同变更
    String SYS_ORDER_TYPE_CONT_DISPUTE = "CONT_DISPUTE"; // 合同纠纷
    String SYS_ORDER_TYPE_CONT_RESOLVE = "CONT_RESOLVE"; // 合同纠纷办结
    String SYS_ORDER_TYPE_CONT_SUSPEND = "CONT_SUSPEND"; // 合同暂停
    String SYS_ORDER_TYPE_CONT_CONTINUE = "CONT_CONTINUE"; // 合同继续
    String SYS_ORDER_TYPE_CONT_TRANSFER = "CONT_TRANSFER"; // 合同移交
    String SYS_ORDER_TYPE_CONT_ARCHIVED = "CONT_ARCHIVED"; // 合同归档
    String SYS_ORDER_TYPE_CONT_CONCLUDE = "CONT_CONCLUDE"; // 合同办结
    String SYS_ORDER_TYPE_CONT_RESCISSION = "CONT_RESCISSION"; // 合同解除
    String SYS_ORDER_TYPE_CONT_PAID = "CONT_PAID"; // 合同收付款计划
    String SYS_ORDER_TYPE_PROJ_009 = "PROJ_009"; // 立项申请
    String SYS_ORDER_TYPE_PROJ_019 = "PROJ_019"; // 项目验收
    String SYS_ORDER_TYPE_PROJ_018 = "PROJ_018"; // 项目基本信息变更
    String SYS_ORDER_TYPE_PROJ_017 = "PROJ_017"; // 项目财务WBS
    String SYS_ORDER_TYPE_PROJ_016 = "PROJ_016"; // 项目计划
    String SYS_ORDER_TYPE_BS_PUR002 = "BS_PUR002"; // 供应商注册单
    String SYS_ORDER_TYPE_PROJ_020 = "PROJ_020"; // 财务WBS信息维护
    String SYS_ORDER_TYPE_PROJ_021 = "PROJ_021"; // 施工WBS信息维护
    String SYS_ORDER_TYPE_PROJ_022 = "PROJ_022"; // 项目计划变更
    String SYS_ORDER_TYPE_PROJ_023 = "PROJ_023"; // 项目关闭
    String SYS_ORDER_TYPE_PROJ_024 = "PROJ_024"; // 财务WBS变更
    String SYS_ORDER_TYPE_EAS_012 = "EAS_012"; // 有合同报账
    String SYS_ORDER_TYPE_EAS_013 = "EAS_013"; // 无合同报账
    String SYS_ORDER_TYPE_EAS_010 = "EAS_010"; // 招待费用报销
    String SYS_ORDER_TYPE_EAS_014 = "EAS_014"; // 批量报账
    String SYS_ORDER_TYPE_EAS_015 = "EAS_015"; // 部门收入报账
    String SYS_ORDER_TYPE_EAS_009 = "EAS_009"; // 差旅报销单
    String SYS_ORDER_TYPE_EAS_008 = "EAS_008"; // 日常及其他费用报账
    String SYS_ORDER_TYPE_EAS_005 = "EAS_005"; // 出差申请
    String SYS_ORDER_TYPE_EAS_019 = "EAS_019"; // 无合同分摊报账
    String SYS_ORDER_TYPE_EAS_020 = "EAS_020"; // 非基站资产转资
    String SYS_ORDER_TYPE_CONT_AUTHOR = "CONT_AUTHOR"; // 合同授权
    String SYS_ORDER_TYPE_PROJ_012 = "PROJ_012"; // 项目基本信息
    String SYS_ORDER_TYPE_BS_PUR001 = "BS_PUR001"; // 物料编码申请
    String SYS_ORDER_TYPE_AE = "AE"; // 系统订单类型
    String SYS_ORDER_TYPE_AG = "AG"; // 系统订单类型
    String SYS_ORDER_TYPE_AS = "AS"; // 供应商表现评估单
    String SYS_ORDER_TYPE_QS = "QS"; // 供应商表现评估通知单
    String SYS_ORDER_TYPE_CONT_RENEW = "CONT_RENEW"; // 合同续签流程
    String SYS_ORDER_TYPE_PURAS = "PURAS"; // 供应商表现评估单(采购)
    // 组织类型
    String SYS_ORG_TYPE = "SYS_ORG_TYPE"; // 组织类型
    String SYS_ORG_TYPE_DEPARTMENT = "DEPARTMENT"; // 部门
    String SYS_ORG_TYPE_COMPANY = "COMPANY"; // 公司
    // 员工职级
    String SYS_POSITION_LEVEL = "SYS_POSITION_LEVEL"; // 员工职级
    String SYS_POSITION_LEVEL_01 = "01"; // 董事长
    String SYS_POSITION_LEVEL_02 = "02"; // CEO
    String SYS_POSITION_LEVEL_03 = "03"; // 董事兼副总裁
    String SYS_POSITION_LEVEL_04 = "04"; // 部门副总经理
    String SYS_POSITION_LEVEL_05 = "05"; // 秘书
    String SYS_POSITION_LEVEL_06 = "06"; // 部门总经理
    String SYS_POSITION_LEVEL_07 = "07"; // 部门经理
    String SYS_POSITION_LEVEL_08 = "08"; // 部门副经理
    String SYS_POSITION_LEVEL_09 = "09"; // 普通员工
    // 角色类型
    String SYS_ROLE_TYPE = "SYS_ROLE_TYPE"; // 角色类型
    String SYS_ROLE_TYPE_SYSTEM = "SYSTEM"; // 系统角色
    String SYS_ROLE_TYPE_WORKFLOW = "WORKFLOW"; // 业务流程角色
    // 流水号重置规则
    String SYS_SERIAL_NUMBER = "SYS_SERIAL_NUMBER"; // 流水号重置规则
    String SYS_SERIAL_NUMBER_NEVER = "NEVER"; // 不重置
    String SYS_SERIAL_NUMBER_DAY = "DAY"; // 按日重置
    String SYS_SERIAL_NUMBER_MONTH = "MONTH"; // 按月重置
    String SYS_SERIAL_NUMBER_YEAR = "YEAR"; // 按年重置
    String SYS_SERIAL_NUMBER_HOUR = "HOUR"; // 按小时重置
    String SYS_SERIAL_NUMBER_MINUTE = "MINUTE"; // 按分重置,
    String SYS_SERIAL_NUMBER_SECOND = "SECOND"; // 按秒重置
    // 系统信息模板类型
    String SYS_TEMPLATE_TYPE = "SYS_TEMPLATE_TYPE"; // 系统信息模板类型
    String SYS_TEMPLATE_TYPE_EMAIL = "EMAIL"; // 邮件模板
    String SYS_TEMPLATE_TYPE_MESSAGE = "MESSAGE"; // 消息模板
    // 用户状态
    String SYS_USER_STATUS = "SYS_USER_STATUS"; // 用户状态
    String SYS_USER_STATUS_ENABLE = "ENABLE"; // 啟用
    String SYS_USER_STATUS_INVALID = "INVALID"; // 失效
    String SYS_USER_STATUS_LOCKING = "LOCKING"; // 鎖定
    // 是否签收
    String SYS_WF_CLAIM = "SYS_WF_CLAIM"; // 是否签收
    String SYS_WF_CLAIM_Y = "Y"; // 已签收
    String SYS_WF_CLAIM_N = "N"; // 未签收
    // 流程会签结果
    String SYS_WF_COUNTERSIGN_RESULT = "SYS_WF_COUNTERSIGN_RESULT"; // 流程会签结果
    String SYS_WF_COUNTERSIGN_RESULT_N = "N"; // 不同意
    String SYS_WF_COUNTERSIGN_RESULT_Y = "Y"; // 同意
    // 流程会签状态
    String SYS_WF_COUNTERSIGN_STATUS = "SYS_WF_COUNTERSIGN_STATUS"; // 流程会签状态
    String SYS_WF_COUNTERSIGN_STATUS_N = "N"; // 未审批
    String SYS_WF_COUNTERSIGN_STATUS_Y = "Y"; // 已审批
    // 流程历史信息审批状态
    String SYS_WF_HISTORY_TYPE = "SYS_WF_HISTORY_TYPE"; // 流程历史信息审批状态
    String SYS_WF_HISTORY_TYPE_COMMIT = "commit"; // 审批通过
    String SYS_WF_HISTORY_TYPE_RETURN = "return"; // 退回上一步
    String SYS_WF_HISTORY_TYPE_WITHDRAW = "withDraw"; // 取回
    String SYS_WF_HISTORY_TYPE_BREAK = "break"; // 退回起草人
    String SYS_WF_HISTORY_TYPE_REPEAL = "repeal"; // 撤销
    String SYS_WF_HISTORY_TYPE_END = "end"; // 流程结束
    String SYS_WF_HISTORY_TYPE_RUNWF = "runWf"; // 审批中
    String SYS_WF_HISTORY_TYPE_Y = "Y"; // 同意
    String SYS_WF_HISTORY_TYPE_N = "N"; // 不同意
    String SYS_WF_HISTORY_TYPE_YY = "YY"; // 签收中
    // 流程操作类型
    String SYS_WF_OPER_TYPE = "SYS_WF_OPER_TYPE"; // 流程操作类型
    String SYS_WF_OPER_TYPE_1 = "1"; // 提交审批
    String SYS_WF_OPER_TYPE_3 = "3"; // 退回上一步
    String SYS_WF_OPER_TYPE_2 = "2"; // 退回发起人
    String SYS_WF_OPER_TYPE_5 = "5"; // 撤销
    String SYS_WF_OPER_TYPE_4 = "4"; // 取回
    // ===End=== ================ [系统模块] ================
}

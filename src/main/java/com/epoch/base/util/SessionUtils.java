package com.epoch.base.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
/**
 *  Session存取工具类
 * 所有自定义数据在session中的存取应通过此类来完成
 * 此类使用session中固定名称的一个Map实现来保存自定义数据，避免自由操作session数据产生的冲突覆盖等问题
* <p>Title: SessionUtils.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年10月8日
 */
public class SessionUtils {
    public static final String CUST_SESSION_DATA_KEY = "_pccw_customSessionData";

    /**
     * 向session中保存自定义数据
     * @param session 目标session
     * @param key 自定义数据key
     * @param data 自定义数据对象
     * @return 先前与指定<code>key</code>映射的数据对象，若先前没有指定<code>key</code>的映射则返回<code>null</code>(返回<code>null</code>也可能是先前的<code>key</code>映射为<code>null</code>值)
     */
    public static Object putSessionData(HttpSession session, String key, Object data) {
        Map<String, Object> map = obtainDataMapWithCreate(session);
        return map.put(key, data);
    }

    /**
     * 由session中获取自定义数据
     * @param session 目标session
     * @param key 自定义数据key
     * @return 与指定<code>key</code>映射的数据对象，若没有指定<code>key</code>的映射则返回<code>null</code>(返回<code>null</code>也可能是先前的<code>key</code>映射为<code>null</code>值)
     */
    public static Object getSessionData(HttpSession session, String key) {
        Map<String, Object> map = obtainDataMapWithCreate(session);
        return map.get(key);
    }

    /**
     * 由session中移除自定义数据
     * @param session 目标session
     * @param key 自定义数据key
     * @return 先前与指定<code>key</code>映射的数据对象，若先前没有指定<code>key</code>的映射则返回<code>null</code>(返回<code>null</code>也可能是先前的<code>key</code>映射为<code>null</code>值)
     */
    public static Object removeSessionData(HttpSession session, String key) {
        Map<String, Object> map = obtainDataMapWithCreate(session);
        return map.remove(key);
    }

    /**
     * 判断session中是否保存有自定义数据
     * @param session 目标session
     * @param key 自定义数据key
     * @return 若存在指定<code>key</code>的映射则返回<code>true</code>，否则返回<code>false</code>
     */
    public static boolean hasSessionData(HttpSession session, String key) {
        Map<String, Object> map = obtainDataMapWithCreate(session);
        return map.containsKey(key);
    }

    private static Map<String, Object> obtainDataMapWithCreate(HttpSession session) {
        @SuppressWarnings("unchecked")
        Map<String, Object> map = (Map<String, Object>)session.getAttribute(CUST_SESSION_DATA_KEY);
        if (map == null) {
            map = new HashMap<String, Object>();
            session.setAttribute(CUST_SESSION_DATA_KEY, map);
        }
        return map;
    }

    private SessionUtils() {
        //禁止工具类实例化
        super();
    }
}

package com.epoch.base.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerUtil {
	private static String NAME = LoggerUtil.class.getName();

	private LoggerUtil() {
	}

	public static Logger getLogger() {
		return getLogger(getClassName());
	}

	public static Logger getLogger(String className) {
		return LoggerFactory.getLogger(className);
	}

	private static String getClassName() {
		StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
		for (int i = 1; i < stacks.length; i++) {
			if (!NAME.equals(stacks[i].getClassName())) {
				return stacks[i].getClassName();
			}

		}
		return NAME;
	}

}
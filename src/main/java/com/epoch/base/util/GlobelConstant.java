package com.epoch.base.util;

/**
 * @author wufei-pccw(80844970)
 */
public interface GlobelConstant {
    /**
     * 账户是否锁定 ? 0 未锁定
     */
    int USER_LOCK_N = 0;
    /**
     * 账户是否锁定 ? 1 锁定
     */
    int USER_LOCK_Y = 1;

    String SESSION_KEY = "session_key";

    /**
     * 手动设置序列号
     */
    String SEQ_SETING_NAME = "seq_seting_name";

    /**
     * 禁用序列，手动创建主键ID
     */
    String DISABLE_AUTO_SEQ = "_disable_auto_seq_";

    String UTF8 = "UTF-8";

    // 常规日期正则表达式(yyyy-MM-dd)
    String REGEX_NORMAL_DATE = "^(?:(?!0000)[0-9]{4}-(?:(?:0[13578]|1[02])-(?:0[1-9]|[12][0-9]|3[01])|(?:0[469]|11)-(?:0[1-9]|[12][0-9]|30)|02-(?:0[1-9]|1[0-9]|2[0-8]))|(?:[0-9]{2}(?:0[48]|[13579][26]|[2468][048])|(?:0[48]|[13579][26]|[2468][048])00)-02-29)$";
    // 常规日期时间正则表达式(yyyy-MM-dd HH:mm:ss)
    String REGEX_NORMAL_DATETIME = "^(?:(?!0000)[0-9]{4}-(?:(?:0[13578]|1[02])-(?:0[1-9]|[12][0-9]|3[01])|(?:0[469]|11)-(?:0[1-9]|[12][0-9]|30)|02-(?:0[1-9]|1[0-9]|2[0-8]))|(?:[0-9]{2}(?:0[48]|[13579][26]|[2468][048])|(?:0[48]|[13579][26]|[2468][048])00)-02-29) (?:[0-1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$";
}

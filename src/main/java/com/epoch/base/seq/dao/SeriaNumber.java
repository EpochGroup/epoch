package com.epoch.base.seq.dao;

import java.io.Serializable;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "SYS_SERIAL_NUMBER")
public class SeriaNumber extends BaseModel<SeriaNumber> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final SeriaNumber dao = new SeriaNumber();
	
    //模型字段名称
    public static final String RULEID = "ID"; // 规则ID
    public static final String RULECODE = "RULE_CODE"; // 规则编码
    public static final String RULENAME = "RULE_NAME"; // 规则名称
    public static final String RULETYPE = "RULE_TYPE"; // 流水号重置规则
    public static final String CURTIME = "CUR_TIME"; // 数据库中当前时间
    public static final String SERIALLENGTH = "SERIAL_LENGTH"; // 流水号长度
    public static final String CURRENTVALUE = "CURRENT_VALUE"; // 当前值
    public static final String INITVALUE = "INIT_VALUE"; // 初始值
    public static final String STEP = "STEP"; // 步长，增长幅度
    public static final String SYSTEMTIME = "SYSTEM_TIME"; // 系统当前时间
    public static final String PREFIX = "PREFIX"; // 前缀
    public static final String SPLIT = "SPLIT"; // 分割符
}

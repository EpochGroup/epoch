package com.epoch.base.seq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epoch.base.exception.BusinessException;
import com.epoch.base.util.DateUtils;

public class SeriaNumberUtils {
    /**
     * 日志记录
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SeriaNumberUtils.class);

    /**
     * 获取流水号
     *
     * @param ruleCode  规则编码
     * @param prefix    前缀
     * @param dateRegex 日期格式化 默认 yyyyMMddHHmmss
     * @param split     分割符 如果为空，默认空
     * @return 流水号
     */
    public static String getSeriaNumber(String ruleCode, String prefix, String dateRegex, String split) {
        String seriaNumber = SerialNumberGeneration.anewUpdateAndGetSerialNum(dateRegex, ruleCode, split, prefix);
        LOGGER.debug("-##getSeriaNumber 流水号：" + seriaNumber);
        return seriaNumber;
    }

    /**
     * 获取流水号
     *
     * @param ruleCode  规则编码
     * @param prefix    前缀
     * @param dateRegex 日期格式化 默认 yyyyMMddHHmmss
     * @return 流水号
     */
    public static String getSeriaNumber(String ruleCode, String prefix, String dateRegex) {
        return getSeriaNumber(ruleCode, prefix, dateRegex, null);
    }

    /**
     * 获取流水号
     *
     * @param ruleCode 规则编码
     * @param prefix   前缀
     * @return 流水号
     */
    public static String getSeriaNumber(String ruleCode, String prefix) {
        return getSeriaNumber(ruleCode, prefix, null, null);
    }

    /**
     * 获取流水号
     *
     * @param ruleCode 规则编码
     * @return 流水号
     */
    public static String getSeriaNumber(String ruleCode) {
        return getSeriaNumber(ruleCode, null, null, null);
    }

    /**
     * 获取流水号
     *
     * @param ruleCode  规则编码
     * @param dateRegex 格式
     * @return 流水号
     */
    public static String getSeriaNumberFormart(String ruleCode, String dateRegex) {
        return getSeriaNumber(ruleCode, null, dateRegex, null);
    }

    /**
     * 获取流水号
     *
     * @param ruleCode 规则编码
     * @param prefix   前缀
     * @param split    分割符 如果为空，默认空
     * @return 流水号
     */
    public static String getSeriaNumberSplit(String ruleCode, String prefix, String split) {
        return getSeriaNumber(ruleCode, prefix, null, split);
    }

    /***
     * 获取新的文档编号
     * @param docType 单据类型  定义在 DocumentType.java中
     * @param erpType 类型
     * @return
     */
    public synchronized static String getNextDocumentNumber(String docType,String erpType)throws BusinessException{
    	String prifix=docType;//generatePrifx(erpType,docType); //CMHK的去掉公司类型前缀
    	try{
    		return getSeriaNumber(docType,prifix,DateUtils.DATEFORMAT_YYYYMMDD);
    	}catch(Exception ex){
    		ex.printStackTrace();
    		throw new BusinessException("generate documentNumber error");
    	}    	
    }
    
    /**
     * 根据公司类型和单据类型生成前缀
     * @param erpType
     * @param docType
     * @return
     */
    private static String generatePrifx(String erpType,String docType)throws BusinessException{
    	try{
        	if(!"".equals(erpType)&&erpType!=null){
        		int i=Integer.parseInt(erpType);
        		switch(i){
        		case 1:
        			return "YX-"+docType;        			
        		case 2:
        			return "TD-"+docType;
        		case 3:
        			return "CT-"+docType;
        		case 4:
        			return "TT-"+docType;    
        		default:
        			throw new BusinessException("unknow ERPTYPE");
        		}
        	}else{
        		throw new BusinessException("ERPTPYE requested");
        	}
    	}catch(Exception ex){
    		throw new BusinessException("generatePrifix Error");
    	}
    }
}

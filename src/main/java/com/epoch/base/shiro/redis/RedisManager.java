package com.epoch.base.shiro.redis;

import com.jfinal.plugin.redis.Redis;
import redis.clients.jedis.Jedis;

import java.util.Set;

public class RedisManager {

    private String host = "180.76.142.154";

    private int port = 6379;

    // 0 - never expire
    private int expire = 0;

    //timeout for jedis try to connect to redis server, not expire time! In milliseconds
    private int timeout = 0;

    private String password = "123456";

    //private static JedisPool jedisPool = null;

    public RedisManager() {

    }

    /**
     * 初始化方法
     */
    public void init() {
//		if(jedisPool == null){
//			if(password != null && !"".equals(password)){
//				jedisPool = new JedisPool(new JedisPoolConfig(), host, port, timeout, password);
//			}else if(timeout != 0){
//				jedisPool = new JedisPool(new JedisPoolConfig(), host, port,timeout);
//			}else{
//				jedisPool = new JedisPool(new JedisPoolConfig(), host, port);
//			}
//
//		}
    }

    /**
     * get value from redis
     *
     * @param key
     * @return
     */
    public byte[] get(byte[] key) {
//		byte[] value = null;
//		Jedis jedis = jedisPool.getResource();
//		try{
//			value = jedis.get(key);
//		}finally{
//			jedisPool.returnResource(jedis);
//		}
//		return value;
        return Redis.use().get(key);
    }

    /**
     * set
     *
     * @param key
     * @param value
     * @return
     */
    public byte[] set(byte[] key, byte[] value) {
        Jedis jedis = Redis.use().getJedis();
        try {
            jedis.set(key, value);
            if (this.expire != 0) {
                jedis.expire(key, this.expire);
            }
        } finally {
            Redis.use().close(jedis);
        }
//		if(this.expire != 0) {
//			Redis.use().setex(key, this.expire, value);
//		}
        return value;
    }

    /**
     * set
     *
     * @param key
     * @param value
     * @param expire
     * @return
     */
    public byte[] set(byte[] key, byte[] value, int expire) {
        Jedis jedis = Redis.use().getJedis();
        ;
        try {
            jedis.set(key, value);
            if (expire != 0) {
                jedis.expire(key, expire);
            }
        } finally {
            Redis.use().close(jedis);
        }
//		if(this.expire != 0) {
//			Redis.use().set(key, value);
//		}
        return value;
    }

    /**
     * del
     *
     * @param key
     */
    public void del(byte[] key) {
        Redis.use().del(key);
    }

    /**
     * flush
     */
    public void flushDB() {
        Jedis jedis = Redis.use().getJedis();
        try {
            jedis.flushDB();
        } finally {
            Redis.use().close(jedis);
        }
    }

    /**
     * size
     */
    public Long dbSize() {
        Long dbSize = 0L;
        Jedis jedis = Redis.use().getJedis();
        try {
            dbSize = jedis.dbSize();
        } finally {
            Redis.use().close(jedis);
        }
        return dbSize;
    }

    /**
     * keys
     *
     * @param pattern
     * @return
     */
    public Set<byte[]> keys(String pattern) {
        Set<byte[]> keys = null;
        Jedis jedis = Redis.use().getJedis();
        try {
            keys = jedis.keys(pattern.getBytes());
        } finally {
            Redis.use().close(jedis);
        }
        return keys;
    }

    public Set<String> keysObj(String pattern) {

        return Redis.use().keys(pattern);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public void set(String key, Object value, int expire) {
        if (this.expire != 0) {
            Redis.use().setex(key, expire, value);
        }
    }

    public void del(String key) {
        Redis.use().del(key);
    }

    public Object get(String key) {
        return Redis.use().get(key);
    }
}

/**
 * Copyright (c) 2011-2017, dafei 李飞 (myaniu AT gmail DOT com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.epoch.base.shiro;

import java.util.concurrent.ConcurrentMap;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.epoch.base.model.BaseModel;
import com.epoch.platform.sys.user.dao.SysUser;


/**
 * ShiroKit. (Singleton, ThreadSafe)
 *
 * @author dafei
 */
public class ShiroKit {

	/**
	 * 登录成功时所用的页面。
	 */
	private static String successUrl = "/index/index";

	/**
	 * 登录成功时所用的页面。
	 */
	private static String loginUrl = "/";

	/**
	 * 登录成功时所用的页面。
	 */
	private static String unauthorizedUrl ="/";


	/**
	 * Session中保存的请求的Key值
	 */
	private static String SAVED_REQUEST_KEY = "jfinalShiroSavedRequest";


	/**
	 * 用来记录那个action或者actionpath中是否有shiro认证注解。
	 */
	private static ConcurrentMap<String, AuthzHandler> authzMaps = null;

	/**
	 * 禁止初始化
	 */
	private ShiroKit() {}

	static void init(ConcurrentMap<String, AuthzHandler> maps) {
		authzMaps = maps;
	}

	static AuthzHandler getAuthzHandler(String actionKey){
		/*
		if(authzMaps.containsKey(controllerClassName)){
			return true;
		}*/
		return authzMaps.get(actionKey);
	}

	public static final String getSuccessUrl() {
		return successUrl;
	}

	public static final void setSuccessUrl(String successUrl) {
		ShiroKit.successUrl = successUrl;
	}

	public static final String getLoginUrl() {
		return loginUrl;
	}
	
	

	public static final void setLoginUrl(String loginUrl) {
		ShiroKit.loginUrl = loginUrl;
	}

	public static final String getUnauthorizedUrl() {
		return unauthorizedUrl;
	}

	public static final void setUnauthorizedUrl(String unauthorizedUrl) {
		ShiroKit.unauthorizedUrl = unauthorizedUrl;
	}
	/**
	 * Session中保存的请求的Key值
	 * @return
	 */
	public static final String getSavedRequestKey(){
		return SAVED_REQUEST_KEY;
	}
	
    /***
     * 获取登陆用户信息
     *
     * @return
     */
    public static BaseModel getLoginUser() {
        Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            return (BaseModel) subject.getPrincipal();
        } else {
            SecurityUtils.getSubject().logout();
           // LoggerUtil.getLogger().error("-##获取当前登录人信息失败");
            return new BaseModel();
        }
    }

    /***
     * 获取登陆用户主键
     *
     * @return
     */
    public static String getUserId() {
        BaseModel user = ShiroKit.getLoginUser();
        if (user != null) {
            return user.getStr(SysUser.ID);
        }
        return null;
    }
    
    /***
     * 获取用户名称
     *
     * @return
     */
    public static String getUserName() {
        BaseModel user = ShiroKit.getLoginUser();
        if (user != null) {
            return user.getStr(SysUser.USER_NAME);
        }
        return null;
    }

    /***
     * 判断是否有资源权限
     *
     * @param p
     * @return
     */
    public static boolean hasPermission(String p) {
        Subject subject = SecurityUtils.getSubject();
        Boolean result = false;
        try {
            result = ((subject != null) && (subject.isPermitted(p)));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return result;
    }
    
    public static String getUserDeptId() {
        String deptId = null;
        BaseModel<?> user = ShiroKit.getLoginUser();
        if (user != null) {
            deptId = user.getStr(SysUser.DEPT_ID);
        }
        return deptId;
    }

    /***
     * 获取登陆用户id
     *
     * @return
     */
    public static String getAccount() {
        BaseModel user = ShiroKit.getLoginUser();
        if (user != null) {
            return user.getStr(SysUser.ACCOUNT);
        }
        return null;
    }
	
}

package com.epoch.base.shiro;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.servlet.ShiroFilter;

public class EpochShiroFilter extends ShiroFilter {

    @Override
    protected boolean isEnabled(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String uri = httpServletRequest.getRequestURI(); //uri就是获取到的连接地址!
        String ctxPath = httpServletRequest.getContextPath();
        if (uri.startsWith(ctxPath)) {
            uri = uri.substring(ctxPath.length());
        }
        String reg = "^/(?:static|ca|se)/.+|\\.(?:htm|html|js|css|json|png|gif|jpg|jpeg|bmp|ico|exe|txt|zip|rar|7z)$";
        Pattern skiedUrlPattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);

        if (skiedUrlPattern.matcher(uri).matches()) {
            return false;
        }
        return super.isEnabled(request, response);
    }

	
}

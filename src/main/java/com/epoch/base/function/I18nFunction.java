package com.epoch.base.function;

import java.util.Arrays;

import org.beetl.core.Context;
import org.beetl.core.Function;

import com.epoch.base.constant.Constant;
import com.epoch.platform.common.util.I18nUtils;

public class I18nFunction implements Function {
    @Override
    public Object call(Object[] objects, Context context) {
        Object[] params = new Object[0];
        if (objects.length > Constant.INT1) {
            params = Arrays.copyOfRange(objects, 1, objects.length);
        }
        return I18nUtils.get(objects[0].toString(), params);
    }
}

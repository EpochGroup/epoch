package com.epoch.base.function;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.core.JFinal;

public class SetSharedVars {
	
	public static final String PATH = "baseStaticUrl";

    public static Map<String, Object> getSharedVars() {
        Map<String, Object> shared = new HashMap<String, Object>();
        //页面路径设置
        shared.put(PATH, JFinal.me().getServletContext().getContextPath()+"/static");
        //设置静态常量
        setStaticConst(shared);
        return shared;
    }

    /**
     * 设置静态常量
     * <p>
     * 主要用于数据字典的dic_code值存放，便于以后管理
     *
     * @param staticConst Map<String, Object>
     */
    private static void setStaticConst(Map<String, Object> staticConst) {
//        staticConst.put("SYS_COMMON_STATUS", DictCodeConstant.SYS_COMMON_STATUS);
//        staticConst.put("SYS_COMMON2_STATUS", DictCodeConstant.SYS_COMMON2_STATUS);
//        staticConst.put("SYS_COMMON_GENDER", DictCodeConstant.SYS_COMMON_GENDER);
//        staticConst.put("SYS_COMMON_ERP_TYPE", DictCodeConstant.SYS_COMMON_ERP_TYPE);
//        staticConst.put("SUPPLIER_COMPANY_CATEGORY", DictCodeConstant.SUPPLIER_COMPANY_CATEGORY);
//        staticConst.put("SUPPLIER_LEVEL", DictCodeConstant.SUPPLIER_LEVEL);
//        staticConst.put("SUPPLIER_SOURCE", DictCodeConstant.SUPPLIER_SOURCE);
//        staticConst.put("SUPPLIER_STATUS", DictCodeConstant.SUPPLIER_STATUS);
//        staticConst.put("VALID_FLAG", DictCodeConstant.VALID_FLAG);
//        staticConst.put("PURCHASING_METHOD", DictCodeConstant.PURCHASING_METHOD);
//        staticConst.put("BIDDING_ORGANIZATION_FORM", DictCodeConstant.BIDDING_ORGANIZATION_FORM);
//        staticConst.put("DOCUMENT_TYPE", DictCodeConstant.DOCUMENT_TYPE);
//        staticConst.put("PURCHASING_CATEGORIES_STATUS", DictCodeConstant.PURCHASING_CATEGORIES_STATUS);
    }
}
package com.epoch.base.tag;

import org.beetl.core.Context;
import org.beetl.core.Function;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TagSearchFunction implements Function {

	public Object call(Object[] paras, Context ctx) {
		// TODO Auto-generated method stub
		if(paras[0]==null) return Collections.EMPTY_LIST;
		List<HTMLTag> list = (List<HTMLTag>)paras[0];
		
		String attrName = (String)paras[1];
		String attrValue = (String)paras[2];
		boolean returnMatch = true ;
		if(paras.length==4){
			returnMatch = (Boolean)paras[3];
		}
		
		List<HTMLTag> matchList = new ArrayList<HTMLTag>();
		List<HTMLTag> notMatchList = new ArrayList<HTMLTag>();
		for(HTMLTag tag :list){			
			String value = (String)tag.get(attrName);			
			if(value==null){
				if(!returnMatch)notMatchList.add(tag);
				continue;
			}
			if(value.equals(attrValue)){
				if(returnMatch)matchList.add(tag);
			}else{
				if(!returnMatch)notMatchList.add(tag);
			}
				
			
		}
		
		if(returnMatch){
			return matchList;
		}else{
			return notMatchList;
		}
	}

}

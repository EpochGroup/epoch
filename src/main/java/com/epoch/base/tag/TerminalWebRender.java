package com.epoch.base.tag;

import org.beetl.core.GroupTemplate;
import org.beetl.core.ResourceLoader;
import org.beetl.core.Template;
import org.beetl.core.exception.BeetlException;
import org.beetl.ext.web.SessionWrapper;
import org.beetl.ext.web.WebRender;
import org.beetl.ext.web.WebVariable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Enumeration;

public class TerminalWebRender extends WebRender {

    private GroupTemplate group = null;
    private ResourceLoader resourceLoader = null;

    public TerminalWebRender(GroupTemplate gt) {
        super(gt);
        group = gt;
        resourceLoader = gt.getResourceLoader();
        // TODO Auto-generated constructor stub
    }


    public void render(String key, HttpServletRequest request, HttpServletResponse response, Object... args) {
        boolean isMobile = (Boolean) request.getAttribute("isMobile");
        Writer writer = null;
        OutputStream os = null;
        String ajaxId = null;
        Template template = null;
        try

        {
            //			response.setContentType(contentType);
            int ajaxIdIndex = key.lastIndexOf("#");
            if (ajaxIdIndex != -1) {
                ajaxId = key.substring(ajaxIdIndex + 1);
                key = key.substring(0, ajaxIdIndex);
                if (isMobile) {
                    String mobileKey = MobileUtil.genMobileKey(key);
                    if (resourceLoader.exist(mobileKey)) {
                        template = this.group.getAjaxTemplate(mobileKey, ajaxId);
                    } else {
                        //使用pc模板
                        template = this.group.getAjaxTemplate(key, ajaxId);
                    }

                } else {
                    template = this.group.getAjaxTemplate(key, ajaxId);
                }


            } else {
                if (isMobile) {
                    String mobileKey = MobileUtil.genMobileKey(key);
                    if (resourceLoader.exist(mobileKey)) {
                        template = this.group.getTemplate(mobileKey);
                        ;
                    } else {
                        //使用pc模板
                        template = template = group.getTemplate(key);
                    }
                } else {
                    template = group.getTemplate(key);
                }


            }

            Enumeration<String> attrs = request.getAttributeNames();

            while (attrs.hasMoreElements()) {
                String attrName = attrs.nextElement();
                template.binding(attrName, request.getAttribute(attrName));

            }
            WebVariable webVariable = new WebVariable();
            webVariable.setRequest(request);
            webVariable.setResponse(response);
            webVariable.setSession(request.getSession());

            template.binding("session", new SessionWrapper(request, webVariable.getSession()));

            template.binding("servlet", webVariable);
            template.binding("request", request);
            template.binding("ctxPath", request.getContextPath());

            modifyTemplate(template, key, request, response, args);

            if (group.getConf().isDirectByteOutput()) {
                os = response.getOutputStream();
                template.renderTo(os);
            } else {
                writer = response.getWriter();
                template.renderTo(writer);
            }

        } catch (IOException e) {
            handleClientError(e);
        } catch (BeetlException e) {
            handleBeetlException(e, writer);
        } finally {
            try {
                if (writer != null)
                    writer.flush();
                if (os != null) {
                    os.flush();
                }
            } catch (IOException e) {
                handleClientError(e);
            }
        }
    }


    protected void handleBeetlException(BeetlException ex, Writer w) {
        HtmlErrorDisplay handler = new HtmlErrorDisplay();
        handler.processExcption(ex, w);
        try {
            w.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

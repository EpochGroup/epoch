package com.epoch.base.tag;

import org.beetl.core.Resource;
import org.beetl.core.ResourceLoader;
import org.beetl.ext.tag.LayoutTag;

import javax.servlet.http.HttpServletRequest;

public class TerminalLayoutTag extends LayoutTag {
	
	protected String getRelResourceId()
	{

		Resource sibling = ctx.getResource();
		//不要使用resource的loder，因为有可能是
		ResourceLoader resourceLoader = gt.getResourceLoader();
		String key =  gt.getResourceLoader().getResourceId(sibling, (String) this.args[0]);
		HttpServletRequest request = (HttpServletRequest)ctx.getGlobal("request");
		boolean isMobile =(Boolean) request.getAttribute("isMobile");
		if(isMobile){
			String mobileKey = MobileUtil.genMobileKey(key);
			if(resourceLoader.exist(mobileKey)){
				return mobileKey;
			}else{
				return key;
			}
		}else{
			return key;
		}
				
		
	}
}

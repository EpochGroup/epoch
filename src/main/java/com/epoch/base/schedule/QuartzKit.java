package com.epoch.base.schedule;

import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzKit {
	
	private static SchedulerFactory schedulerFactory = new StdSchedulerFactory();

    public static SchedulerFactory getSchedulerFactory() {
        return schedulerFactory;
    }

    public static void setSchedulerFactory(SchedulerFactory schedulerFactorys) {
        schedulerFactory = schedulerFactorys;
    }
}

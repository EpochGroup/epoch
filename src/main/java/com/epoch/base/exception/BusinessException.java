package com.epoch.base.exception;

public class BusinessException extends RuntimeException {
	
	/**
     * 
     */
    private static final long serialVersionUID = -7042316061978744444L;

    public BusinessException(){
		super();
	}
	
	public BusinessException(String msg){
		super(msg);
	}
	
	public BusinessException(String msg,Throwable cause){
		super(msg,cause);		
	}
	
	public BusinessException(Throwable cause){
		super(cause);
	}
}

package com.epoch.base.druid;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.alibaba.druid.pool.DruidDataSource;
import com.jfinal.plugin.druid.DruidPlugin;

public class ExtDruidPlugin extends DruidPlugin {
	
    private List<String> connectionInitSqls = new ArrayList<String>();

    public ExtDruidPlugin(String url, String username, String password, String driverClass, String filters) {
        super(url, username, password, driverClass, filters);
    }

    public ExtDruidPlugin(String url, String username, String password, String driverClass) {
        super(url, username, password, driverClass);
    }

    public ExtDruidPlugin(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public void setConnectionInitSql(String sql) {
        this.connectionInitSqls.add(sql);
    }

    @Override
    public boolean start() {
        super.setConnectionInitSql(null);
        boolean result = super.start();
        if (CollectionUtils.isNotEmpty(this.connectionInitSqls)) {
            List<String> initSqls = new ArrayList<String>(this.connectionInitSqls);
            ((DruidDataSource)this.getDataSource()).setConnectionInitSqls(initSqls);
        }
        return result;
    }
}

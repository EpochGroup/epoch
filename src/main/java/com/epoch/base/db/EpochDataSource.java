package com.epoch.base.db;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.epoch.base.annotation.AutoBindModels;
import com.epoch.base.constant.Constant;
import com.epoch.base.constant.DBConst;
import com.jfinal.config.Plugins;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.cache.ICache;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;

public class EpochDataSource {
	
	public static void initDataSource(Plugins plugins) {
		String dbType = PropKit.use(Constant.PROPERTIES_SNOW_DATA).get(DBConst.DBTYPE);
        String driverClass = PropKit.use(Constant.PROPERTIES_SNOW_DATA).get(dbType.concat(DBConst.DRIVERCLASS));
        String jdbcUrl = PropKit.use(Constant.PROPERTIES_SNOW_DATA).get(dbType.concat(DBConst.JDBCURL));
        String username = PropKit.use(Constant.PROPERTIES_SNOW_DATA).get(dbType.concat(DBConst.USERNAME));
        String password = PropKit.use(Constant.PROPERTIES_SNOW_DATA).get(dbType.concat(DBConst.PASSWORD));
        
        DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, username, password, driverClass);
        //配置Druid数据库连接池大小
        druidPlugin.setInitialSize(PropKit.use(Constant.PROPERTIES_SNOW_DATA).getInt(DBConst.DBINITIALSIZE));
        //最小空闲连接数
        druidPlugin.setMinIdle(PropKit.use(Constant.PROPERTIES_SNOW_DATA).getInt(DBConst.DBMINIDLE));
        //最大并发连接数
        druidPlugin.setMaxActive(PropKit.use(Constant.PROPERTIES_SNOW_DATA).getInt(DBConst.DBMAXACTIVE));
        //配置获取连接等待超时的时间
        druidPlugin.setMaxWait(PropKit.use(Constant.PROPERTIES_SNOW_DATA).getInt(DBConst.DB_SETMAXWAIT));
        //配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
        druidPlugin.setTimeBetweenEvictionRunsMillis(PropKit.use(Constant.PROPERTIES_SNOW_DATA).getInt(DBConst.DB_SETMINEVICTABLEIDLETIMEMILLIS));
        //配置一个连接在池中最小生存的时间，单位是毫秒
        druidPlugin.setMinEvictableIdleTimeMillis(PropKit.use(Constant.PROPERTIES_SNOW_DATA).getInt(DBConst.DB_SETTIMEBETWEENEVICTIONRUNSMILLIS));
        druidPlugin.setTestOnBorrow(false);
        druidPlugin.setTestOnReturn(false);
        //配置Druid数据库连接池过滤器配制
        druidPlugin.addFilter(new StatFilter());
        WallFilter wall = new WallFilter();
        wall.setDbType(PropKit.use(Constant.PROPERTIES_SNOW_DATA).get(DBConst.DBTYPE));
        druidPlugin.addFilter(wall);
        druidPlugin.setValidationQuery("select 1 FROM DUAL");
        //添加druidPlugin插件
        plugins.add(druidPlugin);
        
        //配置ActiveRecordPlugin插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(DBConst.DBDATASOURCEMAIN, druidPlugin);
        plugins.add(arp);
        boolean devMode = PropKit.getBoolean(DBConst.CONFIGDEVMODE);
        // 设置开发模式
        arp.setShowSql(devMode);
        // 是否显示SQL
        arp.setDevMode(devMode);
        if (DBConst.DBTYPE_MYSQL.equals(dbType)) {
            //设置断言
            arp.setDialect(new MysqlDialect());
        } else if (DBConst.DBTYPE_ORACLE.equals(dbType)) {
            //设置断言
            arp.setDialect(new OracleDialect());
        }
        //不区分大小写
        arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
        arp.setTransactionLevel(2);
        //sqlmap
        arp.setBaseSqlTemplatePath(PathKit.getRootClassPath() + "/sqlMap");
        arp.addSqlTemplate("sys_common.sql");
        //arp.addSqlTemplate("all.sql");
        arp.setCache(new ICache() {
            @Override
            public <T> T get(String s, Object o) {
                return Redis.use(s).get(o);
            }

            @Override
            public void put(String s, Object o, Object o1) {
                Redis.use(s).set(o, o1);
            }

            @Override
            public void remove(String s, Object o) {
                Redis.use(s).del(o);
            }

            @Override
            public void removeAll(String s) {
                Cache cache = Redis.use(s);
                cache.del(cache.keys("*").toArray());
            }
        });
        //自动扫描表 
        new AutoBindModels(arp);
	}
}

package com.epoch.base.enums;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class SysThemesUtil {
	
	public static SysThemesEnum getSysTheme(HttpServletRequest request){
		String indexStyle = null;
		try {
			Cookie[] cookies = request.getCookies();
			for (Cookie cookie : cookies) {
				if (cookie == null || StringUtils.isEmpty(cookie.getName())) {
					continue;
				}
				if (cookie.getName().equalsIgnoreCase("EPOCHSTYLE")) {
					indexStyle = cookie.getValue();
				}
			}
		} catch (Exception e) {
		}
		return SysThemesEnum.toEnum(indexStyle);
	}
	
	public static SysThemesEnum getSysTheme(){
		return SysThemesEnum.toEnum(null);
	}
}

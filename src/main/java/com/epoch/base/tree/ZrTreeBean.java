package com.epoch.base.tree;

import java.io.Serializable;
import java.util.List;

public class ZrTreeBean  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String parentId;
	
	private String parentName;
	
	private String name;// 菜单名
	
	private String text;
	
	private String code;//编码
	
	private String link;//链接
	
	private String icon;//图标
	
	private Integer type;//类型
	
	private String comments;// 描述
	
	private Boolean open = false;
	
	private List<ZrTreeBean> children;// 子菜单

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ZrTreeBean() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public List<ZrTreeBean> getChildren() {
		return children;
	}

	public void setChildren(List<ZrTreeBean> children) {
		this.children = children;
	}
	
	
}

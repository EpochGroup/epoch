package com.epoch.base.export;

public class ExportRowNumConvertor implements IExportConvertor {
    @Override
    public void init(String initParamStr) {
    }

    @Override
    public Object convertFieldVal(Object row, String field, int rowIdx, int colIdx, Object val) {
        return Integer.valueOf(rowIdx + 1);
    }
}

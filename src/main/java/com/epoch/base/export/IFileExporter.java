package com.epoch.base.export;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface IFileExporter {
    public File export(Collection<?> data, String folderPath, List<HeadMeta> headMetas,
        Map<Integer, IExportConvertor> indexCvtorMap, Map<String, IExportConvertor> fieldCvtorMap,
        Map<String, Object> params) throws ExportException;

    public String getFileNameExt();
}

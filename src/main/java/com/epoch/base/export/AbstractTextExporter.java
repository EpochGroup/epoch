package com.epoch.base.export;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.epoch.base.util.GlobelConstant;


public abstract class AbstractTextExporter extends AbstractExporter {
    @Override
    protected void doExport(OutputStream out, Collection<?> data, List<HeadMeta> headMetas,
        Map<Integer, IExportConvertor> indexCvtorMap, Map<String, IExportConvertor> fieldCvtorMap,
        Map<String, Object> params) throws IOException, ExportException {
        if (CollectionUtils.isNotEmpty(headMetas)) {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, GlobelConstant.UTF8));
            this.doTextExport(writer, data, headMetas, indexCvtorMap, fieldCvtorMap, params);
            writer.flush();
        }
    }

    protected abstract void doTextExport(BufferedWriter writer, Collection<?> data, List<HeadMeta> headMetas,
        Map<Integer, IExportConvertor> indexCvtorMap, Map<String, IExportConvertor> fieldCvtorMap,
        Map<String, Object> params) throws IOException, ExportException;
}

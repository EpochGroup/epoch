package com.epoch.base.export;

public class ExportException extends Exception {
    private static final long serialVersionUID = 1L;
    private final String i18nErrMsg;

    public ExportException() {
        this((String)null);
    }

    public ExportException(String message, Throwable cause) {
        this(message, cause, message);
    }

    public ExportException(String message) {
        this(message, message);
    }

    public ExportException(Throwable cause) {
        this(cause, null);
    }

    public ExportException(String message, Throwable cause, String i18nErrMsg) {
        super(message, cause);
        this.i18nErrMsg = i18nErrMsg;
    }

    public ExportException(String message, String i18nErrMsg) {
        super(message);
        this.i18nErrMsg = i18nErrMsg;
    }

    public ExportException(Throwable cause, String i18nErrMsg) {
        super(cause);
        this.i18nErrMsg = i18nErrMsg;
    }

    public String getI18nErrMsg() {
        return this.i18nErrMsg;
    }
}

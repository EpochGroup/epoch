package com.epoch.base.export;

public interface IExportConvertor {
    public void init(String initParamStr);

    public Object convertFieldVal(Object row, String field, int rowIdx, int colIdx, Object val);
}

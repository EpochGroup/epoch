package com.epoch.platform.oss.cloud;

import java.io.File;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.epoch.base.util.DateUtils;
import com.epoch.base.util.UuidUtil;
/**
 * 
* <p>Title: CloudStorageService.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月15日
 */
public abstract class CloudStorageService {
	
	/**
     * 文件路径
     * @param prefix 前缀
     * @param suffix 后缀
     * @return 返回上传路径
     */
    public String getPath(String prefix, String suffix) {
        //生成uuid
        String uuid = UuidUtil.getUUID();
        //文件路径
        String path = DateUtils.getDateToString(new Date(), "yyyyMMdd") + "/" + uuid;

        if(StringUtils.isNotBlank(prefix)){
            path = prefix + "/" + path;
        }

        return path + suffix;
    }
    
    public String getPath(String prefix) {
        //生成uuid
        String uuid = UuidUtil.getUUID();
        //文件路径
        String path = DateUtils.getDateToString(new Date(), "yyyyMMdd") + "/" + uuid;

        if (StringUtils.isNotBlank(prefix)) {
            path = prefix + "/" + path;
        }

        return path;
    }
    
    public abstract String upload(File file);
    
    public abstract String upload(File file, String path);
    
    public abstract String uploadSuffix(File file, String suffix);

    /**
     * 文件上传
     * @param data    文件字节数组
     * @param path    文件路径，包含文件名
     * @return        返回http地址
     */
    public abstract String upload(byte[] data, String path);

    /**
     * 文件上传
     * @param data     文件字节数组
     * @param suffix   后缀
     * @return         返回http地址
     */
    public abstract String uploadSuffix(byte[] data, String suffix);

    /**
     * 文件上传
     * @param inputStream   字节流
     * @param path          文件路径，包含文件名
     * @return              返回http地址
     */
    public abstract String upload(InputStream inputStream, String path);

    /**
     * 文件上传
     * @param inputStream  字节流
     * @param suffix       后缀
     * @return             返回http地址
     */
    public abstract String uploadSuffix(InputStream inputStream, String suffix);
}

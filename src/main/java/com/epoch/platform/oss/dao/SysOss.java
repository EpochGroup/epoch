package com.epoch.platform.oss.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: SysOss.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月15日
 */
@ModelBind(key="ID", table="sys_oss")
public class SysOss extends BaseModel<SysOss>{

	public static SysOss dao = new SysOss();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String ID = "ID";
	public static final String URL = "URL";

}

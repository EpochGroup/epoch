package com.epoch.platform.sys.role.service;

import com.epoch.platform.sys.role.dao.SysRole;
import com.epoch.platform.sys.user.dao.SysUser;
import com.epoch.platform.sys.userrole.dao.SysUserRole;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;

public class SysRoleService {
	
	@SuppressWarnings("unchecked")
	public void saveRole(SysUser sysUser,Integer[] roleGroup) {
		int userId = sysUser.getInt(SysUser.ID);
		Kv params = Kv.create();
		params.put("userId", userId);
		SqlPara sqlPara = Db.getSqlPara("systemManage.deleteSysRoleByUserId",params);
		Db.update(sqlPara);
		for (Integer integer : roleGroup) {
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.set(SysUserRole.USER_ID, userId);
			sysUserRole.set(SysUserRole.ROLE_ID, integer);
			sysUserRole.save();
		}
	}
	
	public boolean checkCodeExist(String roleCode) {
		Kv params = Kv.create();
		params.set("roleCode",roleCode);
		SqlPara sqlPara = Db.getSqlPara("systemManage.findRoleIsLeaf",params);
		SysRole sysRole = SysRole.dao.findFirst(sqlPara);
		int count = sysRole.getInt("COUNT");
		if(count == 0) {
			return true;
		}
		return false;
	}
}

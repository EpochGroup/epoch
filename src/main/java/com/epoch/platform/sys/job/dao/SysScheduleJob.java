package com.epoch.platform.sys.job.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "SYS_SCHEDULE_JOB", key = "ID")
public class SysScheduleJob extends BaseModel<SysScheduleJob> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final SysScheduleJob dao = new SysScheduleJob();

	public static final String ID = "ID"; //
	public static final String JOB_NAME = "JOB_NAME"; //
	public static final String JOB_GROUP = "JOB_GROUP"; //
	public static final String JOB_STATUS = "JOB_STATUS"; //
	public static final String TARGET_CLASS = "TARGET_CLASS"; //
	public static final String CRON_EXPRESSION = "CRON_EXPRESSION"; //
	public static final String JOB_DESC = "JOB_DESC"; //

}

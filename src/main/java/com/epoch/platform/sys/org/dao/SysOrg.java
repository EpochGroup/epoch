package com.epoch.platform.sys.org.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "sys_org")
public class SysOrg extends BaseModel<SysOrg>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final SysOrg dao = new SysOrg();
	
	public static final String ID = "ID";
	public static final String ORG_CODE = "ORG_CODE";
	public static final String ORG_NAME = "ORG_NAME";
	public static final String PARENT_ID = "PARENT_ID";
	public static final String COMMENTS = "COMMENTS";
	public static final String STATE = "STATE";
	public static final String STATUS = "STATUS";
	public static final String LEAF = "LEAF";
	public static final String SHORT_NAME = "SHORT_NAME";
	public static final String LONG_NAME = "LONG_NAME";
	public static final String ORG_TYPE = "ORG_TYPE";
}

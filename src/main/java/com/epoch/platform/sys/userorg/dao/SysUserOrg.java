package com.epoch.platform.sys.userorg.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "SYS_USER_ORG")
public class SysUserOrg extends BaseModel<SysUserOrg>{
	
	public final static SysUserOrg dao=new SysUserOrg();
	
	public static final String ID = "ID"; // 主键id
	public static final String USER_ID = "USER_ID"; // 用户id
	public static final String ORG_ID = "ORG_ID"; // 组织id
}

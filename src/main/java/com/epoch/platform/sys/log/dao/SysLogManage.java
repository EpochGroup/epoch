package com.epoch.platform.sys.log.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "SYS_LOG_MANAGE", key = "ID")
public class SysLogManage extends BaseModel<SysLogManage> {
	
    public static final SysLogManage dao = new SysLogManage();
    public static final String ID = "ID"; // ID
    public static final String MENU_ID = "MENU_ID"; // 菜单id
    public static final String NEW_BTN = "NEW_BTN"; // 新建按钮
    public static final String DEL_BTN = "DEL_BTN"; // 删除按钮
    public static final String UPDATE_BTN = "UPDATE_BTN"; // 修改按钮
    public static final String QUERY_BTN = "QUERY_BTN"; // 查询按钮
    public static final String CREATED_BY = "CREATED_BY"; // 创建人id
    public static final String CREATED_DATE = "CREATED_DATE"; // 创建时间
    public static final String UPDATED_BY = "UPDATED_BY"; // 最后更新人id
    public static final String UPDATED_DATE = "UPDATED_DATE"; // 更新时间


}

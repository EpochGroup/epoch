package com.epoch.platform.sys.log.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "SYS_LOG", key = "ID")
public class SysLog extends BaseModel<SysLog> {
    public static final SysLog dao = new SysLog();
    public static final String ID = "ID"; // 主键id
    public static final String USER_ID = "USER_ID"; // 用户id
    public static final String LOGIN_IP = "LOGIN_IP"; // 访问IP
    public static final String LOGIN_DATE = "LOGIN_DATE"; // 登入时间
    public static final String LOOUT_DATE = "LOOUT_DATE"; // 登出时间
    public static final String USER_DEPT_ID = "USER_DEPT_ID"; // 操作部门id
    public static final String USER_DEPT_DESC = "USER_DEPT_DESC"; // 操作部门名称
    public static final String OPERATION_TYPE_ID = "OPERATION_TYPE_ID"; // 操作类型id 登陆，登出，菜单点击，删，改，查，审批通过/打回
    public static final String OPERATION_NOTE = "OPERATION_NOTE"; // 操作描述
    public static final String LOGIN_STATUS = "LOGIN_STATUS"; // 操作状态S/F: 成功/失败
    public static final String BUSSINESS_NUMBER = "BUSSINESS_NUMBER"; // 业务单据编号
    public static final String MENU_ID = "MENU_ID"; // 菜单编号
    public static final String MENU_DESC = "MENU_DESC"; // 菜单名称
    public static final String OPERATION_FAILED_REASON = "OPERATION_FAILED_REASON"; // 操作失败原因
    public static final String OPERATION_ROLE_ID = "OPERATION_ROLE_ID"; // 角色id
    public static final String OPERATION_ROLE_DESC = "OPERATION_ROLE_DESC"; // 角色名称
    public static final String OPERATION_POSITION_ID = "OPERATION_POSITION_ID"; // 职位id
    public static final String OPERATION_POSITION_DESC = "OPERATION_POSITION_DESC"; // 职位名称
    public static final String CREATED_BY = "CREATED_BY"; // 创建人id
    public static final String CREATED_DATE = "CREATED_DATE"; // 创建时间
    public static final String UPDATED_BY = "UPDATED_BY"; // 最后更新人id
    public static final String UPDATED_DATE = "UPDATED_DATE"; // 更新时间
    public static final String MODULE_CODE = "MODULE_CODE"; // 模块code
    public static final String EMPLOYEE_NO = "EMPLOYEE_NO"; // 员工工号
    public static final String USER_NAME = "USER_NAME"; // 姓名

    /**
     * 模块名称
     *
     * @param moduleCode 根据常量  ConstantLog.java
     */
    public void setModuleCode(String moduleCode) {
        set(MODULE_CODE, moduleCode);
    }

    /**
     * 菜单code  创建菜单时候，需要创建菜单编码
     *
     * @param menuCode 菜单编码 ConstantLog.java
     */
    public void setMenuCode(String menuCode) {
        set(MENU_ID, menuCode);
    }

    /**
     * 操作类型id 登陆，登出，菜单点击，删，改，查，审批通过/打回
     *
     * @param operationType 操作类型 ConstantLog.java
     */
    public void setOperationType(String operationType) {
        set(OPERATION_TYPE_ID, operationType);
    }

    /**
     * 操作描述
     *
     * @param operationNote 操作描述
     */
    public void setOperationNote(String operationNote) {
        set(OPERATION_NOTE, operationNote);
    }

    /**
     * 操作失败原因
     *
     * @param failedReason 操作失败原因
     */
    public void setFailedReason(String failedReason) {
        set(OPERATION_FAILED_REASON, failedReason);
    }

    /**
     * 业务单据编号
     *
     * @param bussinessNumber 业务单据编号
     */
    public void setNussinessNumber(String bussinessNumber) {
        set(BUSSINESS_NUMBER, bussinessNumber);
    }

    /**
     * 失败构造函数
     *
     * @param moduleCode      模块名称 ConstantLog.java
     * @param menuCode        菜单code ConstantLog.java
     * @param operationType   操作类型id 登陆，登出，菜单点击，删，改，查，审批通过/打回 ConstantLog.java
     * @param operationNote   操作描述
     * @param failedReason    操作失败原因
     * @param bussinessNumber 业务单据编号
     */
    public SysLog(String moduleCode, String menuCode, String operationType, String operationNote, String failedReason, String bussinessNumber) {
        set(MODULE_CODE, moduleCode);
        set(MENU_ID, menuCode);
        set(OPERATION_TYPE_ID, operationType);
        set(OPERATION_NOTE, operationNote);
        set(OPERATION_FAILED_REASON, failedReason);
        set(BUSSINESS_NUMBER, bussinessNumber);
    }

    /**
     * 成功构造函数
     *
     * @param moduleCode      模块名称 ConstantLog.java
     * @param menuCode        菜单code ConstantLog.java
     * @param operationType   操作类型id 登陆，登出，菜单点击，删，改，查，审批通过/打回 ConstantLog.java
     * @param operationNote   操作描述
     * @param bussinessNumber 业务单据编号
     */
    public SysLog(String moduleCode, String menuCode, String operationType, String operationNote, String bussinessNumber) {
        set(MODULE_CODE, moduleCode);
        set(MENU_ID, menuCode);
        set(OPERATION_TYPE_ID, operationType);
        set(OPERATION_NOTE, operationNote);
        set(BUSSINESS_NUMBER, bussinessNumber);
    }

    /**
     * 默认构造方法
     */
    public SysLog() {
    }

}

package com.epoch.platform.sys.dict.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.base.BaseController;
import com.epoch.base.util.AjaxResult;
import com.epoch.base.util.MessageConstant;
import com.epoch.platform.sys.dict.dao.SysDict;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;

@ControllerBind(controllerKey = "/sys/dict")
public class SysDictController extends BaseController {

	// AjaxFormResult ajaxResult = new AjaxFormResult();

	/**
	 * 跳转到选项卡页面
	 */
	public void index() {
		render("/sys/dict/sys_dict_tab.html");
	}
	
	public void listUI() {
		render("/sys/dict/sys_dict_list.html");
	}
	
	public void suvUI() {
		String id = getPara("ID");
		SysDict sysDict;
		if (StringUtils.isNotBlank(id)) {
			sysDict = SysDict.dao.findById(id);
		} else {
			sysDict = new SysDict();
		}
		setAttr("sysDict", sysDict);
		render("/sys/dict/sys_dict_edit.html");
	}

	public void findSysDictListAjax() {
		// 保存一个AjaxResult对象进入域中
		Kv param = commonParams();
		SqlPara sqlPara = Db.getSqlPara("systemManage.findSysDictList", param);
		Page<SysDict> sysDictList = SysDict.dao.paginate(sqlPara, param);
		renderJson(sysDictList);
	}

	public void findSysDictItemListAjax() {
		Kv param = commonParams();
		String dict_code = getPara("dict_code");
		param.set("dict_code", dict_code);
		SqlPara sqlPara = Db.getSqlPara("systemManage.findDictItemByDicCode", param);
		List<SysDict> sysDictItemList = SysDict.dao.find(sqlPara);
		Page<SysDict> page = new Page<>(sysDictItemList, 0, 0, 0, 0);
		renderJson(page);
	}

	public void saveAll() {
		AjaxResult ajaxResult = new AjaxResult();
		SysDict sysDict;
		try {
			sysDict = getModel(SysDict.class, "sysDict");
			if (sysDict != null) {
				sysDict.set("head_flag", "Y");
				if (sysDict.saveOrUpdate()) {
					ajaxResult.setData(sysDict);
					String dict_code = sysDict.get("dict_code");
					String dict_name = sysDict.get("dict_name");
					Map<String, List<SysDict>> editData = this.getTableEditParams(SysDict.class, "editData");
					System.out.println("editData:" + editData);
					List<SysDict> delRows = editData.get("delRows");
					for (SysDict sysDictItem : delRows) {
						Object id = sysDictItem.get("ID");
						sysDictItem.deleteById(id);
					}
					List<SysDict> editRows = editData.get("editRows");
					for (SysDict sysDictItem : editRows) {
						sysDictItem.saveOrUpdate();
					}
					List<SysDict> newRows = editData.get("newRows");
					for (SysDict sysDictItem : newRows) {
						sysDictItem.set("head_flag", "N");
						sysDictItem.set("dict_code", dict_code);
						sysDictItem.set("dict_name", dict_name);
						sysDictItem.saveOrUpdate();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		renderJson(ajaxResult);
	}

	// 校验字典名称是否重复
	public void checkDictName() {
		String id = getPara("id");
		String dicName = getPara("dicName");
		String msg = "true";
		Record record = new Record();
		if (id != null && id != "") {
			record = Db.findFirst(
					"SELECT COUNT(*) FROM SYS_DICT WHERE DICT_NAME = '" + dicName + "'AND ID != '" + id + "'");
		} else {
			record = Db.findFirst("SELECT COUNT(*) FROM SYS_DICT WHERE DICT_NAME = '" + dicName + "'");
		}
		Long count = record.getLong("count(*)");
		if (count > 0) {
			msg = "false";
		}
		renderText(msg);
	}
	
	//校验字典编码是否重复
	public void checkDictCode(){
    	String id = getPara("id");
        String dicCode = getPara("dictCode");
        String msg = "true";
        Record record=new Record();
        if(id!=null&&id!=""){
        	record = Db.findFirst("SELECT COUNT(*) FROM SYS_DICT WHERE DICT_CODE = '"+dicCode+"'AND ID != '"+id+"'");
        }else{	        	
        	record = Db.findFirst("SELECT COUNT(*) FROM SYS_DICT WHERE DICT_CODE = '"+dicCode+"'");  
        }
        Long count = record.getLong("count(*)");
	    if(count>0){
	    	msg = "false";
        }   
        renderText(msg);
    }
	
	public void delete() {
        String[] ids = getParaValues("ids[]");
        AjaxResult ajaxResult = new AjaxResult();
        try {
            if (ids.length > 0) {
                for (String id : ids) {
                    SysDict sysDict = SysDict.dao.findById(id);
                    String dict_code = sysDict.getStr("dict_code");
                    Db.update(Db.getSql("systemManage.deleteSysDictByDicCode"), dict_code);
                }
                ajaxResult.success(MessageConstant.MESSAGE_SUCCESS);
            }
        } catch (Exception e) {
            ajaxResult.addError(MessageConstant.MESSAGE_ERROR);
        }
        redirect("/sys/dict");
    }
}

package com.epoch.platform.sys.dict.service;

import java.util.List;

import com.epoch.platform.sys.dict.dao.SysDict;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;

public class SysDictService {
	
	public List<SysDict>  findSysDictInfoByDictCode(String dictCode){
		Kv params = Kv.create();
		params.set("dict_code",dictCode);
		params.set("head_flag","N");
		SqlPara sqlPara = Db.getSqlPara("systemManage.findSysDictInfoByParams", params);
		List<SysDict> sysDictInfoList = SysDict.dao.find(sqlPara);
		return sysDictInfoList;
	}
}

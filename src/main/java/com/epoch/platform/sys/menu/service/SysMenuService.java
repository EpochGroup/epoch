package com.epoch.platform.sys.menu.service;

import java.util.ArrayList;
import java.util.List;

import com.epoch.base.tree.TreeUtil;
import com.epoch.base.tree.ZrTreeBean;
import com.epoch.platform.sys.menu.dao.SysMenu;
import com.epoch.platform.sys.role.dao.SysRole;
import com.epoch.platform.sys.rolemenu.dao.SysRoleMenu;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;

public class SysMenuService {

	public List<SysMenu> findSysMenuList(Kv params) {
		SqlPara sqlPara = Db.getSqlPara("systemManage.findSysMenList",params);
		List<SysMenu> sysMenuList = SysMenu.dao.find(sqlPara);
		return sysMenuList;
	}
	
	public List<ZrTreeBean>  getAllTreeMenuList(Kv params){
		SqlPara sqlPara = Db.getSqlPara("systemManage.findAllSysMenuList",params);
		List<SysMenu> sysMenuList = SysMenu.dao.find(sqlPara);
		List<ZrTreeBean> allTreeList = new ArrayList<ZrTreeBean>();
		for (SysMenu sysMenu : sysMenuList) {
			ZrTreeBean tree = new ZrTreeBean();
			tree.setId(sysMenu.getStr(SysMenu.ID));
			tree.setParentId(sysMenu.getStr(SysMenu.PARENT_ID));
			tree.setName(sysMenu.getStr(SysMenu.MENU_NAME));
			tree.setCode(sysMenu.getStr(SysMenu.MENU_CODE));
			tree.setLink(sysMenu.getStr(SysMenu.MENU_URL));
			allTreeList.add(tree);
		}
		return TreeUtil.getTreeList(allTreeList);
	}
	
	public List<ZrTreeBean>  getAllTreeMenuList(){
		SqlPara sqlPara = Db.getSqlPara("systemManage.findAllSysMenuList");
		List<SysMenu> sysMenuList = SysMenu.dao.find(sqlPara);
		List<ZrTreeBean> allTreeList = new ArrayList<ZrTreeBean>();
		for (SysMenu sysMenu : sysMenuList) {
			ZrTreeBean tree = new ZrTreeBean();
			tree.setId(sysMenu.getStr(SysMenu.ID));
			tree.setParentId(sysMenu.getStr(SysMenu.PARENT_ID));
			tree.setName(sysMenu.getStr(SysMenu.MENU_NAME));
			tree.setCode(sysMenu.getStr(SysMenu.MENU_CODE));
			tree.setIcon(sysMenu.getStr(SysMenu.MENU_ICON));
			tree.setLink(sysMenu.getStr(SysMenu.MENU_URL));
			tree.setType(sysMenu.getInt(SysMenu.MENU_TYPE));
			allTreeList.add(tree);
		}
		return TreeUtil.getTreeList(allTreeList);
	}
	
	public List<ZrTreeBean>  getAllZtreeTreeMenuList(){
		SqlPara sqlPara = Db.getSqlPara("systemManage.findAllSysMenuList");
		List<SysMenu> sysMenuList = SysMenu.dao.find(sqlPara);
		List<ZrTreeBean> allTreeList = new ArrayList<ZrTreeBean>();
		for (SysMenu sysMenu : sysMenuList) {
			ZrTreeBean tree = new ZrTreeBean();
			tree.setId(sysMenu.getStr(SysMenu.ID));
			tree.setParentId(sysMenu.getStr(SysMenu.PARENT_ID));
			tree.setName(sysMenu.getStr(SysMenu.MENU_NAME));
			tree.setCode(sysMenu.getStr(SysMenu.MENU_CODE));
			tree.setIcon(sysMenu.getStr(SysMenu.MENU_ICON));
			tree.setLink(sysMenu.getStr(SysMenu.MENU_URL));
			tree.setType(sysMenu.getInt(SysMenu.MENU_TYPE));
			allTreeList.add(tree);
		}
		return allTreeList;
	}
	
	public List<ZrTreeBean>  getTreeMenuList(String userId){
		Kv params = Kv.create();
		params.put("user_id", userId);
		SqlPara sqlPara = Db.getSqlPara("systemManage.findSysMenuListByUserId",params);
		List<SysMenu> sysMenuList = SysMenu.dao.find(sqlPara);
		List<ZrTreeBean> allTreeList = new ArrayList<ZrTreeBean>();
		for (SysMenu sysMenu : sysMenuList) {
			ZrTreeBean tree = new ZrTreeBean();
			tree.setId(sysMenu.getStr(SysMenu.ID));
			tree.setParentId(sysMenu.getStr(SysMenu.PARENT_ID));
			tree.setName(sysMenu.getStr(SysMenu.MENU_NAME));
			tree.setCode(sysMenu.getStr(SysMenu.MENU_CODE));
			tree.setIcon(sysMenu.getStr(SysMenu.MENU_ICON));
			tree.setLink(sysMenu.getStr(SysMenu.MENU_URL));
			tree.setType(sysMenu.getInt(SysMenu.MENU_TYPE));
			allTreeList.add(tree);
		}
		return TreeUtil.getTreeList(allTreeList);
		
	}
	
	public List<String> getRoleMenu(List<SysRole> sysRoleList){
		List<String> list = new ArrayList<String>();
		for (SysRole sysRole : sysRoleList) {
			list.add(sysRole.getMyString(SysRoleMenu.MENU_ID));
		}
		return list;
	}
	
	public List<SysMenu>  getTreeMenuListAjax(Kv params){
		SqlPara sqlPara = Db.getSqlPara("systemManage.findSysMenuListAjax",params);
		if(null == params.get("parent_id") ) {
			sqlPara = Db.getSqlPara("systemManage.findSysMenuListAjaxOne");
		}
		List<SysMenu> sysMenuList = SysMenu.dao.find(sqlPara);
		return sysMenuList;
	}
	
	public boolean checkCodeExist(String code) {
		Kv params = Kv.create();
		params.set("menu_code",code);
		SqlPara sqlPara = Db.getSqlPara("systemManage.findMenuCodeCount",params);
		SysMenu sysMenu = SysMenu.dao.findFirst(sqlPara);
		int count = sysMenu.getInt("COUNT");
		if(count == 0) {
			return false;
		}
		return true;
	}
	
	public boolean checkMenuIsLeaf(String menuId) {
		Kv params = Kv.create();
		params.set("parent_id",menuId);
		SqlPara sqlPara = Db.getSqlPara("systemManage.findMenuIsLeaf",params);
		SysMenu sysMenu = SysMenu.dao.findFirst(sqlPara);
		int count = sysMenu.getInt("COUNT");
		if(count == 0) {
			return true;
		}
		return false;
	}
	
	public SysMenu findParentMenu(Integer parentId) {
		Kv params = Kv.create();
		params.set("parent_id",parentId);
		SqlPara sqlPara = Db.getSqlPara("systemManage.findParentMenu",params);
		SysMenu sysMenu = SysMenu.dao.findFirst(sqlPara);
		return sysMenu;
	}
}

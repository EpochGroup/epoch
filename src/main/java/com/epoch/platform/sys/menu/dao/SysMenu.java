package com.epoch.platform.sys.menu.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "sys_menu")
public class SysMenu extends BaseModel<SysMenu>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final SysMenu dao = new SysMenu();

	public static final String ID = "ID";
	public static final String MENU_CODE = "MENU_CODE";
	public static final String MENU_NAME = "MENU_NAME";
	public static final String MENU_LEVEL = "MENU_LEVEL";
	public static final String PARENT_ID = "PARENT_ID";
	public static final String COMMENTS = "COMMENTS";
	public static final String STATUS = "STATUS";
	public static final String LEAF = "LEAF";
	public static final String STATE = "STATE";
	public static final String MENU_URL = "MENU_URL";
	public static final String MENU_ORDER = "MENU_ORDER";
	public static final String MENU_ICON = "MENU_ICON";
	public static final String MENU_TYPE = "MENU_TYPE";
}

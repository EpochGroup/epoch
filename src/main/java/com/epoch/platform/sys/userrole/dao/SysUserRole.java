package com.epoch.platform.sys.userrole.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(key="ID",table="SYS_USER_ROLE")
public class SysUserRole extends BaseModel<SysUserRole> {
	
	private static final long serialVersionUID = 7191111984293176737L;
	
	public final static SysUserRole dao = new SysUserRole();
	public static final String ID = "ID"; // 主键id
	public static final String USER_ID = "USER_ID"; // 用户id
	public static final String ROLE_ID = "ROLE_ID"; // 角色id
	public static final String EFFECT_START_DATE = "EFFECT_START_DATE"; // 生效时间
	public static final String EFFECT_END_DATE = "EFFECT_END_DATE"; // 失效时间
}

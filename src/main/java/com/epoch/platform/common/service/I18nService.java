package com.epoch.platform.common.service;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.redis.Redis;

public class I18nService {
	
	public static final String SQL_KEY_FINDI18NS = "sysCommon.findI18ns";
    public static final String I18N_FIELD_ID = "id";
    public static final String I18N_FIELD_FIELD = "field";
    public static final String I18N_FIELD_LANG = "lang";
    public static final String I18N_FIELD_I18N = "i18n";
    public static final int SQL_BATCH_LIMIT = 1000;
    private static final String I18N_CACHE_KEY_PRE = "_i18nTableCache_";
    
    private Record buildI18nRecord(BigDecimal id, String field, String lang, String i18n) {
        Record record = new Record();
        record.set(I18nService.I18N_FIELD_ID, id);
        record.set(I18nService.I18N_FIELD_FIELD, field);
        record.set(I18nService.I18N_FIELD_LANG, lang);
        record.set(I18nService.I18N_FIELD_I18N, i18n);
        return record;
    }
    
    private <E> E getCache(String tableName, String cacheKey) {
        E result = null;
        String mainKey = buildCacheMainKey(tableName);
        result = Redis.use().hget(mainKey, cacheKey);
        return result;
    }

    private <E> void setCache(String tableName, String cacheKey, E data) {
        String mainKey = buildCacheMainKey(tableName);
        Redis.use().hset(mainKey, cacheKey, data);
    }

    private void clearTableCache(String tableName) {
        String mainKey = buildCacheMainKey(tableName);
        Redis.use().del(mainKey);
    }

    private String buildCacheMainKey(String tableName) {
        return I18N_CACHE_KEY_PRE + StringUtils.lowerCase(tableName);
    }
}

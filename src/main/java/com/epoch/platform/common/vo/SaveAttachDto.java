package com.epoch.platform.common.vo;

import java.io.Serializable;
import java.util.List;

public class SaveAttachDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<AttachInfoDto> newFiles;
    private List<AttachInfoDto> oldFiles;
    private List<AttachInfoDto> delFiles;

    public List<AttachInfoDto> getNewFiles() {
        return this.newFiles;
    }

    public void setNewFiles(List<AttachInfoDto> newFiles) {
        this.newFiles = newFiles;
    }

    public List<AttachInfoDto> getOldFiles() {
        return this.oldFiles;
    }

    public void setOldFiles(List<AttachInfoDto> oldFiles) {
        this.oldFiles = oldFiles;
    }

    public List<AttachInfoDto> getDelFiles() {
        return this.delFiles;
    }

    public void setDelFiles(List<AttachInfoDto> delFiles) {
        this.delFiles = delFiles;
    }
}

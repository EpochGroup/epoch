package com.epoch.platform.common.vo;

import java.io.Serializable;
import java.util.Date;

import com.epoch.platform.common.dao.SysAttachmentFiles;

public class AttachInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private Long size;
    private Date uploadTime;
    private String descn;
    private String downloadUri;
    private String fileKey;

    public AttachInfoDto() {
        super();
    }

    public AttachInfoDto(SysAttachmentFiles attach) {
        super();
        if (attach != null) {
            String pkStr = attach.getStr(SysAttachmentFiles.ATTACHMENT_FILE_ID).toString();
            this.id = pkStr;
            this.name = attach.getStr(SysAttachmentFiles.FILE_ORIGINAL_NAME);
            this.size = attach.getLong(SysAttachmentFiles.FILE_SIZE);
            this.uploadTime = attach.getTimestamp(SysAttachmentFiles.CREATE_DATE);
            this.descn = attach.getStr(SysAttachmentFiles.DESCRIPTION);
            this.fileKey = pkStr;
        }
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Date getUploadTime() {
        return this.uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public String getDescn() {
        return this.descn;
    }

    public void setDescn(String descn) {
        this.descn = descn;
    }

    public String getDownloadUri() {
        return this.downloadUri;
    }

    public void setDownloadUri(String downloadUri) {
        this.downloadUri = downloadUri;
    }

    public String getFileKey() {
        return this.fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }
}

package com.epoch.platform.common.util;

public interface CommonAttachConstant {
    public static final String PROP_ATTACHBASEPATH = "config.attachment.basePath";
    public static final String PARAM_BUSINESSID = "businessId"; //参数名：业务单据ID
    public static final String PARAM_BUSINESSTYPE = "businessType"; //参数名：业务单据类别
    public static final String PARAM_FILEKEY = "fileKey"; //参数名：附件文件key
    public static final String PARAM_NEWUP = "newUp"; //参数名：下载的文件是否为临时新上传的
    public static final String PARAM_ID = "id"; //参数名：附件文件ID
    public static final String PARAM_NAME = "name"; //参数名：附件文件名
    public static final String PARAM_SIZE = "size"; //参数名：附件文件大小
    public static final String PARAM_UPLOADTIME = "uploadTime"; //参数名：附件文件上传时间
    public static final String PARAM_DESCN = "descn"; //参数名：附件文件描述
    public static final String PARAM_DOWNLOADURI = "downloadUri"; //参数名：文件下载URI
    public static final String PARAM_SERVER_PATH = "serverPath"; //参数名：附件Server路径
    public static final String ATTACH_INFOS = "attachInfos"; //参数名：附件信息列表
    public static final String ARC_FILE_ID_PRE = "arc_"; //已存档附件文件ID前缀
    public static final String FILE_KEY_PRE = "_file_"; //上传文件key前缀
}

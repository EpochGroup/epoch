package com.epoch.platform.common.util;

import com.epoch.base.constant.Constant;
import com.epoch.base.intercepter.EpochI18nInterceptor;
import com.jfinal.i18n.I18n;

public class I18nUtils {
	
	public static final String I18N_TABLE_SUF = "_TL";
    public static final String I18N_FORM_DATA_SUF = "_i18n_form_data";
    private static final I18nUtils OBJ = new I18nUtils();
    private static final String COMMON_I18N_NAME = "i18n_common";
    
    public static String get(String key, Object... params) {
        String result = null;
        String locale = EpochI18nInterceptor.getCurLocale();
        try {
            if (params.length > Constant.INT0) {
                try {
                    result = I18n.use(locale).format(key, params);
                } catch (Exception e) {
                    result = I18n.use(COMMON_I18N_NAME, locale).format(key, params);
                }
            } else {
                try {
                    result = I18n.use(locale).get(key);
                } catch (Exception e) {
                    result = I18n.use(COMMON_I18N_NAME, locale).get(key);
                }
            }
        } catch (Exception e) {
            //LOGGER.info("-##I18nUtils get i18n failed. key:{},params:{},errorInfo:{}", key, params, e);
            result = key;
        }
        return result;
    }
}

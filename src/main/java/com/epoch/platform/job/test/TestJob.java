package com.epoch.platform.job.test;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class TestJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("<================>");
		System.out.println("测试任务执行中。。。");
		System.out.println("<================>");
	}

}

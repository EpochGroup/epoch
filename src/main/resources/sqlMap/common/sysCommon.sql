###namespace = "sysCommon"
###根据业务单据ID及业务单据类型查询附件信息

#sql("findAttachByBusinessIdAndType")
SELECT t.* FROM sys_attachment_files t 
WHERE t.business_id = #para(businessId) 
#if(businessType??)
  AND t.business_type = #para(businessType) 
#end
ORDER BY t.create_date DESC 
#end


###根据附件Server路径统计附件文件被引用数量
#sql("countAttachByServerPath")
SELECT COUNT(1) FROM sys_attachment_files t 
WHERE t.server_upload_path = #para(serverPath) 
#end
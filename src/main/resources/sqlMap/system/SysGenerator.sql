#sql("findDbDataList")
select table_name tableName, engine, table_comment tableComment, create_time createTime from information_schema.tables 
			where table_schema = (select database()) 
#if(tablename??) AND INSTR(UPPER(table_name),UPPER(#para(tablename))) > 0 #end
order by create_time desc
#end

#sql("findTable")
select table_name tableName, engine, table_comment tableComment, create_time createTime from information_schema.tables 
			where table_schema = (select database()) and table_name = #para(tableName)
#end

#sql("findTableStart")
select table_name tableName, engine, table_comment tableComment, create_time createTime from information_schema.tables 
			where table_schema = (select database())  and table_name like concat(#para(tableName), '%')	
#end

findTable

#sql("findColumns")
select column_name columnName, data_type dataType, column_comment columnComment, column_key columnKey, extra from information_schema.columns
 			where table_name = #para(tableName) and table_schema = (select database()) order by ordinal_position
#end
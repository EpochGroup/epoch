#--日志管理平台查询日志--#
#sql("findSysLogByParam")
select * from sys_log l where 1=1
  #if(_fuzzyParams.user_name??)
	  and Upper(l.user_name) like Upper(#para(_fuzzyParams.user_name)) escape '/'
	#end
	#if(_fuzzyParams.employee_no??)
	  and Upper(l.employee_no) like Upper(#para(_fuzzyParams.employee_no)) escape '/'
	#end
	#if(_fuzzyParams.module_code??)
	  and Upper(l.module_code) like Upper(#para(_fuzzyParams.module_code)) escape '/'
	#end
	#if(_fuzzyParams.operation_position_desc??)
	  and Upper(l.operation_position_desc) like Upper(#para(_fuzzyParams.operation_position_desc)) escape '/'
	#end
	#if(_fuzzyParams.operation_role_desc??)
	  and Upper(l.operation_role_desc) like Upper(#para(_fuzzyParams.operation_role_desc)) escape '/'
	#end
	#if(_fuzzyParams.operation_failed_reason??)
	  and Upper(l.operation_failed_reason) like Upper(#para(_fuzzyParams.operation_failed_reason)) escape '/'
	#end
	#if(_fuzzyParams.menu_desc??)
	  and Upper(l.menu_desc) like Upper(#para(_fuzzyParams.menu_desc)) escape '/'
	#end
	#if(_fuzzyParams.menu_id??)
	  and Upper(l.menu_id) like Upper(#para(_fuzzyParams.menu_id)) escape '/'
	#end
	#if(_fuzzyParams.bussiness_number??)
	  and Upper(l.bussiness_number) like Upper(#para(_fuzzyParams.bussiness_number)) escape '/'
	#end
	#if(_fuzzyParams.login_status??)
	  and Upper(l.login_status) like Upper(#para(_fuzzyParams.login_status)) escape '/'
	#end
	#if(_fuzzyParams.operation_type_id??)
	  and Upper(l.operation_type_id) like Upper(#para(_fuzzyParams.operation_type_id)) escape '/'
	#end
	#if(_fuzzyParams.user_dept_desc??)
	  and Upper(l.user_dept_desc) like Upper(#para(_fuzzyParams.user_dept_desc)) escape '/'
	#end
	#if(_fuzzyParams.operation_note??)
	  and Upper(l.operation_note) like Upper(#para(_fuzzyParams.operation_note)) escape '/'
	#end
	#if(_fuzzyParams.login_ip??)
	  and Upper(l.login_ip) like Upper(#para(_fuzzyParams.login_ip)) escape '/'
	#end
	#if(created_date??)###创建日期
        and to_char(l.created_date, 'yyyy-MM-dd') = #para(created_date)
    #end
    #if(login_date??)###登入时间
        and to_char(l.login_date, 'yyyy-MM-dd') = #para(login_date)
    #end
    #if(loout_date??)###登出时间
        and to_char(l.loout_date, 'yyyy-MM-dd') = #para(loout_date)
    #end
	order by #if(sortName??)#(sortName) #if(sortOrder?? && sortOrder=="desc")desc#end #else l.update_date desc #end
#end



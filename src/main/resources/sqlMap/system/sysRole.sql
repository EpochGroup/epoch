#sql("findSysRole")
select * from sys_role where 1 = 1
#if(role_name??) and INSTR(UPPER(role_name),UPPER(#para(role_name))) > 0 #end
#if(role_code??) and INSTR(UPPER(role_code),UPPER(#para(role_code))) > 0 #end
#if(comments??) and INSTR(UPPER(comments),UPPER(#para(comments))) > 0 #end
#if(status??) and status = #para(status) #end
#if(role_type??) and role_type = #para(role_type) #end
#end

--查询角色弹出框公共
#sql("findSysRole.meta")
  {
    rowId:"id",
    colDatas:[
        {field:"role_name",head:"角色名称",width:"15%",switchable:"false"},
        {field:"comments",head:"角色描述",width:"20%"},
        {field:"status",head:"状态",width:"15%",filterType:"select",dictCode:"PUB_ENABLE_DISABLE"},
        {field:"role_type",head:"角色类型",width:"35%",filterType:"select",dictCode:"SYS_ROLE_TYPE"}
    ]
  }
#end

--查询单个角色记录
#sql("findSysRoleOne")
select * from sys_role where 1=1 
#if(role_code??) and role_code=#para(role_code) #end
#if(role_name??) and role_name = #para(role_name) #end
#if(id??) and id = #para(id) #end
#end

--根据角色和组织对应关系
#sql("deleteRoleMenuByRoleId")
delete from sys_role_menu where role_id = ?
#end

#-- 根据角色ID查询 --#
#sql("findSysMenuByRoleId")
select group_concat(r.menu_name) as "menuRoleShow",group_concat(r.id) as "menuRole" from SYS_ROLE_MENU u inner join sys_menu r on u.menu_id = r.id where u.role_id = ?
#end

###根据角色状态查询角色树数据
#sql("findSysRoleForTreeByStatus")
SELECT r.id,
       r.role_name,
       r.role_type
FROM   sys_role r
WHERE  r.status = #para(status)
ORDER  BY r.id
#end

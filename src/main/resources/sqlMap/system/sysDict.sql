--查询字典所有信息
#sql("findSysDictList")
select * from sys_dict where head_flag = 'Y'
  #if(dict_code??)  AND INSTR(UPPER(dict_code),UPPER(#para(dict_code))) > 0 #end
  #if(dict_name??)  AND INSTR(UPPER(dict_name),UPPER(#para(dict_name))) > 0 #end
  #if(dict_flag??) AND dict_flag =#para(dict_flag)
  #end
  #if(dict_comments??)  AND INSTR(UPPER(dict_comments),UPPER(#para(dict_comments))) > 0 #end
#end

###查询数据字典项
#sql("findDictItemByDicCode")
SELECT t.* FROM sys_dict t
WHERE t.head_flag = 'N' AND t.dict_code = #para(dict_code)
#if(item_flag??)  
AND t.item_flag = #para(item_flag)
#end
ORDER BY item_seq
#end

###查询数据字典根据编码
#sql("findSysDictInfoByParams")
SELECT * FROM SYS_DICT T
WHERE 1=1
#if(head_flag??)  AND T.HEAD_FLAG = #para(head_flag)#end
#if(dict_code??)  AND T.DICT_CODE = #para(dict_code)#end
#if(dict_flag??)  AND T.DICT_FLAG = #para(dict_flag)#end
#if(item_flag??)  AND T.ITEM_FLAG = #para(item_flag)#end
ORDER BY item_seq
#end

###删除数据字典
#sql("deleteSysDictByDicCode")
DELETE FROM sys_dict WHERE dict_code = ?
#end





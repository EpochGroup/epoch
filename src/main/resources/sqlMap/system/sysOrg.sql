#sql("findSysOrgList")
select * from sys_org WHERE  PARENT_ID =#para(parent_id)
#end

#sql("findLeafSysOrg")
select * from sys_org  	WHERE ID = #para(id)
#end


#sql("findAllSysOrgList")
select * from sys_org
#end

#sql("findSysOrgListOne")
select id,org_code,org_name as text,PARENT_ID as pid,state,org_type,short_name,status,company_name from sys_org WHERE parent_id is null
#end


#sql("findOrgCodeCount")
select count(ID) as COUNT from sys_org where org_code =#para(org_code)
#end



#sql("findSysOrgListAjax")
select * from sys_org where PARENT_ID =#para(orgId)
#end

--根据用户ID查询人员和组织对应关系
#sql("findUserOrgByUserId")
select * from sys_user_org where  user_id = ?
#end

--根据角色查询用户
#sql("findSysUserByOrgId")
select distinct(u.id) as user_id,org.id as org_id,u.account,u.user_name,u.email as email,u.phone_number as phone_number,org.org_name as name from sys_user u 
left join sys_org org on u.dept_id = org.id left join sys_user_org o  on u.dept_id = o.org_id and u.id=o.user_id
where u.dept_id=#para(org_id)
 #if(org_id??) and INSTR(UPPER(org.id),UPPER(#para(org_id))) > 0 #end
 #if(account??) and INSTR(UPPER(u.account),UPPER(#para(account))) > 0 #end
 #if(user_name??) and INSTR(UPPER(u.user_name),UPPER(#para(user_name))) > 0 #end
 #if(email??)and INSTR(UPPER(u.email),UPPER(#para(email))) > 0 #end
 #if(phone_number??) and INSTR(UPPER(u.phone_number),UPPER(#para(phone_number))) > 0 #end
 #if(name??)and INSTR(UPPER(org.org_name),UPPER(#para(name))) > 0 #end
order by #if(sortName??)#(sortName) #else u.user_name #end #if(sortOrder?? && sortOrder=="desc")desc#end
#end

--根据用户ID删除人员和组织对应关系
#sql("deleteSysUserOrgByUserId")
delete from sys_user_org where user_id = ?
#end

#sql("findSysUserByUserId")
select  o.id as id,o.dept_id as dept_id from sys_user o
WHERE o.id=?
#end

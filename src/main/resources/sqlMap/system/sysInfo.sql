#--查询所有消息 --#
#sql("findInfoList")
select * from sys_info  where 1=1
#if(info_title??) and info_title like '%'||#para(info_title)||'%'#end
#if(info_desc??) and info_desc like '%'||#para(info_desc)||'%'#end
#if(status??) and status=#para(status)#end
#if(info_type??) and info_type=#para(info_type)#end	 
order by #if(sortName??)#(sortName) #else update_date #end #if(sortOrder?? && sortOrder=="desc") desc #end
#end

#sql("findInfoListTwo")
select * from sys_info where status = 'ENABLE'  order by UPDATE_DATE desc limit 0,2
#end

#sql("findInfoListCount")
select count(*) from sys_info where status = 'ENABLE'
#end

#sql("deleteInfo")
DELETE FROM sys_info where ID = ?
#end

#sql("findInfo")
SELECT * FROM sys_info where ID = ?
#end
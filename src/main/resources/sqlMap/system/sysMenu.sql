#sql("findSysMenList")
select * from sys_menu WHERE 1=1 
  #if(parent_id??)
  	AND PARENT_ID =#para(parent_id)
  #end
  #if(menu_code??)
  	AND MENU_CODE =#para(menu_code)
  #end
  ORDER BY MENU_ORDER
#end


#sql("findSysMenListById")
select * from sys_menu WHERE PARENT_ID =#para(parent_id) ORDER BY MENU_ORDER
#end

#sql("findSysMenuListByUserId")	
SELECT DISTINCT me.*
FROM (
SELECT DISTINCT sm.*
FROM sys_menu sm,(
SELECT *
FROM sys_menu
WHERE
id IN(
SELECT DISTINCT rm.menu_id
FROM sys_user_role ur
LEFT JOIN sys_role_menu rm ON ur.role_id = rm.role_id
WHERE ur.user_id =  #para(user_id) )) subMenu
WHERE subMenu.PARENT_ID IS NULL OR subMenu.PARENT_ID = sm.ID) me,sys_menu sm2
WHERE (me.PARENT_ID IS NULL OR me.PARENT_ID = sm2.ID) UNION ALL
SELECT *
FROM sys_menu
WHERE
id IN(
SELECT DISTINCT rm.menu_id
FROM sys_user_role ur
LEFT JOIN sys_role_menu rm ON ur.role_id = rm.role_id
WHERE ur.user_id =  #para(user_id) )
#end

#sql("findAllSysMenuList")
select * from sys_menu where 1 = 1
#if(status??)
  	AND status =#para(status)
#end
ORDER BY MENU_ORDER
#end

#-- 根据菜单ID查询 --#
#sql("findSysMenu")
SELECT group_concat(r.role_name) as "menuRoleShow",group_concat(r.id) as "menuRole"  FROM SYS_ROLE_MENU u inner join sys_role r on u.role_id = r.id WHERE u.MENU_ID = ?
#end

--根据菜单ID删除对应角色
#sql("deleteMenuRoleByMenuId")
 delete from sys_role_menu where menu_id = ?
#end


#sql("findSysMenuListAjax")
select id,menu_name as text,parent_id as pid,menu_code,comments,status,state,menu_icon,menu_url,menu_order from sys_menu WHERE PARENT_ID=#para(parent_id)
#end

#sql("findSysMenuListAjaxOne")
select id,menu_name as text,parent_id as pid,menu_code,comments,status,state,menu_icon,menu_url,menu_order from sys_menu WHERE parent_id is null
#end

#sql("findMenuCodeCount")
select count(ID) as COUNT from sys_menu where menu_code =#para(menu_code)
#end


#sql("findMenuIsLeaf")
select count(ID) as COUNT from sys_menu where PARENT_ID=#para(parent_id)
#end

#sql("findUserRoleMenuList")
select distinct(menu_id) from sys_user_role sur inner join sys_role_menu srm on sur.ROLE_ID = srm.ROLE_ID where user_id = #para(userId)
#end


#sql("findParentMenu")
select * from sys_menu where 
#if(parent_id??) 
PARENT_ID=#para(parent_id) 
#else PARENT_ID is null 
#end
#end

#sql("initMenuCode")
select * from sys_menu where  menu_code is not null
#end



//获取项目根路径
function getRootPath() {
    // 当前网址
    var curWwwPath = window.document.location.href;
    // 主机地址之后的目录
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    // 获取主机地址
    var localhostPath = curWwwPath.substring(0, pos);
    // 获取带“/”的项目名
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    
    return (localhostPath + projectName);
}

function isEmpty(s){
    if(typeof s != "undefined" && s != null && s!=''){
        return false;
    }
    return true;
}
//去首尾空白
function trim(s){
    return isEmpty(s) ? s : s.replace(/^\s+|\s+$/g,"");
}
//判断空白字符串
function isBlank(s){
    return isEmpty(trim(s));
}
//判断email合法性
function isEmail(email){
    var reg = /^[-_.a-zA-Z0-9]+\@(?:[-_a-zA-Z0-9]+\.)+[a-zA-Z0-9]{2,4}$/;
    return reg.test(email);
}
//判断是否是Excel文件
function isExcel(excel){
    var reg = /\.xlsx?$/;
    return reg.test(excel);
}

function escapeHtmlText(str){
    var result = str;
    if(!isEmpty(result)){
        result = result.replace(/&/g,"&amp;");
        result = result.replace(/</g,"&lt;");
        result = result.replace(/>/g,"&gt;");
        result = result.replace(/"/g,"&quot;");
        result = result.replace(/'/g,"&apos;");
        result = result.replace(/\r\n/g,"<br/>");
        result = result.replace(/\n/g,"<br/>");
        result = result.replace(/\r/g,"<br/>");
        result = result.replace(/ /g,"&nbsp;");
        result = result.replace(/&nbsp;((?:&nbsp;)*)/g," $1");
    }
    return result;
}

function appendTipMessage(obj,msg,timeout,fadetime){
    $("#" + obj).append(msg).show();
    if(timeout == null){
        timeout = 0;
    }
    if(fadetime == null){
        fadetime = 400;
    }
    if(timeout > 0){
        setTimeout("closeAlert('#" + obj + "'," + fadetime + ")",timeout);
    }
}
function appendTipMsgRow(obj,msg,timeout,fadetime){
    appendTipMessage(obj,msg + "<br/>",timeout,fadetime);
}

function appendTipMsgRows(obj,msgs,timeout,fadetime){
    if(!isEmpty(msgs)){
        appendTipMessage(obj,($.isArray(msgs) ? msgs : [msgs]).join("<br/>"),timeout,fadetime);
    }
}
function clearTipMsgs(obj){
    var objs = $.isArray(obj) ? obj : [obj];
    $.each(objs,function(i,o){
        var $o = null;
        if(typeof o == "string"){
            $o = $("#" + o);
        }else{
            $o = $(o);
        }
        $o.empty().hide();
    });
}

function getUUID() {
    return 'xxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

function clearMessage(){
	var infodivs=$(".alert-info");
	var alertdivs=$(".alert-success");
	var errordivs=$(".alert-danger");
	
	infodivs.each(
			function(i,el){
				var div=$(this);
				div.hide();
			}	
	);
	
	alertdivs.each(
			function(i,el){
				var div=$(this);
				div.hide();
			}	
	);
	
	errordivs.each(
			function(i,el){
				var div=$(this);
				div.hide();
			}	
	);
}

function showToDoMessage(obj,msg,timeout,fadetime){
	clearMessage();
	if(typeof msg=='undefined'||null==msg||""==msg){
		msg="处理中请稍后......";		
	}
	msg="&nbsp;&nbsp;&nbsp;"+msg;
	$("#"+obj).empty().html(msg).show();
    if(timeout == null){
        timeout = 0;
    }
    if(fadetime == null){
        fadetime = 400;
    }
    if(timeout > 0){
        setTimeout("closeAlert('#" + obj + "'," + fadetime + ")",timeout);
    }
}

function showAlertMessage(obj,msg,timeout,fadetime){
	clearMessage();
	if(typeof msg=='undefined'||null==msg||""==msg){
		msg="处理成功";		
	}
	
	$("#"+obj).empty().html(msg).show();	
    if(timeout == null){
        timeout = 2000;
    }
    if(fadetime == null){
        fadetime = 400;
    }
    if(timeout > 0){
        setTimeout("closeAlert('#" + obj + "'," + fadetime + ")",timeout);
    }
}

function showErrorMessage(obj,msg,timeout,fadetime){
	clearMessage();
	if(typeof msg=='undefined'||null==msg||""==msg){
		msg="数据处理出错...";		
	}
	$("#"+obj).empty().html(msg).show();	
    if(timeout == null){
        timeout = 0;
    }
    if(fadetime == null){
        fadetime = 400;
    }
    if(timeout > 0){
        setTimeout("closeAlert('#" + obj + "'," + fadetime + ")",timeout);
    }
}

function closeAlert(obj,fadetime){
	var div=$(obj);
	div.hide(fadetime);
}

function setSubmitStatu(flag){	
	var buttons=$(":button");	
	if(flag){
		buttons.each(
				function(i,el){
					var input=$(this);
					input.attr('disabled',"disabled");					
				}		
		);
		
	}else{
		buttons.each(
				function(i,el){
					var input=$(this);
					input.removeAttr('disabled');					
				}		
		);
	}			
}

var CommonUtil = {
		delBtn: function (headToolbarId, tableId, keyId, url, callback, preCallBack, cancelFunc, _beforePost, _error) {
            if (isEmpty(headToolbarId)) {
                //为空加入默认值
                headToolbarId = "_common_head_bar";
            }
            headToolbarId = "#" + headToolbarId + "_Del";
            $(headToolbarId).show();
            $(headToolbarId).unbind();
            $(headToolbarId).click(function () {
                var rows = $("#" + tableId).bootstrapTable("getAllSelections");
                var isFlag = true;//是否能往下走
                if (!isEmpty(preCallBack)) {
                    isFlag = preCallBack(rows);//不能使用ajax请求
                }
                if (isFlag) {
                    _common.del(rows, keyId, url, null, function (data) {
                        setSubmitStatu(false);
                        callback(data);
                    }, cancelFunc, _beforePost, _error);
                }
            });
        },
		del: function (rows, keyId, url, msg, _function, cancelFunc, _beforePost, _error) {
	        var ids = [];
	        if (rows instanceof Array) {
	            if (isEmpty(rows)) {
	                MessageBox.error("请至少选择一行记录！");
	                return;
	            }
	            if (isEmpty(keyId)) {
	                MessageBox.error(_i18ns("cmn_msg_err_needIdForCommonDel"));
	                return;
	            }
	            for (var id in rows) {
	                ids.push(rows[id][keyId]);
	            }
	        } else {
	            ids.push(keyId);
	        }
	        MessageBox.confirm("确认删除选中数据？", function () {
	            clearMessage();
	            if(!isEmpty(_beforePost)){
	                var result = _beforePost();
	                if(result == false){
	                    return;
	                }
	            }
	            $.ajax({
	                url:url,
	                type:"POST",
	                data:{ids:ids},
	                success:function (data) {
	                    if (!isEmpty(_function)) {
	                        _function(data);
	                    } else {
	                        if (data.code == 1) {//1表示失败  0表示成功
	                            if (isEmpty(msg)) {
	                                msg = "删除失败！";
	                            }
	                            MessageBox.error(msg)
	                        }
	                    }
	                },
	                error:function(xhr,status,err){
	                    console.warn("ajax response error: {status:'" + status + "',exception:'" + err + "'}");
	                    if(!isEmpty(_error)){
	                        _error(xhr,status,err);
	                    }
	                }
	            });
	        }, cancelFunc);
	    }
};
/*
 * MessageBox V1.0.0
 * Copyright 2017 SM.
 * // 标记成功失败，默认0：成功，1：失败、用于alert，2：失败、用于confirm
 */
function MessageBox(option) {
    option = option || {};
    var setting = {
        id: 'messageBox',
        msgType: 'error',
        ok: "确定",
        yes: "确定",
        no: "取消",
        close: "关闭",
        style: {
            'success': 'text-success',
            'confirm': 'text-warning',
            'error': 'text-danger'
        },
        successCall: function () {
        },
        cancelCall: function () {
        }
    };
    this.option = $.extend({}, setting, option);
    return this.show();
}
MessageBox.prototype.show = function () {
    var option = this.option;
    var _this = this;
    var html = '';
    html += '<div id=' + option.id + ' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Label" aria-hidden="true">';
    html +=   '<div class="modal-dialog modal-sm" role="document">';
    html +=     '<div class="modal-content">';
    html +=       '<div class="modal-header">';
    html +=         '<button type="button" class="close" data-dismiss="modal" aria-label="' + option.close + '"><span aria-hidden="true"></span></button>';
    html +=         '<h4 class="modal-title ' + option.style[option.msgType] + '">' + option.title + '</h4>';
    html +=       '</div>';
    html +=       '<div class="modal-body" style="padding:10px 10px 10px 10px;">';
    html +=         '<p class="' + option.style[option.msgType] + '">' + option.msg + '</p>';
    html +=       '</div>';
    html +=       '<div class="modal-footer center">';
    if (option.msgType == 'confirm') {
        html +=     '<button id="' + option.id + '_confirm_yes" type="button" class="btn btn-success btn-sm">' + option.yes + '</button>';
        html +=     '<button id="' + option.id + '_confirm_cancel" type="button" class="btn btn-success btn-sm" data-dismiss="modal">' + option.no + '</button>';
    } else {
        html +=     '<button type="button" class="btn btn-success btn-sm" data-dismiss="modal">' + option.ok + '</button>';
    }
    html +=       '</div>';
    html +=     '</div>';
    html +=   '</div>';
    html += '</div>';
    $('body').append(html);
    $("#" + option.id + "_confirm_yes").click(function (e) {
        option.successCall();
        $('#' + option.id).modal('hide');
        $('#' + option.id).modal('hideModal');
    });
    $("#" + option.id + "_confirm_cancel").click(function (e) {
        option.cancelCall();
        $('#' + option.id).modal('hide');
        $('#' + option.id).modal('hideModal');
    });
    $('#' + option.id).on('hidden.bs.modal', function (e) {
        //option.cancelCall();
        _this.hide();
    })
    $('#' + option.id).modal('show');
};
MessageBox.prototype.hide = function () {
    if ($("#" + this.option.id)) {
        $("#" + this.option.id).remove();
    }
};


MessageBox.error = function (msg, successCall) {
    new MessageBox({
    	id: (new Date()).getTime() + "",
        msg: msg,
        msgType: 'error',
        title: "错误",
        successCall: (typeof successCall == 'function') ? successCall : undefined
    });
};
MessageBox.success = function (msg, successCall) {
    new MessageBox({
    	id: (new Date()).getTime() + "",
        msg: msg,
        title: "信息",
        msgType: 'success',
        successCall: (typeof successCall == 'function') ? successCall : undefined
    });
};
MessageBox.confirm = function (msg, successCall, cancelCall) {
    new MessageBox({
    	id: (new Date()).getTime() + "",
        msg: msg,
        msgType: 'confirm',
        title: "确认信息",
        successCall: (typeof successCall == 'function') ? successCall : undefined,
        cancelCall: (typeof cancelCall == 'function') ? cancelCall : undefined
    });
};

MessageBox.msgBox = function (data, successCall) {
    var msgType = data.code == "0" ? "success" : (data.code == "1" ? "error" : "confirm");
    var title = msgType == 'confirm' ? "确认信息" : (msgType == 'success' ? _i18ns("cmn_label_info") : "错误");
    new MessageBox({
    	id: (new Date()).getTime() + "",
        msg: data.message,
        msgType: msgType,
        title: title,
        successCall: (typeof successCall == 'function') ? successCall : undefined
    });
};

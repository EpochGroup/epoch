
@var id = tag.id;
@var title = tag.title;
@var body = tag.body;//内容
@var showSaveBtn = tag.showSaveBtn!"true";
@var showCloseBtn = tag.showCloseBtn!"true";
@var showClearBtn = tag.showClearBtn!"false";
@var btnClose = tag.btnClose!"关闭";
@var btnSave = tag.btnSave!"保存";
@var btnClear = tag.btnClear!"清空";
@var backdrop = tag.backdrop!"false";
@var width = tag.width!"auto";
@var height = tag.height!"auto";
<div class="modal fade" id="${id}" tabindex="-1"
     @if(backdrop!="true"){
        data-backdrop="static"
     @}
     role="dialog" aria-labelledby="${id}Label" aria-hidden="true">
    <div id="${id}_dialog" class="modal-dialog" style="width: ${width};">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="${id}Label">
                    ${title}
                </h4>
            </div>
            <div class="modal-body" id="${id}_body" style="height:${height};">
                ${body}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" id="${id}_clear"${showClearBtn == 'true' ? '' : ' style="display:none;"'}>
                    <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>${lisFun.escapeHtml(btnClear)}
                </button>
                <button type="button" class="btn btn-primary btn-sm" id="${id}_save"${showSaveBtn == 'true' ? '' : ' style="display:none;"'}>
                    <span class="glyphicon glyphicon-save" aria-hidden="true"></span>${lisFun.escapeHtml(btnSave)}
                </button>
                <button type="button" class="btn btn-primary btn-sm" id="${id}_close"${showCloseBtn == 'true' ? '' : ' style="display:none;"'}>
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>${lisFun.escapeHtml(btnClose)}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(function () {
        $("#${id}_clear").on("click", function() {
            $("#${id}_selections").val("");
            window.frames["${id}_frame"].$("#_commonPopupTable_select_ids").val("")
            //$("#${id}_selectRowJson").val();
            //window.frames["${id}_frame"].$("#_commonPopupTable_select_rows_json").val("")
            $('#${id}').modal('hide');
        });
        $("#${id}_close").on("click", function() {
            $('#${id}').modal('hide');
        });
        $('#${id}').modal('hide');
        
        
        $('.modal-content').draggable({   
            handle: ".modal-header",   
            cursor: 'move',   
            refreshPositions: false  
        });  
    });
    
</script>

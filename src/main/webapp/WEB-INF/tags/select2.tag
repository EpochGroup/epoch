@var id = tag.id!"common_select2";
@var name = tag.name!"common_select2";
@var multiple = tag.multiple!"false";
@var type = tag.type!'local';
@var url = tag.url!'';
@var width = tag.width!'100%';
@var disabled = tag.disabled!'false';
@var placeholder = tag.placeholder!'-请选择-';
@var data = tag.data!'';
@var value = tag.value!'';
<select id="${id}" name="${name}" class="form-control form-control-sm"></select>
<script>
    // @if(type == "local"){
    //是否启用占用字符
    var select2${id} = $("#${id}").select2({
        data: ${data},
        multiple: ${multiple},
        width: '${width}',
        placeholder: '${placeholder}'
    });
    var $value${id} = "${value}";
    if(!isEmpty($value${id})){
        $value${id} = $value${id}.split(",");
        select2${id}.val($value${id}).trigger("change");

    }
    // @}else{
    //使用远程调用
    $.ajax({
        type:"post",
        url:'${url}',
        dataType:"json",
        contentType:"application/json",
        success:function(data){
            var select2${id} = $("#${id}").select2({
                data: data,
                multiple: ${multiple},
                width:  '${width}',
                placeholder: '${placeholder}'
            });
            var $value${id} = "${value}";
            if(!isEmpty($value${id})){

                $value${id} = $value${id}.split(",");
                select2${id}.val($value${id}).trigger("change");
            }
        },
        error:function(data){
            console.error("数据加载失败");
        }
    });

    // @}
</script>
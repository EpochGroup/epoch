@var id = tag.id;
@var title = tag.title;
@var sqlKey = tag.sqlKey;
@var callback = tag.callback!"null";
@var filter = tag.filter;
@var multiple = tag.multiple!"false";
@var backdrop = tag.backdrop!"false";
@var paginate = tag.paginate!"true";
@var width = tag.width!"1000px";
@var height = tag.height!"580px";
@var hideBtn = tag.hideBtn!"false";
@var btnClickFunc = tag.btnClickFunc!"undefined";
@var selections = tag.selections!"undefined";
@var selectRowJson = tag.selectRowJson!"undefined";
@var showBack = tag.showBack!"false";
@var autoInit = tag.autoInit!"true";
<span id="${id}_popBtn" class="btn visa-btn-icon visa-btn-select fa fa-search fa-1" ></span>
<input id="${id}_popPath" type="hidden" value="${basePath}/sys/common/popup/init" />
<input id="${id}_opt_title" type="hidden" value="${lisFun.escapeForQuot(title)}" />
<input id="${id}_opt_sqlKey" type="hidden" value="${sqlKey}" />
<input id="${id}_opt_callback" type="hidden" />
<input id="${id}_opt_filter" type="hidden" />
<input id="${id}_opt_multiple" type="hidden" value="${multiple}" />
<input id="${id}_opt_backdrop" type="hidden" value="${backdrop}" />
<input id="${id}_opt_paginate" type="hidden" value="${paginate}" />
<input id="${id}_opt_width" type="hidden" value="${width}" />
<input id="${id}_opt_height" type="hidden" value="${height}" />
<input id="${id}_selections" type="hidden" />
<input id="${id}_selectRowJson" type="hidden" />
<input id="${id}_showBack" type="hidden" value="${showBack}" />
<input id="${id}_autoInit" type="hidden" value="${autoInit}" />
<epoch:winOpen id="${id}" title="${title}" backdrop="${backdrop}" btnSave="确定" btnClose="取消"  showClearBtn="true" btnClear="清空">
    <iframe id="${id}_frame" name="${id}_frame" scrolling="no" style="box-sizing:border-box;border-width:0;width:100%;"></iframe>
</epoch:winOpen>
<script type="text/javascript">
$(function(){
    var callbackStr = "${lisFun.escapeForQuot(callback)}";
    var filterStr = "${lisFun.escapeForQuot(filter)}";
    var selections = "${lisFun.escapeForQuot(selections)}";
    var selectRowJson = "${lisFun.escapeForQuot(selectRowJson)}";
    $("#${id}_opt_callback").val(callbackStr);
    $("#${id}_opt_filter").val(filterStr);
    $("#${id}_selections").val(selections);
    $("#${id}_selectRowJson").val(selectRowJson);
    var btnClickFunc = window.${btnClickFunc};
    if (!btnClickFunc) {
        btnClickFunc = function(event){
            CommonPopup.show("${id}");
        };
    }
    $("#${id}_popBtn").on("click", btnClickFunc);
    if("${hideBtn}" == "true"){
        $("#${id}_popBtn").css("display","none");
    }
});
</script>

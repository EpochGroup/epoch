@var id = tag.id;
@var url = tag.url;
@var auto = tag.auto!"false";
@var dragDrop = tag.dragDrop!"false";
@var numLimit = tag.numLimit;
@var sizeLimit = tag.sizeLimit;
@var totalSizeLimit = tag.totalSizeLimit;
@var accept = tag.accept;
@var filter = tag.filter;
@var readonly = tag.readonly!"false";
@var allowEmptyFile = tag.allowEmptyFile!"true";
@var simpleMode = tag.simpleMode!"false";
@var simpleModeAlign = tag.simpleModeAlign!"hor";
@var showChooseFile = readonly == "true" ? "false" : (tag.showChooseFile!"true");
@var showUpload = readonly == "true" ? "false" : (tag.showUpload!(auto == "true" ? "false" : "true"));
@var showFileSize = tag.showFileSize!"false";
@var showStatus = tag.showStatus!"false";
@var showUploadTime = tag.showUploadTime!"false";
@var showDescn = tag.showDescn!"false";
@var showProgress = tag.showProgress!"false";
@var showOperation = tag.showOperation!"true";
@var showRemove = readonly == "true" ? "false" : (tag.showRemove!"true");
@var showRetry = readonly == "true" ? "false" : (tag.showRetry!"true");
@var showDownload = tag.showDownload!"false";
@var descnAllowModify = readonly == "true" ? "false" : (tag.descnAllowModify!"true");
@var headerDisplay = tag.headerDisplay!"auto";
@var fileSizeUnit = tag.fileSizeUnit!"'ALL'";
@var styleClass = tag.styleClass;
@var inlineStyle = tag.inlineStyle;
@var fileNameHeadText = tag.fileNameHeadText!"文件名称";
@var fileSizeHeadText = tag.fileSizeHeadText!"文件大小";
@var fileStatusHeadText = tag.fileStatusHeadText!"文件状态";
@var uploadTimeHeadText = tag.uploadTimeHeadText!"上传时间";
@var uploadTimeLocalText = tag.uploadTimeLocalText!"本地";
@var descnHeadText = tag.descnHeadText!"描述";
@var uploadProgressHeadText = tag.uploadProgressHeadText!"上传进度";
@var optionHeadText = tag.optionHeadText!"操作";
@var chooseFileBtnText = tag.chooseFileBtnText!"选择文件";
@var uploadBtnText = tag.uploadBtnText!"上传";
@var removeBtnText = tag.removeBtnText!"移除";
@var retryBtnText = tag.retryBtnText!"重试";
@var downloadBtnText = tag.downloadBtnText!"下载";
@var waitUploadText = tag.waitUploadText!"等待上传";
@var uploadingText = tag.uploadingText!"上传中";
@var uploadFailedText = tag.uploadFailedText!"上传失败";
@var uploadSuccessText = tag.uploadSuccessText!"上传成功";
@var archiveText = tag.archiveText!"已存档";
@var customUploader = tag.customUploader != null ? tag.customUploader : "null";
@var onBeforeFileQueued = tag.onBeforeFileQueued!"undefined";
@var onFileQueued = tag.onFileQueued!"undefined";
@var onFilesQueued = tag.onFilesQueued!"undefined";
@var onFileDequeued = tag.onFileDequeued!"undefined";
@var onUploaderReset = tag.onUploaderReset!"undefined";
@var onUploadStart = tag.onUploadStart!"undefined";
@var onUploadStop = tag.onUploadStop!"undefined";
@var onUploadFinished = tag.onUploadFinished!"undefined";
@var onFileStart = tag.onFileStart!"undefined";
@var onDataBeforeSend = tag.onDataBeforeSend!"undefined";
@var onServerAccept = tag.onServerAccept!"undefined";
@var onFileProgress = tag.onFileProgress!"undefined";
@var onFileError = tag.onFileError!"undefined";
@var onFileSuccess = tag.onFileSuccess!"undefined";
@var onFileComplete = tag.onFileComplete!"undefined";
@var onValidateFaild = tag.onValidateFaild!"undefined";
@var onBeforeFileRemove = tag.onBeforeFileRemove!"undefined";
@var onFileRemove = tag.onFileRemove!"undefined";
@var alterMoreBtns = tag.alterMoreBtns;
@var children = tag.children;
@var childrenMap = tag.childrenMap;
@var moreBtns = alterMoreBtns!childrenMap['moreBtn'];
<div id="${id}"${styleClass != null ? " class='" + styleClass + "'" : ""}${inlineStyle != null ? " style='" + inlineStyle + "'" : ""}>
    <div id="${id}_fileList" class="overxauto"><table id="${id}_fileListTbl" class="uploader-fileList-table">
        <thead id="${id}_fileListThead"></thead>
        <tbody id="${id}_fileListTbody"></tbody>
    </table><div id="${id}_simpleModeBox" class="simple-mode-box" style="display:none;"></div></div>
    <div id="${id}_btnBar" class="uploader-btnBar">
        <div id="${id}_pickerHide" style="display:none;"></div>
        @var moreBtnGrp = null;
        @var moreBtnMap = lisFun.buildMoreBtnMap(moreBtns,2);
        @moreBtnGrp = moreBtnMap["0"];
        @if(moreBtnGrp != null){
            @for(moreBtn in moreBtnGrp){
                ${moreBtn.body}
            @}
        @}
        <button id="${id}_pickBtn" type="button" class="btn btn-primary"${"true" == showChooseFile ? "" : " style='display:none;'"}>${chooseFileBtnText}</button>
        @moreBtnGrp = moreBtnMap["1"];
        @if(moreBtnGrp != null){
            @for(moreBtn in moreBtnGrp){
                ${moreBtn.body}
            @}
        @}
        <button id="${id}_uploadBtn" type="button" class="btn btn-primary"${"true" == showUpload ? "" : " style='display:none;'"}>${uploadBtnText}</button>
        @moreBtnGrp = moreBtnMap["OTH"];
        @if(moreBtnGrp != null){
            @for(moreBtn in moreBtnGrp){
                ${moreBtn.body}
            @}
        @}
    </div>
    <div id="${id}_debugDiv" style="display:none;"><textarea id="${id}_debugArea" rows="8" style="width:100%;"></textarea></div>
    <script type="text/javascript">
    var _var_${id}_uploader = (function(){
        var debugFlag = false;
        var auto = "${auto}" == "true";
        var dragDrop = "${dragDrop}" == "true";
        var fileNumLimit = "${numLimit}";
        var fileSingleSizeLimit = "${sizeLimit}";
        var fileSizeLimit = "${totalSizeLimit}";
        var accept = "${accept}";
        var filter = "${filter}";
        var allowEmptyFile = "${allowEmptyFile}" == "true";
        var readonly = "${readonly}" == "true";
        var simpleMode = "${simpleMode}" == "true";
        var simpleModeAlign = "${simpleModeAlign}";
        var showUpload = "${showUpload}" == "true";
        var showFileSize = "${showFileSize}" == "true";
        var showStatus = "${showStatus}" == "true";
        var showUploadTime = "${showUploadTime}" == "true";
        var showDescn = "${showDescn}" == "true";
        var showProgress = "${showProgress}" == "true";
        var showOperation = "${showOperation}" == "true";
        var showRemove = "${showRemove}" == "true";
        var showRetry = "${showRetry}" == "true";
        var showDownload = "${showDownload}" == "true";
        var descnAllowModify = "${descnAllowModify}" == "true";
        var headerDisplay = "${headerDisplay}";
        var fileSizeUnit = ${fileSizeUnit};
        var allFileSizeUnit = ["KB","MB","GB","TB"];
        var calcFileSize = function(size,unitArray){
            var result = size + " B";
            if(unitArray != null && $.isArray(unitArray)){
                var SIZE_K = 1024;
                var SIZE_M = SIZE_K * 1024;
                var SIZE_G = SIZE_M * 1024;
                var SIZE_T = SIZE_G * 1024;
                if(size >= SIZE_T && $.inArray("TB",unitArray) >= 0){
                    result = String(Math.round(size * 100 / SIZE_T) / 100) + " TB";
                }else if(size >= SIZE_G && $.inArray("GB",unitArray) >= 0){
                    result = String(Math.round(size * 100 / SIZE_G) / 100) + " GB";
                }else if(size >= SIZE_M && $.inArray("MB",unitArray) >= 0){
                    result = String(Math.round(size * 100 / SIZE_M) / 100) + " MB";
                }else if(size >= SIZE_K && $.inArray("KB",unitArray) >= 0){
                    result = String(Math.round(size * 100 / SIZE_K) / 100) + " KB";
                }
            }
            return result;
        };
        var appendFileDom = function(file){
            var fileDom = '';
            if(readonly && simpleMode){
                $("#${id}_fileListTbl").hide();
                fileDom = '<span id="${id}_file_' + file.id + '" class="' + simpleModeAlign + '-align">' + file.name + '</span>';
                var tips = file.name || "";
                if(showFileSize){
                    var size = file.size;
                    if(fileSizeUnit == "ALL"){
                        fileSizeUnit = allFileSizeUnit;
                    }
                    if($.isArray(fileSizeUnit)){
                        size = calcFileSize(size,fileSizeUnit);
                    }
                    tips += "\n${fileSizeHeadText} : " + (size == null ? "" : size);
                }
                if(showUploadTime){
                    tips += "\n${uploadTimeHeadText} : " + (file.uploadTime == null ? "" : file.uploadTime);
                }
                if(showDescn){
                    tips += "\n${descnHeadText} : " + (file.descn == null ? "" : file.descn);
                }
                $("#${id}_simpleModeBox").append(fileDom);
                var fileElmt = $("#${id}_file_" + file.id);
                fileElmt.attr("title",tips);
                if(showOperation && showDownload && file.hasOwnProperty("downloadUri")){
                    fileElmt.text("");
                    var downLink = $('<a id="${id}_download_' + file.id + '" target="_blank">' + file.name + '</a>');
                    downLink.attr("href","${basePath}" + file.downloadUri);
                    fileElmt.append(downLink);
                }
                $("#${id}_simpleModeBox").show();
            }else{
                fileDom = '<tr id="${id}_file_' + file.id + '">';
                fileDom += '<td class="uploadFileName">' + file.name + '</td>';
                if(showFileSize){
                    var size = file.size;
                    if(fileSizeUnit == "ALL"){
                        fileSizeUnit = allFileSizeUnit;
                    }
                    if($.isArray(fileSizeUnit)){
                        size = calcFileSize(size,fileSizeUnit);
                    }
                    fileDom += '<td id="${id}_fileSize_"' + file.id + '" class="uploadFileSize">' + size + '</td>'
                }
                if(showStatus){
                    fileDom += '<td id="${id}_upSts_' + file.id + '" class="uploadStatus">${waitUploadText}</td>';
                }
                if(showUploadTime){
                    fileDom += '<td id="${id}_uploadTime_' + file.id + '" class="uploadTime"></td>';
                }
                if(showDescn){
                    fileDom += '<td id="${id}_descn_' + file.id + '" class="uploadDescn">'
                        + '<input id="${id}_descnBox_' + file.id + '" type="text" class="form-control form-control-sm descnBox"/>'
                    + '</td>';
                }
                if(showProgress){
                    fileDom += '<td id="${id}_progress_' + file.id + '" class="uploadProgress">'
                        + '<div class="progress progress-striped active" style="margin-bottom:0;position:relative;">'
                            + '<div class="progress-text" style="text-align:center;position:absolute;top:0;left:0;width:100%;z-index:100;"></div>'
                            + '<div class="progress-bar center" style="position:absolute;top:0;left:0;width:0%;"></div>'
                        + '</div>'
                    + '</td>';
                }
                if(showOperation){
                    fileDom += '<td id="${id}_options_' + file.id + '" class="uploadOptions">';
                    if(showRemove){
                        fileDom += '<a id="${id}_remove_' + file.id + '" class="btn btn-visa btn-visa-md uploadOption" href="javascript:;">${removeBtnText}</a>';
                    }
                    if(showRetry){
                        fileDom += '<a id="${id}_retry_' + file.id + '" class="btn btn-visa btn-visa-md uploadOption" href="javascript:;">${retryBtnText}</a>';
                    }
                    if(showDownload){
                        fileDom += '<a id="${id}_download_' + file.id + '" class="btn btn-visa btn-visa-md uploadOption" href="javascript:;" target="_blank">${downloadBtnText}</a>';
                    }
                    fileDom += '</td>';
                }
                fileDom += '</tr>';
                $("#${id}_fileListTbody").append(fileDom);
                if(showOperation){
                    if(showRetry){
                        $("#${id}_retry_" + file.id).hide();
                    }
                    if(showDownload){
                        $("#${id}_download_" + file.id).hide();
                    }
                }
            }
        };
        var delArrayItem = function(array,item,cmpFunc){
            var result = null;
            if(array != null){
                var len = array.length;
                var i = 0;
                for(;i < len;i++){
                    var found = false;
                    if(cmpFunc){
                        found = cmpFunc(array[i],item);
                    }else{
                        found = array[i] == item;
                    }
                    if(found){
                        var dels = array.splice(i,1);
                        result = dels[0];
                        break;
                    }
                }
            }
            return result;
        };
        var initData = function(){
            return {
                newFiles:[],
                oldFiles:[],
                delFiles:[]
            };
        };
        var updateThead = function(){
            var thead = $("#${id}_fileListThead");
            if(headerDisplay == "show"){
                thead.show();
            }else if(headerDisplay == "hide"){
                thead.hide();
            }else{
                if($("#${id}_fileListTbody>tr").length > 0){
                    thead.show();
                }else{
                    thead.hide();
                }
            }
        };
        var initThead = function(){
            var thead = $("#${id}_fileListThead");
            var headDom = '<tr>';
            headDom +='<th class="uploadFileName">${fileNameHeadText}</th>';
            if(showFileSize){
                headDom +='<th class="uploadFileSize">${fileSizeHeadText}</th>';
            }
            if(showStatus){
                headDom +='<th class="uploadStatus">${fileStatusHeadText}</th>';
            }
            if(showUploadTime){
                headDom +='<th class="uploadTime">${uploadTimeHeadText}</th>';
            }
            if(showDescn){
                headDom +='<th class="uploadDescn">${descnHeadText}</th>';
            }
            if(showProgress){
                headDom +='<th class="uploadProgress">${uploadProgressHeadText}</th>';
            }
            if(showOperation){
                headDom +='<th class="uploadOptions">${optionHeadText}</th>';
            }
            headDom += '</tr>';
            thead.empty();
            thead.append(headDom);
            updateThead();
        };
        var lpad = function(str,padStr,totalLen){
            var result = String(str);
            var padStrLen = padStr.length;
            var len = totalLen - result.length;
            var i = 0;
            for(;i < len;i += padStrLen){
                result = padStr + result;
            }
            var newLen = result.length;
            if(newLen > totalLen){
                result = result.substring(newLen - totalLen);
            }
            return result;
        };
        var getCurTimeStr = function(){
            var now = new Date();
            var year = lpad(String(now.getFullYear()),"0",4);
            var month = lpad(String(now.getMonth() + 1),"0",2);
            var date = lpad(String(now.getDate()),"0",2);
            var hour = lpad(String(now.getHours()),"0",2);
            var minute = lpad(String(now.getMinutes()),"0",2);
            var second = lpad(String(now.getSeconds()),"0",2);
            var result = year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
            return result;
        };
        var fillDescn = function(files){
            var len = files.length;
            var i = 0;
            for(;i < len;i++){
                var file = files[i];
                var descn = $("#${id}_descnBox_" + file.id).val();
                file.descn = descn;
            }
        };
        // event handler
        var onBeforeFileQueued = ${onBeforeFileQueued};
        var onFileQueued = ${onFileQueued};
        var onFilesQueued = ${onFilesQueued};
        var onFileDequeued = ${onFileDequeued};
        var onUploaderReset = ${onUploaderReset};
        var onUploadStart = ${onUploadStart};
        var onUploadStop = ${onUploadStop};
        var onUploadFinished = ${onUploadFinished};
        var onFileStart = ${onFileStart};
        var onDataBeforeSend = ${onDataBeforeSend};
        var onServerAccept = ${onServerAccept};
        var onFileProgress = ${onFileProgress};
        var onFileError = ${onFileError};
        var onFileSuccess = window.${onFileSuccess};
        var onFileComplete = ${onFileComplete};
        var onValidateFaild = ${onValidateFaild};
        var onBeforeFileRemove = ${onBeforeFileRemove};
        var onFileRemove = ${onFileRemove};
        var customUploader = ${customUploader};
        var _data = initData();
        initThead();
        var webUploader = null;
        if(customUploader != null){
            webUploader = customUploader;
        }else{
            var opts = {
                swf: "${baseStaticUrl}/common/flash/Uploader.swf",
                server: '${url}',
                pick: "#${id}_pickerHide",
                resize: false,
                threads: 1,
                timeout: 0
            }
            if(auto){
                opts.auto = true;
            }
            if(dragDrop){
                opts.dnd = "#${id}";
            }
            if(fileNumLimit != ""){
                opts.fileNumLimit = Number(fileNumLimit);
            }
            if(fileSingleSizeLimit != ""){
                opts.fileSingleSizeLimit = Number(fileSingleSizeLimit);
            }
            if(fileSizeLimit != ""){
                opts.fileSizeLimit = Number(fileSizeLimit);
            }
            if(accept != "" || filter != ""){
                opts.accept = {
                    title: "${chooseFileBtnText}",
                    extensions: accept,
                    mimeTypes: filter
                };
            }
            if(allowEmptyFile){
                opts.allowEmptyFile = true;
            }
            webUploader = WebUploader.create(opts);
            webUploader.on("beforeFileQueued",function(file){
                debug("beforeFileQueued: " + file.id);
                if(opts.fileNumLimit != null && _data.oldFiles.length > 0 && $("#${id}_fileListTbody>tr").length >= opts.fileNumLimit){
                    webUploader.trigger("error","Q_EXCEED_NUM_LIMIT");
                    return false;
                }
                if(onBeforeFileQueued){
                    var result = onBeforeFileQueued(file);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("fileQueued",function(file){
                debug("fileQueued: " + file.id);
                appendFileDom(file);
                updateThead();
                if(showOperation){
                    if(showRemove){
                        $("#${id}_remove_" + file.id).on("click",(function(file){
                            return function(){
                                if(onBeforeFileRemove){
                                    var result = onBeforeFileRemove(file);
                                    if(result == false){
                                        return;
                                    }
                                }
                                webUploader.removeFile(file.id,true);
                                delArrayItem(_data.newFiles,{id:file.id},function(i1,i2){
                                    return i1.id == i2.id;
                                });
                                $("#${id}_file_" + file.id).remove();
                                updateThead();
                                if(onFileRemove){
                                    onFileRemove(file);
                                }
                            };
                        })(file));
                    }
                    if(showRetry){
                        $("#${id}_retry_" + file.id).on("click",(function(file){
                            return function(){
                                webUploader.retry(file);
                                $("#${id}_retry_" + file.id).hide();
                            };
                        })(file));
                    }
                }
                if(onFileQueued){
                    var result = onFileQueued(file);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("filesQueued",function(files){
                debug("filesQueued: " + files.length);
                updateThead();
                if(onFilesQueued){
                    var result = onFilesQueued(files);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("fileDequeued",function(file){
                debug("fileDequeued: " + file.id);
                updateThead();
                if(onFileDequeued){
                    var result = onFileDequeued(file);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("reset",function(){
                debug("reset...");
                updateThead();
                if(showUpload){
                    $("#${id}_uploadBtn").removeAttr("disabled");
                }
                if(onUploaderReset){
                    var result = onUploaderReset();
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("startUpload",function(){
                debug("startUpload...");
                if(showUpload){
                    $("#${id}_uploadBtn").prop("disabled",true);
                }
                if(onUploadStart){
                    var result = onUploadStart();
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("stopUpload",function(){
                debug("stopUpload...");
                if(showUpload){
                    $("#${id}_uploadBtn").removeAttr("disabled");
                }
                if(onUploadStop){
                    var result = onUploadStop();
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("uploadFinished",function(){
                debug("uploadFinished...");
                if(showUpload){
                    $("#${id}_uploadBtn").removeAttr("disabled");
                }
                if(onUploadFinished){
                    var result = onUploadFinished();
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("uploadStart",function(file){
                debug("uploadStart: " + file.id);
                if(onFileStart){
                    var result = onFileStart(file);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("uploadBeforeSend",function(object,data,headers){
                debug("uploadBeforeSend: object: " + object + ", data: " + data + ", headers: " + headers);
                if(onDataBeforeSend){
                    var result = onDataBeforeSend(object,data,headers);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("uploadAccept",function(object,ret){
                debug("uploadAccept: object: " + object + ", ret: " + ret);
                if(onServerAccept){
                    var result = onServerAccept(object,ret);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("uploadProgress",function(file,percentage){
                if(showStatus){
                    $("#${id}_upSts_" + file.id).text("${uploadingText}");
                }
                if(showProgress){
                    var progressCell = $("#${id}_progress_" + file.id);
                    var percentStr = Math.round(percentage * 100) + "%";
                    progressCell.find(".progress-bar").css("width",percentStr);
                    progressCell.find(".progress-text").text(percentStr);
                }
                if(onFileProgress){
                    var result = onFileProgress(file,percentage);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("uploadError",function(file,reason){
                debug("uploadError: " + file.id + ", reason: " + reason);
                if(showStatus){
                    $("#${id}_upSts_" + file.id).text("${uploadFailedText}");
                }
                if(showProgress){
                    $("#${id}_progress_" + file.id).find(".progress-bar").addClass("progress-bar-danger");
                }
                if(showOperation){
                    if(showRetry){
                        $("#${id}_retry_" + file.id).show();
                    }
                }
                if(onFileError){
                    var result = onFileError(file,reason);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("uploadSuccess",function(file,response){
                debug("uploadSuccess: " + file.id + ", response: " + response);
                var fileData = {
                    id:file.id,
                    name:file.name,
                    size:file.size
                };
                $.extend(fileData,response);
                _data.newFiles.push(fileData);
                if(showStatus){
                    $("#${id}_upSts_" + file.id).text("${uploadSuccessText}");
                }
                if(showUploadTime){
                    var uploadTime = "";
                    if(response != null && response.hasOwnProperty("uploadTime")){
                        uploadTime = response.uploadTime;
                    }else{
                        uploadTime = "(${uploadTimeLocalText}) " + getCurTimeStr();
                    }
                    $("#${id}_uploadTime_" + file.id).text(uploadTime);
                }
                if(showOperation){
                    if(showRetry){
                        $("#${id}_retry_" + file.id).hide();
                    }
                    if(showDownload){
                        if(response != null && response.hasOwnProperty("downloadUri")){
                            var $download = $("#${id}_download_" + file.id);
                            $download.attr("href","${basePath}" + response.downloadUri);
                            $download.show();
                        }
                    }
                }
                if(onFileSuccess){
                    var result = onFileSuccess(file,response);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("uploadComplete",function(file){
                debug("uploadComplete: " + file.id);
                if(showProgress){
                    $("#${id}_progress_" + file.id).find(".progress").removeClass("active");
                }
                if(onFileComplete){
                    var result = onFileComplete(file);
                    if(result == false){
                        return result;
                    }
                }
            });
            webUploader.on("error",function(type){
                debug("error: " + type);
                if(onValidateFaild){
                    var result = onValidateFaild(type);
                    if(result == false){
                        return result;
                    }
                }
                var errMsg = "";
                switch(type){
                case "F_DUPLICATE":
                    MessageBox.error("文件已存在！");
                    break;
                case "F_EXCEED_SIZE":
                    errMsg = "文件大小超出限制！[最大{0}字节]";
                    errMsg = errMsg.replace("{0}",fileSingleSizeLimit);
                    MessageBox.error(errMsg);
                    break;
                case "Q_EXCEED_SIZE_LIMIT":
                    errMsg = "文件总大小超出限制！[最大{0}字节]";
                    errMsg = errMsg.replace("{0}",fileSizeLimit);
                    MessageBox.error(errMsg);
                    break;
                case "Q_EXCEED_NUM_LIMIT":
                    errMsg = "文件数量超出限制！[最多{0}个]";
                    errMsg = errMsg.replace("{0}",fileNumLimit);
                    MessageBox.error(errMsg);
                    break;
                case "Q_TYPE_DENIED":
                    errMsg = "文件格式有误！[只允许{0}格式]";
                    errMsg = errMsg.replace("{0}",accept);
                    MessageBox.error(errMsg);
                    break;
                case "Q_EMPTY_FILE_DENIED":
                    MessageBox.error("禁止上传空文件！");
                    break;
                default:
                    break;
                }
            });
        }
        $("#${id}_pickBtn").on("click",function(){
            $("#${id}_pickerHide>div>label").click();
        });
        $("#${id}_uploadBtn").on("click",function(){
            webUploader.upload();
        });
        webUploader.extResetFileList = function(){
            _data = initData();
            $("#${id}_fileListTbody").empty();
            $("#${id}_simpleModeBox").empty();
            updateThead();
        }
        webUploader.extAddFileList = function(files){
            if(files != null){
                var len = files.length;
                var i = 0;
                for(;i < len;i++){
                    var file = files[i];
                    appendFileDom(file);
                    var fileData = $.extend({},file);
                    _data.oldFiles.push(fileData);
                    if(!(readonly && simpleMode)){
                        if(showStatus){
                            $("#${id}_upSts_" + file.id).text("${archiveText}");
                        }
                        if(showUploadTime){
                            $("#${id}_uploadTime_" + file.id).text(file.uploadTime);
                        }
                        if(showDescn){
                            var descnBox = $("#${id}_descnBox_" + file.id);
                            descnBox.val(file.descn);
                            if(!descnAllowModify){
                                descnBox.prop("readonly",true);
                            }
                        }
                        if(showProgress){
                            $("#${id}_progress_" + file.id).empty();
                        }
                        if(showOperation){
                            if(showRemove){
                                $("#${id}_remove_" + file.id).on("click",(function(file){
                                    return function(){
                                        if(onBeforeFileRemove){
                                            var result = onBeforeFileRemove(file);
                                            if(result == false){
                                                return;
                                            }
                                        }
                                        var delOne = delArrayItem(_data.oldFiles,{id:file.id},function(i1,i2){
                                            return i1.id == i2.id;
                                        });
                                        _data.delFiles.push(delOne);
                                        $("#${id}_file_" + file.id).remove();
                                        updateThead();
                                        if(onFileRemove){
                                            onFileRemove(file);
                                        }
                                    };
                                })(file));
                            }
                            if(showDownload){
                                if(file.hasOwnProperty("downloadUri")){
                                    var $download = $("#${id}_download_" + file.id);
                                    $download.attr("href","${basePath}" + file.downloadUri);
                                    $download.show();
                                }
                            }
                        }
                    }
                }
                updateThead();
            }
        };
        webUploader.extGetData = function(){
            var data = initData();
            data.newFiles = _data.newFiles.slice(0);
            data.oldFiles = _data.oldFiles.slice(0);
            data.delFiles = _data.delFiles.slice(0);
            fillDescn(data.newFiles);
            fillDescn(data.oldFiles);
            fillDescn(data.delFiles);
            return data;
        }
        var debug = function(msg){
            if(debugFlag){
                var debugArea = $("#${id}_debugArea");
                var ctnt = debugArea.val() + msg + "\n";
                var len = ctnt.length;
                if(len > 999999){
                    ctnt = ctnt.substring(len - 999999);
                }
                debugArea.val(ctnt);
            }
        }
        if(debugFlag){
            $("#${id}_debugDiv").show();
        }
        return webUploader;
    })();
    </script>
</div>

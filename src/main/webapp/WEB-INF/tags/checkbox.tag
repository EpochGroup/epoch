@var map = tag.map!{};
@var item = tag.item!{};
@var name = tag.name!{};
@var onchange = tag.onchange!'undefined';
@if(map != null && map != ''){
	@for(entryM in map!{}){
		<input type="checkbox" class="heigth_repair_check" onchange="${onchange}"
		    @if(item != null && item != ''){
		      @for(entryI in item!{}){
                @if(entryM.key == entryI.key){
                  checked="checked"
                @}}
		    @}
	           name="${name}" value="${entryM.key}"><span>${entryM.value}</span>
    @}
@}
@var id = tag.id;
@var title = tag.title;
@var treeData = tag.treeData;
@var treeSetting = tag.treeSetting;
@var callback = tag.callback!"null";
@var multiple = tag.multiple!"false";
@var showRoot = tag.showRoot!"true";
@var rootName = tag.rootName!'根节点';
@var rootIdKey = tag.rootIdKey!"id";
@var rootIdValue = tag.rootIdValue!"0";
@var onlyCheckLeaf = tag.onlyCheckLeaf!"false";
@var parentSubRelated = tag.parentSubRelated!"true";
@var backdrop = tag.backdrop!"false";
@var width = tag.width!"auto";
@var height = tag.height!"auto";
@var showPopBtn = tag.showPopBtn!"true";
@var confirmBtnText = tag.confirmBtnText!'确定';
@var cancelBtnText = tag.cancelBtnText!'取消';
@var onPopBtnClick = tag.onPopBtnClick!"undefined";
<span id="${id}_popBtn" class="btn visa-btn-icon visa-btn-select fa fa-search fa-1" style="${showPopBtn == 'true' ? '' : 'display:none;'}"></span>
<input id="${id}_opt_title" type="hidden" value="${title}" />
<input id="${id}_opt_treeData" type="hidden" />
<input id="${id}_opt_treeSetting" type="hidden" />
<input id="${id}_opt_callback" type="hidden" />
<input id="${id}_opt_multiple" type="hidden" value="${multiple}" />
<input id="${id}_opt_showRoot" type="hidden" value="${showRoot}" />
<input id="${id}_opt_rootName" type="hidden" value="${rootName}" />
<input id="${id}_opt_rootIdKey" type="hidden" value="${rootIdKey}" />
<input id="${id}_opt_rootIdValue" type="hidden" value="${rootIdValue}" />
<input id="${id}_opt_onlyCheckLeaf" type="hidden" value="${onlyCheckLeaf}" />
<input id="${id}_opt_parentSubRelated" type="hidden" value="${parentSubRelated}" />
<input id="${id}_opt_backdrop" type="hidden" value="${backdrop}" />
<input id="${id}_opt_width" type="hidden" value="${width}" />
<input id="${id}_opt_height" type="hidden" value="${height}" />
<input id="${id}_opt_confirmBtnText" type="hidden" value="${confirmBtnText}" />
<input id="${id}_opt_cancelBtnText" type="hidden" value="${cancelBtnText}" />
<epoch:winOpen id="${id}" title="${title}" backdrop="${backdrop}" width="${width}" height="${height}"
    btnSave="${confirmBtnText}" btnClose="${cancelBtnText}">
    <ul id="${id}_tree" class="ztree"></ul>
</epoch:winOpen>
<script type="text/javascript">
(function($){
    var treeData = ${treeData!"''"};
    var treeSettingStr = '${lisFun.escapeForQuot(treeSetting,"\'")}';
    var callbackStr = '${lisFun.escapeForQuot(callback,"\'")}';
    $("#${id}_opt_callback").val(callbackStr);
    $("#${id}_opt_treeData").val(JSON.stringify(treeData));
    $("#${id}_opt_treeSetting").val(treeSettingStr);
    var onPopBtnClick = window.${onPopBtnClick};
    if (!onPopBtnClick) {
        onPopBtnClick = function(e){
            CommonTreePopup.show("${id}");
        };
    }
    $("#${id}_popBtn").on("click",onPopBtnClick);
})(jQuery);
</script>

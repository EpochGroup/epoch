@var id = tag.id;
@var businessId = tag.businessId;
@var businessType = tag.businessType;
@var auto = tag.auto!"false";
@var dragDrop = tag.dragDrop!"false";
@var numLimit = tag.numLimit;
@var sizeLimit = tag.sizeLimit;
@var totalSizeLimit = tag.totalSizeLimit;
@var accept = tag.accept;
@var filter = tag.filter;
@var readonly = tag.readonly!"false";
@var allowEmptyFile = tag.allowEmptyFile!"true";
@var simpleMode = tag.simpleMode!"false";
@var simpleModeAlign = tag.simpleModeAlign!"hor";
@var showChooseFile = readonly == "true" ? "false" : (tag.showChooseFile!"true");
@var showUpload = readonly == "true" ? "false" : (tag.showUpload!(auto == "true" ? "false" : "true"));
@var showFileSize = tag.showFileSize!"false";
@var showStatus = tag.showStatus!"false";
@var showUploadTime = tag.showUploadTime!"false";
@var showDescn = tag.showDescn!"false";
@var showProgress = tag.showProgress!"false";
@var showOperation = tag.showOperation!"true";
@var showRemove = readonly == "true" ? "false" : (tag.showRemove!"true");
@var showRetry = readonly == "true" ? "false" : (tag.showRetry!"true");
@var showDownload = tag.showDownload!"true";
@var descnAllowModify = readonly == "true" ? "false" : (tag.descnAllowModify!"true");
@var headerDisplay = tag.headerDisplay!"auto"; 
@var fileSizeUnit = tag.fileSizeUnit!"'ALL'";
@var styleClass = tag.styleClass;
@var inlineStyle = tag.inlineStyle;
@var fileNameHeadText = tag.fileNameHeadText!"文件名称";
@var fileSizeHeadText = tag.fileSizeHeadText!"文件大小";
@var fileStatusHeadText = tag.fileStatusHeadText!"文件状态";
@var uploadTimeHeadText = tag.uploadTimeHeadText!"上传时间";
@var uploadTimeLocalText = tag.uploadTimeLocalText!"本地";
@var descnHeadText = tag.descnHeadText!"描述";
@var uploadProgressHeadText = tag.uploadProgressHeadText!"上传进度";
@var optionHeadText = tag.optionHeadText!"操作";
@var chooseFileBtnText = tag.chooseFileBtnText!"选择文件";
@var uploadBtnText = tag.uploadBtnText!"上传"; 
@var removeBtnText = tag.removeBtnText!"移除";
@var retryBtnText = tag.retryBtnText!"重试";
@var downloadBtnText = tag.downloadBtnText!"下载";
@var waitUploadText = tag.waitUploadText!"等待上传";
@var uploadingText = tag.uploadingText!"上传中";
@var uploadFailedText = tag.uploadFailedText!"上传失败";
@var uploadSuccessText = tag.uploadSuccessText!"上传成功";
@var archiveText = tag.archiveText!"已存档";
@var customUploader = tag.customUploader != null ? tag.customUploader : "null";
@var onBeforeFileQueued = tag.onBeforeFileQueued!"undefined";
@var onFileQueued = tag.onFileQueued!"undefined";
@var onFilesQueued = tag.onFilesQueued!"undefined";
@var onFileDequeued = tag.onFileDequeued!"undefined";
@var onUploaderReset = tag.onUploaderReset!"undefined";
@var onUploadStart = tag.onUploadStart!"undefined";
@var onUploadStop = tag.onUploadStop!"undefined";
@var onUploadFinished = tag.onUploadFinished!"undefined";
@var onFileStart = tag.onFileStart!"undefined";
@var onDataBeforeSend = tag.onDataBeforeSend!"undefined";
@var onServerAccept = tag.onServerAccept!"undefined";
@var onFileProgress = tag.onFileProgress!"undefined"; 
@var onFileError = tag.onFileError!"undefined";
@var onFileSuccess = tag.onFileSuccess!"undefined";
@var onFileComplete = tag.onFileComplete!"undefined";
@var onValidateFaild = tag.onValidateFaild!"undefined";
@var onBeforeFileRemove = tag.onBeforeFileRemove!"undefined";
@var onFileRemove = tag.onFileRemove!"undefined";
@var onInitFilesLoad = tag.onInitFilesLoad!"undefined";
@var alterMoreBtns = tag.alterMoreBtns;
@var children = tag.children;
@var childrenMap = tag.childrenMap;
@var moreBtns = alterMoreBtns!childrenMap['moreBtn'];

<epoch:uploader id="${id}"
    url="${basePath}/sys/common/attach/upload"
    auto="${auto}"
    dragDrop="${dragDrop}"
    numLimit="${numLimit}"
    sizeLimit="${sizeLimit}"
    totalSizeLimit="${totalSizeLimit}"
    accept="${accept}"
    filter="${filter}"
    readonly="${readonly}"
    allowEmptyFile="${allowEmptyFile}"
    simpleMode="${simpleMode}"
    simpleModeAlign="${simpleModeAlign}"
    showChooseFile="${showChooseFile}"
    showUpload="${showUpload}"
    showFileSize="${showFileSize}"
    showStatus="${showStatus}"
    showUploadTime="${showUploadTime}"
    showDescn="${showDescn}"
    showProgress="${showProgress}"
    showOperation="${showOperation}"
    showRemove="${showRemove}"
    showRetry="${showRetry}"
    showDownload="${showDownload}"
    descnAllowModify="${descnAllowModify}"
    headerDisplay="${headerDisplay}"
    fileSizeUnit="${fileSizeUnit}"
    styleClass="${styleClass}"
    inlineStyle="${inlineStyle}"
    fileNameHeadText="${fileNameHeadText}"
    fileSizeHeadText="${fileSizeHeadText}"
    fileStatusHeadText="${fileStatusHeadText}"
    uploadTimeHeadText="${uploadTimeHeadText}"
    uploadTimeLocalText="${uploadTimeLocalText}"
    descnHeadText="${descnHeadText}"
    uploadProgressHeadText="${uploadProgressHeadText}"
    optionHeadText="${optionHeadText}"
    chooseFileBtnText="${chooseFileBtnText}"
    uploadBtnText="${uploadBtnText}"
    removeBtnText="${removeBtnText}"
    retryBtnText="${retryBtnText}"
    downloadBtnText="${downloadBtnText}"
    waitUploadText="${waitUploadText}"
    uploadingText="${uploadingText}"
    uploadFailedText="${uploadFailedText}"
    uploadSuccessText="${uploadSuccessText}"
    archiveText="${archiveText}"
    customUploader="${customUploader}"
    onBeforeFileQueued="${onBeforeFileQueued}"
    onFileQueued="${onFileQueued}"
    onFilesQueued="${onFilesQueued}"
    onFileDequeued="${onFileDequeued}"
    onUploaderReset="${onUploaderReset}"
    onUploadStart="${onUploadStart}"
    onUploadStop="${onUploadStop}"
    onUploadFinished="${onUploadFinished}"
    onFileStart="${onFileStart}"
    onDataBeforeSend="${onDataBeforeSend}"
    onServerAccept="${onServerAccept}"
    onFileProgress="${onFileProgress}"
    onFileError="${onFileError}"
    onFileSuccess="${onFileSuccess}"
    onFileComplete="${onFileComplete}"
    onValidateFaild="${onValidateFaild}"
    onBeforeFileRemove="${onBeforeFileRemove}"
    onFileRemove="${onFileRemove}"
    alterMoreBtns="${moreBtns}"
/>
<script type="text/javascript">
var _var_${id}_attach = (function(){
    var uploader = _var_${id}_uploader;
    var onInitFilesLoad = window.${onInitFilesLoad};
    var attach = {
        uploader:uploader,
        refresh:function(businessId,businessType){
            if(isEmpty(businessId)){
                businessId = "${businessId}";
            }
            if(isEmpty(businessType)){
                businessType = "${businessType}";
            }
            uploader.reset();
            uploader.extResetFileList();
            if(!isEmpty(businessId)){
                var url = "${basePath}/sys/common/attach/initFileList?businessId=" + businessId;
                if(!isEmpty(businessType)){
                    url += "&businessType=" + businessType;
                }
                $.ajax({
                    url:url,
                    type:"GET",
                    dataType:"json",
                    success:function(result,status,xhr){
                        if(result.hasOwnProperty("attachInfos")){
                            uploader.extAddFileList(result.attachInfos);
                            if(onInitFilesLoad){
                                onInitFilesLoad(result.attachInfos);
                            }
                        }
                    },
                    error:function(xhr,status,err){
                        console.warn("ajax response error: {status:'" + status + "',exception:'" + err + "'}");
                    }
                });
            }
        },
        checkConfirm:function(callback){
            var stats = uploader.getStats();
            if(stats.queueNum > 0 || stats.progressNum > 0 || stats.interruptNum > 0 || stats.uploadFailNum > 0){
                MessageBox.confirm(_i18ns("cmn_msg_warn_confirmForUploading"),callback);
            }else{
                if(callback){
                    callback();
                }
            }
        }
    };
    attach.refresh();
    return attach;
})();
</script>
